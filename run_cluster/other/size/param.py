from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_layer
import numpy as np
import itertools

variable = {'nb_cube_x': np.arange(3.0, 11.0, 1.0),
             'nb_cube_y': np.arange(3.0,11.0,1.0)}

def parameter():
    parameter_layer.param_topology['param_neuron']['t_ref']=1.5
    parameter_layer.param_connexion['g']=1.0
    parameter_layer.param_topology['sigma_I_ext']=1.0
    parameter_layer.param_topology['mean_I_ext']=700.0
    return parameter_layer

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()