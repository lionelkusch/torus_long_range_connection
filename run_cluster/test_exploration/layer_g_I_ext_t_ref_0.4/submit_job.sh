#!/bin/sh

# define SLURM params
walltime="--time 72:00:00"
min_config_cpu="--mincpus 4"
nb_node="--nodes 1"
nb_processus_by_job="--ntasks 4"
nb_cpu_by_task="--cpus-per-task 1"
nb_task_by_cpu="--ntasks-per-core 1"
nb_thread_by_core="--threads-per-core 1"
politic_thread_allocation="--hint compute_bound"
memory="--mem 4G"

option_SLURM="${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${politic_thread_allocation} ${memory}"

# define params explorating
NAME_SIMULATION='layer_g_I_ext_t_ref_0.4'

# define bash loop
# create export commands
exvars="--export=NAME_SIMULATION=${NAME_SIMULATION}"

# set a name to the job
jobname="--job-name=${NAME_SIMULATION}"

stdout="--output ./${NAME_SIMULATION}.out"
stderr="--error ./${NAME_SIMULATION}.err"

# build a scavenger job, gpu job, or other job
echo ${option_SLURM} ${exvars} ${jobname} ${stdout} ${stderr} ./submit_pys.sh
printf "Sbatch should run now\n"

# submit slurm bash file
sbatch ${option_SLURM} ${exvars} ${jobname} ${stdout} ${stderr} ./submit_pys.sh
printf "Sbatch run now\n"

