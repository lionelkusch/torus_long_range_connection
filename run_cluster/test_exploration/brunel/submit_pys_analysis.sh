#!/bin/sh
export LC_ALL=en_US.UTF-8

# export vars
echo ${NAME_SIMULATION}

# internal vars
export PYTHONPATH=/home/lionelkusch/:$PYTHONPATH
nest_simulator=/home/lionelkusch/Jirsa_V_Stefanescu_A_2010_with_Nest/singularity_run/Nest_simulator
run_simulation=/home/lionelkusch/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/Brunel_simulation/analysis.py
folder_data=/home/lionelkusch/save_data/${NAME_SIMULATION}/

# program
${nest_simulator} ${run_simulation} ${folder_data}
