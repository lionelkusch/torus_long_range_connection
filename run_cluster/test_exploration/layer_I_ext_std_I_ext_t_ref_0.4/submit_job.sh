#!/bin/sh

# define SLURM params
walltime="--time 72:00:00"
min_config_cpu="--mincpus 4"
nb_node="--nodes 1"
nb_processus_by_job="--ntasks 1"
nb_cpu_by_task="--cpus-per-task 4"
nb_task_by_cpu="--ntasks-per-core 1"
nb_thread_by_core="--threads-per-core 1"
politic_thread_allocation="--hint compute_bound"
memory="--mem 4G"

option_SLURM_sim="${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${politic_thread_allocation} ${memory}"

nb_processus_by_job="--ntasks 4"
nb_cpu_by_task="--cpus-per-task 1"
option_SLURM_ana="${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${politic_thread_allocation} ${memory}"

# define params explorating
NAME_SIMULATION='layer_I_ext_std_I_ext_t_ref_0.4'

# define bash loop
# create export commands
exvars="--export=NAME_SIMULATION=${NAME_SIMULATION}"

# set a name to the job
jobname_sim="--job-name=${NAME_SIMULATION}_sim"
jobname_an="--job-name=${NAME_SIMULATION}_an"

stdout_sim="--output ./${NAME_SIMULATION}_sim.out"
stderr_sim="--error ./${NAME_SIMULATION}_sim.err"
stdout_an="--output ./${NAME_SIMULATION}_an.out"
stderr_an="--error ./${NAME_SIMULATION}_an.err"

# build a scavenger job, gpu job, or other job
echo ${option_SLURM_sim} ${exvars} ${jobname_sim} ${stdout_sim} ${stderr_sim} ./submit_pys.sh
printf "Sbatch should run now\n"

# submit slurm bash file
id_job=$(sbatch ${option_SLURM_sim} ${exvars} ${jobname_sim} ${stdout_sim} ${stderr_sim} ./submit_pys_simulation.sh)
printf "id simulation ${id_job}\n"
dependecy="--dependecy afterany:${id_job}"
sbatch ${dependency} ${option_SLURM_ana} ${exvars} ${jobname_an} ${stdout_an} ${stderr_an} ./submit_pys_analysis.sh
printf "Sbatch run now\n"

