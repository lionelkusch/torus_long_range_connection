#!/bin/sh
module load daint-gpu
module load sarus

BEGIN=2000
END=12000
# export vars
echo ${NAME_SIMULATION} ${BEGIN} ${END}

# internal vars
export NESTRCFILENAME=$SCRATCH/Nest_file
name_image=load/library/nest
run_simulation=$SCRATCH/Jirsa_V_Stefanescu_A_2010_with_Nest/run_cluster/Piz_Daint/test_adaptation/weight_hom/sim.py
folder_data=$SCRATCH/save_data/${NAME_SIMULATION}/

# program
cd $SCRATCH
echo ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}
sarus --debug run --mount=type=bind,source=$SCRATCH,destination=$SCRATCH ${name_image} python ${run_simulation} ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}
