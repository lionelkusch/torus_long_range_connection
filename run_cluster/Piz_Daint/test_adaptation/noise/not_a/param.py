from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_mechanism
import numpy as np
import itertools

variable = {
	    #'sigma_noise': np.arange(0.0,2000.0,200.0),
	    'sigma_noise': np.arange(1100.0,2200.0,200.0),
            'tau_long':np.concatenate(([0.1],np.arange(10.0, 450.0, 10.0)))
		     }

def parameter():
    parameter_grid_mechanism.param_topology['param_neuron']['b']=0.0
    parameter_grid_mechanism.param_topology['param_neuron']['a']=0.0
    parameter_grid_mechanism.param_background['sigma_noise']=1800.0
    parameter_grid_mechanism.param_connexion['weight_long']=15.0
    parameter_grid_mechanism.param_topology['long_connections']=np.array([([5,5,'e'],[15,5,'e']),
                                 ([5,5,'e'],[15,5,'i']),
                                 ])
    return parameter_grid_mechanism

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
