#!/bin/sh
module load daint-gpu
module load sarus

BEGIN=12000
END=22000
# export vars
echo ${NAME_SIMULATION} ${BEGIN} ${END}
echo ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}

# internal vars
export NESTRCFILENAME=$SCRATCH/Nest_file
name_image=load/library/nest
run_simulation=$SCRATCH/Jirsa_V_Stefanescu_A_2010_with_Nest/run_cluster/Piz_Daint/test_adaptation/g/analysis_one.py
folder_data=$SCRATCH/save_data/${NAME_SIMULATION}/
database=$SCRATCH/save_data/${NAME_SIMULATION}/database.db
table="exploration_1"

# program
echo ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}
number_tread=15
for i in $(seq 0 ${number_tread})
do
	echo $(echo "${SLURM_ARRAY_TASK_ID} * ${number_tread} + $i" |bc)
	sarus --debug run --mount=type=bind,source=$SCRATCH,destination=$SCRATCH ${name_image} python ${run_simulation} ${folder_data} $(echo "${SLURM_ARRAY_TASK_ID} * ${number_tread} + $i" |bc) ${database} ${table} ${BEGIN} ${END} &
done
wait 
