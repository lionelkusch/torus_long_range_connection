#!/bin/sh

# define SLURM params
walltime="--time 01:00:00"
min_config_cpu="--mincpus 16"
nb_node="--nodes 1"
nb_processus_by_job="--ntasks 1"
nb_processus_by_node="--ntasks-per-node 1"
nb_cpu_by_task="--cpus-per-task 16"
nb_task_by_cpu="--ntasks-per-core 2"
nb_thread_by_core="--threads-per-core 2"
memory="--mem 8G"
nb_sim=4000
mail=" --mail-type ALL --mail-user lionel.kusch@univ-amu.fr "
obligation="-p normal -C gpu -A ich001 "
#obligation="-p debug -C gpu -A ich001 "
option_SLURM_sim="${obligation} ${mail} ${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job}  ${nb_processus_by_node} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${memory}"

walltime="--time 2:00:00"
min_config_cpu="--mincpus 20"
nb_processus_by_job="--ntasks 20"
nb_processus_by_node="--ntasks-per-node 20"
nb_cpu_by_task="--cpus-per-task 1"
nb_an=$(expr $nb_sim / 20 + 1)
memory="--mem 60G"
## test part
#walltime="--time 0:30:00"
#min_config_cpu="--mincpus 2"
#nb_processus_by_job="--ntasks 2"
#nb_processus_by_node="--ntasks-per-node 2"
#nb_cpu_by_task="--cpus-per-task 1"
option_SLURM_ana="${obligation} ${mail} ${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job}  ${nb_processus_by_node} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${memory}"

# define params explorating
NAME_SIMULATION='adaptation_full_long'

# define bash loop
# create export commands
exvars="--export=NAME_SIMULATION=${NAME_SIMULATION}"

# set a name to the job
jobname="--job-name=${NAME_SIMULATION}"

#MOdify it from write in scrach partition 
stdout_sim="--output ./log/${NAME_SIMULATION}_%A_%a_sim.out"
stderr_sim="--error ./log/${NAME_SIMULATION}_%A_%a_sim.err"
stdout_an="--output ./log/${NAME_SIMULATION}_%A_%a_an.out"
stderr_an="--error ./log/${NAME_SIMULATION}_%A_%a_an.err"

# build a scavenger job, gpu job, or other job
echo ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} ./submit_pys.sh
printf "Sbatch should run now\n"

# submit slurm bash file
#increment=100
#increment_an=$(expr $increment / 20)
#n_0=0
#n_0_an=0
#n_an=$(($increment_an-1))
#init_sim=$(($increment-1))
#for ((n=$init_sim; n<$nb_sim; n=n+$increment));
#do 
#	echo ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} --array $n_0-$n ./submit_pys.sh
#	id_job=$(sbatch ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} --array $n_0-$n ./submit_pys_simulation.sh)
#	id_job=$(echo ${id_job} | cut --delimiter=' ' -f4)
#	printf "Sbatch run now\n"
#	echo "sbatch --dependency=afterany:$id_job ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} --array $n_0_an-$n_an ./submit_pys_analysis.sh"
#	sbatch --dependency=afterany:$id_job ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} --array $n_0_an-$n_an ./submit_pys_analysis_one.sh
#	printf "Sbatch run now\n"
#	n_0=$(($n_0+$increment))
#	n_0_an=$(($n_0_an+$increment_an))
#	n_an=$(($n_an+$increment_an))
#	sleep 30s
#done


#echo ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} --array $n-$nb_sim ./submit_pys.sh
#id_job=$(sbatch ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} --array $n-$nb_sim ./submit_pys_simulation.sh)
#id_job=$(echo ${id_job} | cut --delimiter=' ' -f4)
#printf "Sbatch run now\n"
#echo "sbatch --dependency=afterany:$id_job ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} --array $n_an-$nb_an ./submit_pys_analysis.sh"
#sbatch --dependency=afterany:$id_job ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} --array $n_an-$nb_an ./submit_pys_analysis_one.sh


echo ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} ./submit_pys.sh
sbatch --dependency=afterany:21321295 ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} --array 201-210 ./submit_pys_analysis_one.sh
