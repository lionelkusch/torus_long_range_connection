from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_sub
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([0.0,0.2,0.4,0.6,0.8,
					   1.0,1.2,1.4,1.6,1.8,
					   2.0,4.0,6.0,8.0,
					   10.0,12.5,15.0,17.5,
					   20.0,22.5,25.0,27.5,
					   30.0,35.0,
					   40.0,45.0,
					   50.0
					]),
             'sigma_noise': np.arange(0.0, 2000.0, 100.0)}

def parameter():
    return parameter_grid_sub

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
