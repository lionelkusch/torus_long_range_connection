#!/bin/sh

# define SLURM params
walltime="--time 00:40:00"
min_config_cpu="--mincpus 16"
nb_node="--nodes 1"
nb_processus_by_job="--ntasks 1"
nb_processus_by_node="--ntasks-per-node 1"
nb_cpu_by_task="--cpus-per-task 16"
nb_task_by_cpu="--ntasks-per-core 2"
nb_thread_by_core="--threads-per-core 2"
memory="--mem 8G"
array='--array 0-540'
mail=" --mail-type ALL --mail-user lionel.kusch@univ-amu.fr "
obligation="-p normal -C gpu -A ich001 "
option_SLURM_sim="${obligation} ${mail} ${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job}  ${nb_processus_by_node} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${memory} ${array}"

walltime="--time 2:00:00"
min_config_cpu="--mincpus 20"
nb_processus_by_job="--ntasks 20"
nb_processus_by_node="--ntasks-per-node 20"
nb_cpu_by_task="--cpus-per-task 1"
array='--array 0-27'
memory="--mem 60G"
## test part
#walltime="--time 0:30:00"
#min_config_cpu="--mincpus 2"
#nb_processus_by_job="--ntasks 2"
#nb_processus_by_node="--ntasks-per-node 2"
#nb_cpu_by_task="--cpus-per-task 1"
option_SLURM_ana="${obligation} ${mail} ${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job}  ${nb_processus_by_node} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${memory} ${array}"

# define params explorating
NAME_SIMULATION='sub_noise_gc'

# define bash loop
# create export commands
exvars="--export=NAME_SIMULATION=${NAME_SIMULATION}"

# set a name to the job
jobname="--job-name=${NAME_SIMULATION}"

#MOdify it from write in scrach partition 
stdout_sim="--output ./log/${NAME_SIMULATION}_%A_%a_sim.out"
stderr_sim="--error ./log/${NAME_SIMULATION}_%A_%a_sim.err"
stdout_an="--output ./log/${NAME_SIMULATION}_%A_%a_an.out"
stderr_an="--error ./log/${NAME_SIMULATION}_%A_%a_an.err"

# build a scavenger job, gpu job, or other job
echo ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} ./submit_pys.sh
printf "Sbatch should run now\n"

# submit slurm bash file
id_job=$(sbatch ${option_SLURM_sim} ${exvars} ${jobname} ${stdout_sim} ${stderr_sim} ./submit_pys_simulation.sh)
id_job=$(echo ${id_job} | cut --delimiter=' ' -f4)
printf "Sbatch run now\n"
echo "sbatch --dependency=afterany:$id_job ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} ./submit_pys_analysis.sh"
sbatch --dependency=afterany:$id_job ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} ./submit_pys_analysis_one.sh
printf "Sbatch run now\n"

#echo ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} ./submit_pys.sh
#sbatch ${option_SLURM_ana} ${exvars} ${jobname} ${stdout_an} ${stderr_an} ./submit_pys_analysis_one.sh
