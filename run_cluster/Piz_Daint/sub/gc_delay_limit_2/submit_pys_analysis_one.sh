#!/bin/sh
module load daint-gpu
module load shifter-ng

BEGIN=2000
END=12000
# export vars
echo ${NAME_SIMULATION} ${BEGIN} ${END}
echo ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}

# internal vars
export NESTRCFILENAME=$SCRATCH/tng/lionel/Nest_file
name_image=load/library/Nest2.14
run_simulation=$SCRATCH/tng/lionel/Jirsa_V_Stefanescu_A_2010_with_Nest/run_cluster/Piz_Daint/sub/gc_delay_limit_2/analysis_one.py
folder_data=$SCRATCH/tng/lionel/save_data/${NAME_SIMULATION}/
database=$SCRATCH/tng/lionel/save_data/${NAME_SIMULATION}/database.db
table="exploration_1"

# program
echo ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}
number_tread=20
for i in $(seq 0 ${number_tread})
do
	echo $(echo "${SLURM_ARRAY_TASK_ID} * ${number_tread} + $i" |bc)
	shifter --debug run ${name_image} python ${run_simulation} ${folder_data} $(echo "${SLURM_ARRAY_TASK_ID} * ${number_tread} + $i" |bc) ${database} ${table} ${BEGIN} ${END} &
done
wait 
