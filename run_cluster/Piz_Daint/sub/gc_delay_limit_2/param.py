from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_sub
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0,100.0,110.0,120.0
					]),

             'tau_long': np.concatenate((np.arange(80.0, 320.0, 10.0),[20000.0]))}

def parameter():
    parameter_grid_sub.param_background['sigma_noise']=120.0
    parameter_grid_sub.param_connexion['g']=2.0
    parameter_grid_sub.param_topology['long_range']=True
    return parameter_grid_sub

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
