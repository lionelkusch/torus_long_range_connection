from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_sub
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([0.0,0.2,0.4,0.6,0.8,
					   1.0,1.2,1.5,1.7,
					   2.0,4.0,6.0,8.0,
					   10.0,12.0,14.0,18.,
					   20.0,22.5,25.0,27.5,
					]),
             'tau_long': np.concatenate((np.arange(80.0, 320.0, 10.0),[20000.0]))}

def parameter():
    parameter_grid_sub.param_background['sigma_noise']=2500.0
    parameter_grid_sub.param_background['mean_noise']=400.0
    parameter_grid_sub.param_topology['long_range']=True
    return parameter_grid_sub

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
