from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_sub
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,
					   1.0,1.5,
					   2.0,4.0,6.0,7.0,8.0,9.0,
					   10.0,11.0,12.0,13.0,14.0,15.0,18.,
					   20.0,22.5,25.0,27.5,
					   30.0,32.5,35.0,37.5,
					   40.0,42.5,45.0,47.5,
					   50.0,55.0,
					]),
             'tau_long': np.arange(80.0, 320.0, 10.0)}

def parameter():
    parameter_grid_sub.param_background['sigma_noise']=600.0
    parameter_grid_sub.param_topology['long_range']=True
    return parameter_grid_sub

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
