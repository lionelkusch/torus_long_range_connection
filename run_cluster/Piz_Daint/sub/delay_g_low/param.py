from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_sub
import numpy as np
import itertools

variable = {'g': np.array([0.051,0.052,0.053,0.054,0.055,0.056,0.057,0.058,0.059
		   	   #0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,
			   #0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,
			   #0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,
			   #1.0,2.0,10.0
			   ]),
             'tau_long': np.arange(80.0, 320.0, 10.0)}

def parameter():
    parameter_grid_sub.param_background['sigma_noise']=2500.0
    parameter_grid_sub.param_background['mean_noise']=400.0
    #parameter_grid_sub.param_connexion['weight_excitatory']=10.0
    parameter_grid_sub.param_connexion['weight_excitatory']=6.0
    parameter_grid_sub.param_topology['long_range']=True
    return parameter_grid_sub

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
