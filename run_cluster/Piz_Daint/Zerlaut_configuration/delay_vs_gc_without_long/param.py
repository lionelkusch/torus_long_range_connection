from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_zerlaut
import numpy as np
import itertools

variable = {
	    'weight_excitatory': np.array([
				    #0.0,
				    0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.85,0.9,0.95,
				    #1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,
				    #2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,
				    #3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,
				    #4.0,5.0,10.0,15.0,20.0,
				    #22.0,22.5,23.0,23.5,24.0,24.5,25.0,25.5,26.0,26.5,
				    #27.0,28.0,29.0,30.0
				    32.0,34.0,36.0,38.0,40.0,42.0,44.0,46.0,48.0,50.0
					]),
             'tau_long': np.arange(80.0, 320.0, 50.0),
	     }

def parameter():
    return parameter_grid_zerlaut

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
