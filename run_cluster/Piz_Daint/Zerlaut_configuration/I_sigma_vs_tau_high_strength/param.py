from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_zerlaut
import numpy as np
import itertools

variable = {
	    'sigma_I_ext': np.array([0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
					]),
             'tau_long': np.arange(80.0, 320.0, 10.0),
	     }

def parameter():
    parameter_grid.param_connexion['weight_excitatory']=27.0
    return parameter_grid_zerlaut

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
