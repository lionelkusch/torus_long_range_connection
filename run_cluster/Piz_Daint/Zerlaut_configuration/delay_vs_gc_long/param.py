from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_zerlaut
import numpy as np
import itertools

variable = {
	    'weight_long': np.array([
				    3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,
					]),
             'tau_long': np.arange(80.0, 320.0, 10.0),
	     }

def parameter():
    return parameter_grid_zerlaut

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
