from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable = {'sigma_I_ext': np.array([1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
				     #0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0,
				     #100.0,110.0,120.0,130.0,140.0,150.0,160.0,170.0,180.0,190.0,
				     #200.0,250.0,300.0,350.0
					]),
             'tau_long': np.arange(80.0, 500.0, 10.0)}

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=1.5
    parameter_grid.param_connexion['g']=1.0
    parameter_grid.param_topology['sigma_I_ext']=0.0
    parameter_grid.param_topology['sigma_V_0']=100.0
    parameter_grid.param_topology['mean_w_0']=10.0
    parameter_grid.param_topology['mean_I_ext']=700.0
    parameter_grid.param_topology['long_range']=True
    parameter_grid.param_background['noise']=True
    parameter_grid.param_background['sigma_noise']=50.0
    parameter_grid.param_connexion['weight_excitatory']=3.5
    parameter_grid.param_nest['master_seed']=46
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
