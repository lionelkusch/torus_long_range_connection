from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([
					   #0.0,0.5,
					   #1.0,1.5,
					   #2.0,2.5,
					   #3.0,3.5,
					   #4.0,4.5,
					   #5.0,5.5,
					   #6.0,6.5,
					   #7.0,7.5,
					   #8.0,8.5,
					   #9.0,9.5,
					   #10.0,15.0,20.0,25.0
					   #20.5,
					   #21.0,21.5,
					   #22.0,22.5,
					   #23.0,23.5,
					   #24.0,24.5,
					   #25.5,
					   #26.0,26.5,
					   #27.0,27.5,
					   #0.6,0.7,0.8,0.9,1.1,1.2,1.3,1.4
					   1.6,1.7,1.8,1.9
					]),
             'tau_long': np.arange(80.0, 320.0, 10.0),
             #'tau_long': np.array([0.0]),
	     }

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=1.5
    parameter_grid.param_connexion['g']=0.8
    parameter_grid.param_topology['sigma_I_ext']=0.0
    parameter_grid.param_topology['dim_x']=3.0
    parameter_grid.param_topology['dim_y']=3.0
    parameter_grid.param_topology['sigma_V_0']=100.0
    parameter_grid.param_topology['mean_w_0']=10.0
    parameter_grid.param_topology['mean_I_ext']=700.0
    parameter_grid.param_topology['long_range']=True
    #parameter_grid.param_topology['long_range']=False
    parameter_grid.param_background['noise']=True
    parameter_grid.param_background['sigma_noise']=50.0
    parameter_grid.param_nest['master_seed']=46
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
