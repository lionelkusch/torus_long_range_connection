from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([0.0,0.2,0.5,1.0,1.5,2.0,2.5,2.6,2.7,2.8,2.9,
					   3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,
					   4.0,4.1,4.2,4.3,4.4,4.5,
					   5.0,10.0,15.0,20.0,25.0
					]),
             'tau_long': np.arange(80.0, 500.0, 10.0)}

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=1.5
    parameter_grid.param_connexion['g']=1.0
    parameter_grid.param_topology['sigma_I_ext']=0.0
    parameter_grid.param_topology['sigma_V_0']=100.0
    parameter_grid.param_topology['mean_w_0']=10.0
    parameter_grid.param_topology['mean_I_ext']=700.0
    parameter_grid.param_topology['long_range']=True
    parameter_grid.param_background['noise']=True
    parameter_grid.param_background['sigma_noise']=50.0
    parameter_grid.param_nest['master_seed']=46
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
