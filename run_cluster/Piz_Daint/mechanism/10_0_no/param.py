from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_mechanism
import numpy as np
import itertools

variable = {
	    'weight_long': np.array([0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]),
            'tau_long':np.concatenate(([0.1],np.arange(10.0, 400.0, 10.0)))
		     }

def parameter():
    parameter_grid_mechanism.param_background['sigma_noise']=0.0
    return parameter_grid_mechanism

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
