from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid_mechanism
import numpy as np
import itertools

variable = {'weight_long': np.array([0.0,2.0,5.0,7.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,22.0,25.0]),
             'tau_long':np.concatenate(([0.1],np.arange(10.0, 400.0, 10.0)))
		     }

def parameter():
    parameter_grid_mechanism.param_background['sigma_noise']=1800.0
    parameter_grid_mechanism.param_topology['long_connections'] =np.array([([5,5,'e'],[15,5,'e']),
	                                     ([5,5,'e'],[15,5,'i']),])
    parameter_grid_mechanism.param_topology['param_neuron']['tau_w']=144.0
    return parameter_grid_mechanism

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
