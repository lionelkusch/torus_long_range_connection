import sys
sys.path.append('/scratch/snx3000/bp000176/tng/lionel/')
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.run_exploration import run
from param import parameter, name_variable, range_element
import numpy as np

def run_exploration(path,n,begin,end):
    list_connection =   [
    np.array([([5,5,'e'],[0,5,'e']),([0,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[1,5,'e']),([1,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[2,5,'e']),([2,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[3,5,'e']),([3,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[4,5,'e']),([4,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[5,5,'e']),([5,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[6,5,'e']),([6,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[7,5,'e']),([7,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[8,5,'e']),([8,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[9,5,'e']),([9,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[10,5,'e']),([10,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[11,5,'e']),([11,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[12,5,'e']),([12,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[13,5,'e']),([13,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[14,5,'e']),([14,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[15,5,'e']),([15,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[16,5,'e']),([16,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[17,5,'e']),([17,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[18,5,'e']),([18,5,'i'],[5,5,'e'])]),
    np.array([([5,5,'e'],[19,5,'e']),([19,5,'i'],[5,5,'e'])]),
    ]
    dict_variable = {}
    element = range_element()[n]
    results_path = path
    for i,name in enumerate(name_variable()):
        dict_variable[name]= element[i]
        results_path+='_'+name+'_'+str(element[i])
    print('folder'+results_path)
    params = parameter()
    params.param_connexion['weight_long']=params.param_connexion['weight_excitatory']*2.0
    params.param_topology['long_connections'] = list_connection[dict_variable['distance']] 
    print(params.param_topology['long_connections'])
    run(results_path, params, dict_variable, begin, end, print_volt=False)

if __name__ == "__main__":
    import sys
    if len(sys.argv)==5:
        run_exploration(sys.argv[1],int(sys.argv[2]),float(sys.argv[3]),float(sys.argv[4]))
    else:
        print('missing argument')
