#!/bin/sh
module load daint-gpu
module load shifter-ng

BEGIN=12000
END=22000
# export vars
echo ${NAME_SIMULATION} ${BEGIN} ${END}

# internal vars
export NESTRCFILENAME=$SCRATCH/tng/lionel/Nest_file
name_image=load/library/Nest2.14
run_simulation=$SCRATCH/tng/lionel/Jirsa_V_Stefanescu_A_2010_with_Nest/run_cluster/Piz_Daint/asynchronous/test_2/sim.py
folder_data=$SCRATCH/tng/lionel/save_data/${NAME_SIMULATION}/

# program
echo ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}
shifter --debug run ${name_image} python ${run_simulation} ${folder_data} ${SLURM_ARRAY_TASK_ID} ${BEGIN} ${END}
