from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable = {'conn_sigma_excitatory_long': np.array([
					#0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4
					0.01,0.2,0.4,0.6,2.6,2.8,3.0,3.2,3.4
					]),
             'tau_long': np.arange(80.0, 320.0, 10.0),
	     }

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=1.5
    parameter_grid.param_connexion['g']=0.8
    parameter_grid.param_connexion['weight_excitatory']=1.3
    parameter_grid.param_connexion['weight_long']=2.6
    parameter_grid.param_topology['sigma_I_ext']=0.0
    parameter_grid.param_topology['dim_x']=3.0
    parameter_grid.param_topology['dim_y']=3.0
    parameter_grid.param_topology['sigma_V_0']=100.0
    parameter_grid.param_topology['mean_w_0']=10.0
    parameter_grid.param_topology['mean_I_ext']=700.0
    parameter_grid.param_topology['long_range']=True
    parameter_grid.param_background['noise']=False
    parameter_grid.param_background['sigma_noise']=50.0
    parameter_grid.param_nest['master_seed']=46
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
