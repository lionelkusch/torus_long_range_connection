import sys
sys.path.append('/scratch/snx3000/bp000176/tng/lionel/')
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.run_exploration import analysis
from param import parameter, range_element, name_variable

def analysis_one(path,n,data_base,table_name,begin,end):
    dict_variable = {}
    element = range_element()[n]
    results_path = path
    for i,name in enumerate(name_variable()):
        dict_variable[name]= element[i]
        results_path+='_'+name+'_'+str(element[i])
    print('folder'+results_path)
    param = parameter()
    param.param_nest['local_num_threads'] = 1
    analysis(results_path,parameter(),data_base,table_name,dict_variable,begin,end)


if __name__ == "__main__":
    import sys
    if len(sys.argv)==7:
        analysis_one(sys.argv[1],int(sys.argv[2]),sys.argv[3],sys.argv[4],float(sys.argv[5]),float(sys.argv[6]))
    else:
        print('missing argument')
