from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable = {'weight_excitatory': np.array([
					   2.5,3.0,3.5,4.0,4.5,
					   5.0,5.5,6.0,6.5
					]),
             'tau_long': np.arange(80.0,330.0, 10.0),
	     }

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=1.5
    parameter_grid.param_connexion['g']=1.0
    parameter_grid.param_topology['sigma_I_ext']=0.0
    parameter_grid.param_topology['sigma_V_0']=100.0
    parameter_grid.param_topology['mean_w_0']=10.0
    parameter_grid.param_topology['mean_I_ext']=700.0
    parameter_grid.param_topology['long_range']=True
    parameter_grid.param_background['noise']=True
    parameter_grid.param_background['sigma_noise']=50.0
    parameter_grid.param_nest['master_seed']=46
    parameter_grid.param_topology['percentage_inactive_neurons']=0.1
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()
