#!/bin/sh
export LC_ALL=en_US.UTF-8

BEGIN=2000
END=12000
# export vars
echo ${NAME_SIMULATION} ${BEGIN} ${END}

# internal vars
export PYTHONPATH=/home/lionelkusch/:$PYTHONPATH
nest_simulator=/home/lionelkusch/Jirsa_V_Stefanescu_A_2010_with_Nest/singularity_run/Nest_simulator
run_simulation=/home/lionelkusch/Jirsa_V_Stefanescu_A_2010_with_Nest/run_cluster/big_layer/g_t_ref_0_4/analysis.py
folder_data=/home/lionelkusch/save_data/${NAME_SIMULATION}/
database=/home/lionelkusch/save_data/${NAME_SIMULATION}/database.db
table="exploration_1"

# program
${nest_simulator} ${run_simulation} ${folder_data} ${database} ${table} ${BEGIN} ${END}
