from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable =  {'g': np.arange(1.0, 6.0, 1.0),
             'mean_I_ext': np.arange(650.0, 1000.0, 50.0)}

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=0.4
    parameter_grid.param_topology['sigma_I_ext']=1.0
    parameter_grid.param_topology['long_range']=False
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()