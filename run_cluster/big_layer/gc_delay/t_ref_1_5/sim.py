from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.run_exploration import run
from param import parameter, name_variable, range_element

def run_exploration(path,n,begin,end):
    dict_variable = {}
    element = range_element()[n]
    results_path = path
    for i,name in enumerate(name_variable()):
        dict_variable[name]= element[i]
        results_path+='_'+name+'_'+str(element[i])
    print('folder'+results_path)
    params = parameter()
    params.param_connexion['weight_long']=params.param_connexion['weight_excitatory']
    run(results_path, params, dict_variable, begin, end, print_volt=False)

if __name__ == "__main__":
    import sys
    if len(sys.argv)==5:
        run_exploration(sys.argv[1],int(sys.argv[2]),float(sys.argv[3]),float(sys.argv[4]))
    else:
        print('missing argument')
