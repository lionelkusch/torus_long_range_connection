from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.run_exploration import run_exploration_2D
from param import parameter, dict_variable

def run_exploration(path,data_base,table_name,begin,end,print_volt=False):
    run_exploration_2D(path, parameter(), data_base, table_name, dict_variable() , begin, end, print_volt,simulation=False)

if __name__ == "__main__":
    import sys
    if len(sys.argv)==6:
        run_exploration(sys.argv[1],sys.argv[2],sys.argv[3],float(sys.argv[4]),float(sys.argv[5]))
    elif len(sys.argv)==7:
        run_exploration(sys.argv[1],sys.argv[2],sys.argv[3],float(sys.argv[4]),float(sys.argv[5]),bool(sys.argv[6]))
    elif len(sys.argv)==1:
        run_exploration( './big_network/', 'database.db', 'exploration', 2000.0, 12000.0)
    else:
        print('missing argument')
