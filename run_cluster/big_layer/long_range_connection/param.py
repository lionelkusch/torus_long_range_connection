from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_grid
import numpy as np
import itertools

variable = {'tau_long': np.arange(80.0, 500.0, 50.0),
             'weight_long': np.arange(5.0,31.0,5.0)}

def parameter():
    parameter_grid.param_topology['param_neuron']['t_ref']=1.5
    parameter_grid.param_connexion['g']=1.0
    parameter_grid.param_topology['sigma_I_ext']=1.0
    parameter_grid.param_topology['mean_I_ext']=700.0
    return parameter_grid

def range_element():
    return list(itertools.product(variable.values()[0],variable.values()[1]))

def dict_variable():
    return variable

def name_variable():
    return variable.keys()