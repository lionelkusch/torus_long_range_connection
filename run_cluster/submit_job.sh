#!/bin/sh

# define SLURM params
walltime="--time 24:00:00"
min_config_cpu="--mincpus=4"
nb_node="--nodes=1"
nb_processus_by_job="--ntasks=1"
nb_cpu_by_task="--cpus-per-task=4"
nb_task_by_cpu="--ntasks-per-core=1"
nb_thread_by_core="--threads-per-core=1"
politic_thread_allocation="--hint=compute_bound"
memory="--mem=4G"

option_SLURM=${walltime} ${min_config_cpu} ${nb_node} ${nb_processus_by_job} ${nb_cpu_by_task} ${nb_task_by_cpu} ${nb_thread_by_core} ${politic_thread_allocation} ${memory}

# define params explorating
NAME_SIMULATION='first_experience'

min_sigma=0.0
max_sigma=4.0
stepsize_sigma=0.1

min_tau_long=0.
max_tau_long=6.
stepsize_tau_long=0.5

# define bash loop
for sigma in $(seq ${min_sigma} ${stepsize_sigma} ${max_sigma}); do
    for tau_long in $(seq ${min_tau_long} ${stepsize_tau_long} ${max_tau_long}); do
        echo ${sigma}
        echo ${tau_long}

        # create export commands
        exvars="--export=NAME_SIMULATION=${NAME_SIMULATION},SIGMA=${sigma},TAU_LONG=${tau_long}"
        
        # set a name to the job
        jobname="--job-name=${NAME_SIMULATION}_sigma${sigma}_tau_long${tau_long}"
        
        stdout="--output slurm_out/${NAME_SIMULATION}_sigma${sigma}_tau_long${tau_long}.out"
        stderr="--error slurm_err/${NAME_SIMULATION}_sigma${sigma}_tau_long${tau_long}.err"
        
        # build a scavenger job, gpu job, or other job
        echo ${option_SLURM} ${exvars} ${jobname} ${stdout} ${stderr} ./submit_pys.sh
        printf "Sbatch should run now\n"

        # submit slurm bash file
        sbatch ${option_SLURM} ${exvars} ${jobname} ${stdout} ${stderr} ./submit_pys.sh
        printf "Sbatch run now\n"

    done
done

