#!/bin/sh
export LC_ALL=en_US.UTF-8

# export vars
echo ${NAME_SIMULATION} ${SIGMA} ${TAU_LONG} ${BEGIN} ${END}

# internal vars
nest_simulator=/home/lionelkusch/Jirsa_V_Stefanescu_A_2010_with_Nest/singularity_run/Nest_simulator
run_simulation=/home/lionelkusch/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run.py
folder_data=/home/lionelkusch/save_data/${NAME_SIMULATION}/${SIGMA}_${TAU_LONG}
database=/home/lionelkusch/save_data/${NAME_SIMULATION}/database.db
table=${SIGMA}_${TAU_LONG}

# program
${nest_simulator} ${run_simulation} ${folder_data} ${database} ${table} ${SIGMA} ${TAU_LONG} ${BEGIN} ${END}
