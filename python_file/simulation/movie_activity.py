import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from matplotlib import gridspec
from analysis_local import get_gids, load_spike,compute_synchronize_R
import numpy as np
import os
from copy import copy

def transfom_spike(gid, spike, begin, end,resolution):
        spikes = []
        id_spikes_threshold = []
        for i in np.arange(gid[0], gid[1], 1):
            spike_i = spike[np.where(spike[:, 0] == i)]
            spike_i = (spike_i[np.where(spike_i[:, 1] >= begin), 1]).flatten()
            spike_i = (spike_i[np.where(spike_i <= end)])
            spikes.append(spike_i)
            if np.shape(spike_i)[0] > 20: #Filter neurons with enough spikes
                id_spikes_threshold.append([len(spikes)-1,len(spike_i)])
        return spikes,id_spikes_threshold

def R_list(spikes,id_spikes_threshold,resolution):
        if id_spikes_threshold:
            R_list,R_times=compute_synchronize_R(spikes,id_spikes_threshold,resolution)
        else:
            R_list =[]
            R_times=[-1,-1]
        return R_times,R_list

def hist_all_list(spikes,begin,end,resolution):
        spikes.append([begin,end])
        spikes_concat = np.concatenate(spikes)
        if int(end - begin) > 0:
            hist_0 = np.histogram(spikes_concat, bins=int((end - begin)/resolution))  # for bins at 1 milisecond
            hist_0[0][0]-=1
            hist_0[0][-1]-=1
        else:
            hist_0 = None
        if int(end - begin) > 0:
            hist_1 = np.histogram(spikes_concat, bins=int(end - begin))  # for bins at 1 milisecond
            hist_1[0][0]-=1
            hist_1[0][-1]-=1
        else:
            hist_1 = None
        if int(end - begin) > 3:
            hist_3 = np.histogram(spikes_concat, bins=int((end - begin) / 3))  # for bins at 3 milisecond
            hist_3[0][0]-=1
            hist_3[0][-1]-=1
        else:
            hist_3 = None
        return hist_3,hist_1,hist_0

def hist_01_list(spikes,begin,end,resolution):
        spikes.append([begin,end])
        spikes_concat = np.concatenate(spikes)
        if int(end - begin) > 0:
            hist_0 = np.histogram(spikes_concat, bins=int((end - begin)/resolution))  # for bins at 1 milisecond
            hist_0[0][0]-=1
            hist_0[0][-1]-=1
        else:
            hist_0 = None
        return hist_0

def compute_all_element(gids,data,begin,end,resolution):
    list_Rs_rev = []
    list_hist_0_3_rev = []
    list_hist_0_1_rev = []
    list_hist_0_001_rev = []
    for h in list(range(len(gids))):
        print('h',h)
        spikes,id_spikes_threshold= transfom_spike(gids[h], data[h], begin, end, resolution)
        # R_times,R_lists= R_list(spikes,id_spikes_threshold,resolution)
        hist_0_3, hist_0_1,hist_0_001 = hist_all_list(spikes,begin,end,resolution)
        # hist
        list_hist_0_3_rev.append(hist_0_3)

        # hist
        list_hist_0_1_rev.append(hist_0_1)

        # hist
        list_hist_0_001_rev.append(hist_0_001)

        # R
        # list_Rs_rev.append([R_times[0],R_times[1],R_lists])
    return list_hist_0_3_rev,list_hist_0_1_rev,list_hist_0_001_rev,list_Rs_rev

def compute_hist_01_element(gids,data,begin,end,resolution):
    list_hist_0_001_rev = []
    for h in list(range(len(gids))):
        spikes,id_spikes_threshold= transfom_spike(gids[h], data[h], begin, end, resolution)
        hist_0_001 = hist_01_list(spikes,begin,end,resolution)

        # hist
        list_hist_0_001_rev.append(hist_0_001)

    return list_hist_0_001_rev

def analysis_local(path,begin,end,resolution,limit_burst,name,delay,long_range=True):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    # #take the data
    gids = get_gids(path)
    # gids=[[2, 162], [204, 364]]
    begin = begin-delay-10.0
    # data_all = load_spike(gids,path, begin, end)
    # if data_all == -1:
    #     return -1
    # np.save(path+'/data_all.npy',np.array(data_all))
    # print('save data')
    # data_all = np.load(path+'./data_all.npy')
    # list_hist_0_3_rev,list_hist_0_1_rev,list_hist_0_001_rev,list_Rs_rev = compute_all_element(gids,data_all,begin,end,resolution)

    # list_Rs_rev = np.load(path+'R.npy')
    # fig = plt.figure()
    # for i in range(len(list_Rs_rev)):
    #     print(i)
    #     plt.hold(True)
    #     plt.plot(range(len(list_Rs_rev[i])),np.array(list_Rs_rev[i])+i)
    # plt.show()
    #
    # hist3 = np.array([a[0] for a in list_hist_0_3_rev])
    # np.save(path+'hist3.npy', hist3)
    # print('save hist3')
    # hist1 = np.array([a[0] for a in list_hist_0_1_rev])
    # np.save(path+'hist1.npy', hist1)
    # print('save hist1')
    # hist01 = np.array([a[0] for a in list_hist_0_001_rev])
    # np.save(path+'hist01.npy', hist01)
    # print('save hist01')

    # list_hist_0_001_rev = np.load(path+'hist01.npy')
    # list_hist_0_001_rev[55]+=list_hist_0_001_rev[201]
    # fig = plt.figure(figsize=(20, 20))
    # plt.hold(True)
    # for j,i in enumerate([55,155,11,125]):
    #     plt.plot(np.arange(2000.0,12000.0,0.1),list_hist_0_001_rev[i]+list_hist_0_001_rev[i+200]+j*200)
    # ax = plt.gca()
    # ax.tick_params(axis='both', labelsize=20.0)
    # ax.set_yticks([100.0,300.0,500.0,700.0])
    # ax.set_yticklabels(['src','target','random_1','random_2'],fontdict={"fontsize":20.0})
    # ax.set_xlabel('time',{"fontsize":30.0})
    # ax.set_ylabel('NB spikes',{"fontsize":30.0})
    # ax.set_title('instantaneous firing rate\n with bins = 0.1ms',{"fontsize":30.0})
    # plt.savefig(path+'hist_pop_01.png')

    # list_hist_0_1_rev = np.load(path+'hist1.npy')
    # list_hist_0_1_rev[55]+=list_hist_0_1_rev[201]
    # fig = plt.figure(figsize=(20, 20))
    # plt.hold(True)
    # for j,i in enumerate([55,155,11,125]):
    #     plt.plot(np.arange(2000.0,12000.0,1.0),list_hist_0_1_rev[i]+list_hist_0_1_rev[i+200]+j*200)
    # ax = plt.gca()
    # ax.tick_params(axis='both', labelsize=20.0)
    # ax.set_yticks([100.0,300.0,500.0,700.0])
    # ax.set_yticklabels(['src','target','random_1','random_2'],fontdict={"fontsize":20.0})
    # ax.set_xlabel('time',{"fontsize":30.0})
    # ax.set_ylabel('NB spikes',{"fontsize":30.0})
    # ax.set_title('instantaneous firing rate\n with bins = 1ms',{"fontsize":30.0})
    # plt.savefig(path+'hist_pop_1.png')


    # list_hist_0_3_rev = np.load(path+'hist3.npy')
    # list_hist_0_3_rev[55]+=list_hist_0_3_rev[201]
    # fig = plt.figure(figsize=(20, 20))
    # plt.hold(True)
    # for j,i in enumerate([55,155,11,125]):
    #     plt.plot(np.arange(2000.0,11997.0,3.0),list_hist_0_3_rev[i]+list_hist_0_3_rev[i+200]+j*200)
    # ax = plt.gca()
    # ax.tick_params(axis='both', labelsize=20.0)
    # ax.set_yticks([100.0,300.0,500.0,700.0])
    # ax.set_yticklabels(['src','target','random_1','random_2'],fontdict={"fontsize":20.0})
    # ax.set_xlabel('time',{"fontsize":30.0})
    # ax.set_ylabel('NB spikes',{"fontsize":30.0})
    # ax.set_title('instantaneous firing rate\n with bins = 3ms',{"fontsize":30.0})
    # plt.savefig(path+'hist_pop_3.png')


    #
    # begin_R=[]
    # end_R=[]
    # R=[]
    # for element in list_Rs_rev:
    #     begin_R.append(element[0])
    #     end_R.append(element[1])
    #     R.append(element[2])
    # list_R = np.array(R)
    # np.save(path+'R.npy', list_R)
    # print('save R')
    # list_begin_R = np.array(begin_R)
    # np.save(path+'R_begin.npy', list_begin_R)
    # print('save R begin')
    # list_end_R = np.array(end_R)
    # np.save(path+'R_end.npy', list_end_R)
    # print('save R end')

    hist01 = np.load(path+'/hist_'+str(resolution)+'.npy')

    # list_hist_0_001_rev = compute_hist_01_element(gids,data_all,begin,end,resolution)
    # hist01 = np.array([a[0] for a in list_hist_0_001_rev])
    # np.save(path+'hist_'+str(resolution)+'.npy', hist01)
    # print('save hist01')

    if long_range:
        hist01[55,:]=hist01[55,:]+hist01[200,:]
    if not os.path.exists(path+"/"+str(resolution)+"/"):
            os.makedirs(path+"/"+str(resolution)+"/")

    my_dpi=100
    # parameter fig
    src_x = 5
    src_y = 4
    target_x = 15
    target_y = 4
    x_src = [src_x, src_x, src_x + 1, src_x + 1, src_x]  # abscisses des sommets
    y_src = [src_y, src_y + 1, src_y + 1, src_y, src_y]  # ordonnees des sommets
    x_target = [target_x, target_x, target_x + 1, target_x + 1, target_x]  # abscisses des sommets
    y_target = [target_y, target_y + 1, target_y + 1, target_y, target_y]  # ordonnees des sommets
    text_fontsize = 20.0
    title_fontsize = 20.0

    begin_init = copy(begin)
    end_init = copy(end)
    begin = int((delay + 10)/resolution)
    size = int((end_init-begin_init-delay-10)/resolution)
    fig_1 = plt.figure(figsize=(2000/my_dpi, 800/my_dpi), dpi=my_dpi)
    def update_fig(i):
        print(i)
        fig_1.clf()
        gs = gridspec.GridSpec(2, 3, width_ratios=[1, 1, 20])
        ax1 = fig_1.add_subplot(gs[0, 0])
        ax2 = fig_1.add_subplot(gs[1, 0])
        ax3 = fig_1.add_subplot(gs[0, 1])
        ax4 = fig_1.add_subplot(gs[1, 1])
        ax5 = fig_1.add_subplot(gs[:, 2])
        # source
        if begin+i-delay >= 0:
            src_data_previous = np.ones((1, 1)) * ([hist01[256, begin+i-delay]])
        else :
            src_data_previous= np.ones((1, 1)) * np.NAN
        src_data = np.ones((1,1))*([hist01[256,begin+i]])
        ax1.text(1.0,-1.0,'source',fontdict={'fontsize':text_fontsize})
        ax1.matshow(src_data_previous, vmin = 0.0, vmax =40.0,extent=[0.0,1.0,1.0,0.0],cmap=plt.get_cmap('hsv'),animated=True)
        ax1.set_yticklabels(range(src_y,src_y+2),{'fontsize':text_fontsize})
        ax1.set_xticklabels(range(src_x,src_x+2),{'fontsize':text_fontsize})
        ax1.text(0.0,2.0,'t='+str((begin+i)*resolution-delay+begin_init),fontdict={'fontsize':text_fontsize - 5.0})
        ax3.matshow(src_data, vmin=0.0, vmax=40.0, extent=[0.0, 1.0, 1.0, 0.0],cmap=plt.get_cmap('hsv'),animated=True)
        ax3.set_yticklabels(range(src_y, src_y+2), {'fontsize': text_fontsize})
        ax3.set_xticklabels(range(src_x, src_x+2), {'fontsize': text_fontsize})
        ax3.text(0.0,2.0, 't=' + str((begin+i)*resolution+begin_init), fontdict={'fontsize': text_fontsize - 5.0})

        #target
        if begin+i-delay >= 0:
            target_data_previous = np.ones((1, 1)) * ([hist01[356, begin+i-delay]])
        else :
            target_data_previous= np.ones((1, 1)) * np.NAN
        target_data =np.ones((1,1))*([hist01[356,begin+i]])
        ax2.text(1.0,-1.0,'target', fontdict={'fontsize': text_fontsize})
        ax2.matshow(target_data_previous, vmin = 0.0, vmax =40.0,extent=[0.0,1.0,1.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        ax2.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
        ax2.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
        ax2.text(0.0,2.0,'t='+str((begin+i)*resolution-delay+begin_init),fontdict={'fontsize':text_fontsize - 5.0})
        ax4.matshow(target_data, vmin = 0.0, vmax =40.0,extent=[0.0,1.0,1.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        ax4.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
        ax4.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
        ax4.text(0.0,2.0,'t='+str((begin+i)*resolution+begin_init),fontdict={'fontsize':text_fontsize - 5.0})

        #plot
        if long_range :
            data = np.transpose(np.reshape(hist01[201:,begin+i],(20,10)))
        else:
            data = np.transpose(np.reshape(hist01[200:,begin+i],(20,10)))
        im = ax5.matshow( data ,vmin = 0.0, vmax =40.0,extent=[0.0,20.0,10.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        col = fig_1.colorbar(im, cax = fig_1.add_axes([0.91, 0.2, 0.05, 0.6]))
        col.set_ticklabels(range(0,41),{'fontsize':text_fontsize})
        ax5.set_yticks(range(0,10))
        ax5.set_yticklabels(range(0,10),{'fontsize':text_fontsize})
        ax5.set_xticks(range(0,21))
        ax5.set_xticklabels(range(0,21),{'fontsize':text_fontsize})
        ax5.grid(color='r', linestyle='--', linewidth=0.3)

        ax5.plot(x_src,y_src,'b', linewidth=2.0)
        ax5.plot(x_target,y_target,'b', linewidth=2.0)
        ax5.text(0.0,-1.0,name+ ' t='+str((begin+i)*resolution+begin_init)+' ms',fontdict={'fontsize':title_fontsize})
        return [fig_1]
    anim = manimation.FuncAnimation(fig_1, update_fig,frames=np.arange(0,size,1), blit=True)
    anim.save(path+"/"+str(resolution)+"/inhibitory.mp4")

    fig_2 = plt.figure(figsize=(2000/my_dpi, 800/my_dpi), dpi=my_dpi)
    def update_fig(i):
        print(i)
        fig_2.clf()
        gs = gridspec.GridSpec(2, 3, width_ratios=[1, 1, 20])
        ax1 = fig_2.add_subplot(gs[0, 0])
        ax2 = fig_2.add_subplot(gs[1, 0])
        ax3 = fig_2.add_subplot(gs[0, 1])
        ax4 = fig_2.add_subplot(gs[1, 1])
        ax5 = fig_2.add_subplot(gs[:, 2])
        # source
        if begin+i-delay >= 0:
            src_data_previous = np.ones((1, 1)) * ([hist01[55, begin+i-delay]])
        else :
            src_data_previous= np.ones((1, 1)) * np.NAN
        src_data = np.ones((1,1))*([hist01[55,begin+i]])
        ax1.text(1.0,-1.0,'source',fontdict={'fontsize':text_fontsize})
        ax1.matshow(src_data_previous, vmin = 0.0, vmax =160.0,extent=[0.0,1.0,1.0,0.0],cmap=plt.get_cmap('hsv'),animated=True)
        ax1.set_yticklabels(range(src_y,src_y+2),{'fontsize':text_fontsize})
        ax1.set_xticklabels(range(src_x,src_x+2),{'fontsize':text_fontsize})
        ax1.text(0.0,2.0,'t='+str((begin+i)*resolution-delay+begin_init),fontdict={'fontsize':text_fontsize - 5.0})
        ax3.matshow(src_data, vmin=0.0, vmax=160.0, extent=[0.0, 1.0, 1.0, 0.0],cmap=plt.get_cmap('hsv'),animated=True)
        ax3.set_yticklabels(range(src_y, src_y+2), {'fontsize': text_fontsize})
        ax3.set_xticklabels(range(src_x, src_x+2), {'fontsize': text_fontsize})
        ax3.text(0.0,2.0, 't=' + str((begin+i)*resolution+begin_init), fontdict={'fontsize': text_fontsize - 5.0})

        #target
        if begin+i-delay >= 0:
            target_data_previous = np.ones((1, 1)) * ([hist01[155, begin+i-delay]])
        else :
            target_data_previous= np.ones((1, 1)) * np.NAN
        target_data =np.ones((1,1))*([hist01[155,begin+i]])
        ax2.text(1.0,-1.0,'target', fontdict={'fontsize': text_fontsize})
        ax2.matshow(target_data_previous, vmin = 0.0, vmax =160.0,extent=[0.0,1.0,1.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        ax2.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
        ax2.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
        ax2.text(0.0,2.0,'t='+str((begin+i)*resolution-delay+begin_init),fontdict={'fontsize':text_fontsize - 5.0})
        ax4.matshow(target_data, vmin = 0.0, vmax =160.0,extent=[0.0,1.0,1.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        ax4.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
        ax4.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
        ax4.text(0.0,2.0,'t='+str((begin+i)*resolution+begin_init),fontdict={'fontsize':text_fontsize - 5.0})

        #plot
        data = np.transpose(np.reshape(hist01[:200,begin+i],(20,10)))
        im = ax5.matshow( data ,vmin = 0.0, vmax =160.0,extent=[0.0,20.0,10.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        col = fig_2.colorbar(im, cax = fig_1.add_axes([0.91, 0.2, 0.05, 0.6]))
        col.set_ticklabels(range(0,41),{'fontsize':text_fontsize})
        ax5.set_yticks(range(0,10))
        ax5.set_yticklabels(range(0,10),{'fontsize':text_fontsize})
        ax5.set_xticks(range(0,21))
        ax5.set_xticklabels(range(0,21),{'fontsize':text_fontsize})
        ax5.grid(color='r', linestyle='--', linewidth=0.3)

        ax5.plot(x_src,y_src,'b', linewidth=2.0)
        ax5.plot(x_target,y_target,'b', linewidth=2.0)
        ax5.text(0.0,-1.0,name+ ' t='+str((begin+i)*resolution+begin_init)+' ms',fontdict={'fontsize':title_fontsize})
        return [fig_2]
    anim = manimation.FuncAnimation(fig_2, update_fig,frames=np.arange(0,size,1), blit=True)
    anim.save(path+"/"+str(resolution)+"/excitatory.mp4")
    print('END')

    fig_3 = plt.figure(figsize=(2000/my_dpi, 800/my_dpi), dpi=my_dpi)
    def update_fig(i):
        print(i)
        fig_3.clf()
        gs = gridspec.GridSpec(2, 3, width_ratios=[1, 1, 20])
        ax1 = fig_3.add_subplot(gs[0, 0])
        ax2 = fig_3.add_subplot(gs[1, 0])
        ax3 = fig_3.add_subplot(gs[0, 1])
        ax4 = fig_3.add_subplot(gs[1, 1])
        ax5 = fig_3.add_subplot(gs[:, 2])
        # source
        if begin+i-delay >= 0:
            src_data_previous = np.ones((1, 1)) * ([hist01[256, begin+i-delay]+hist01[55,begin+i-delay]])
        else :
            src_data_previous= np.ones((1, 1)) * np.NAN
        src_data = np.ones((1,1))*([hist01[256,begin+i]+hist01[55,begin+i]])
        ax1.text(1.0,-1.0,'source',fontdict={'fontsize':text_fontsize})
        ax1.matshow(src_data_previous, vmin = 0.0, vmax =200.0,extent=[0.0,1.0,1.0,0.0],cmap=plt.get_cmap('hsv'),animated=True)
        ax1.set_yticklabels(range(src_y,src_y+2),{'fontsize':text_fontsize})
        ax1.set_xticklabels(range(src_x,src_x+2),{'fontsize':text_fontsize})
        ax1.text(0.0,2.0,'t='+str((begin+i)*resolution-delay+begin_init),fontdict={'fontsize':text_fontsize - 5.0})
        ax3.matshow(src_data, vmin=0.0, vmax=200.0, extent=[0.0, 1.0, 1.0, 0.0],cmap=plt.get_cmap('hsv'),animated=True)
        ax3.set_yticklabels(range(src_y, src_y+2), {'fontsize': text_fontsize})
        ax3.set_xticklabels(range(src_x, src_x+2), {'fontsize': text_fontsize})
        ax3.text(0.0,2.0, 't=' + str((begin+i)*resolution+begin_init), fontdict={'fontsize': text_fontsize - 5.0})

        #target
        if begin+i-delay >= 0:
            target_data_previous = np.ones((1, 1)) * ([hist01[356, begin+i-delay]+hist01[155,begin+i-delay]])
        else :
            target_data_previous= np.ones((1, 1)) * np.NAN
        target_data =np.ones((1,1))*([hist01[356,begin+i]+hist01[155,begin+i]])
        ax2.text(1.0,-1.0,'target', fontdict={'fontsize': text_fontsize})
        ax2.matshow(target_data_previous, vmin = 0.0, vmax =200.0,extent=[0.0,1.0,1.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        ax2.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
        ax2.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
        ax2.text(0.0,2.0,'t='+str((begin+i)*resolution-delay+begin_init),fontdict={'fontsize':text_fontsize - 5.0})
        ax4.matshow(target_data, vmin = 0.0, vmax =200.0,extent=[0.0,1.0,1.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        ax4.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
        ax4.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
        ax4.text(0.0,2.0,'t='+str((begin+i)*resolution+begin_init),fontdict={'fontsize':text_fontsize - 5.0})

        #plot
        if long_range:
            data = np.transpose(np.reshape(hist01[0:200,begin+i]+hist01[201:401,begin+i],(20,10)))
        else:
            data = np.transpose(np.reshape(hist01[0:200,begin+i]+hist01[200:401,begin+i],(20,10)))
        im = ax5.matshow( data ,vmin = 0.0, vmax =200.0,extent=[0.0,20.0,10.0,0.0],origin='lower',cmap=plt.get_cmap('hsv'),animated=True)
        col = fig_3.colorbar(im, cax = fig_1.add_axes([0.91, 0.2, 0.05, 0.6]))
        col.set_ticklabels(range(0,41),{'fontsize':text_fontsize})
        ax5.set_yticks(range(0,10))
        ax5.set_yticklabels(range(0,10),{'fontsize':text_fontsize})
        ax5.set_xticks(range(0,21))
        ax5.set_xticklabels(range(0,21),{'fontsize':text_fontsize})
        ax5.grid(color='r', linestyle='--', linewidth=0.3)

        ax5.plot(x_src,y_src,'b', linewidth=2.0)
        ax5.plot(x_target,y_target,'b', linewidth=2.0)
        ax5.text(0.0,-1.0,name+ ' t='+str((begin+i)*resolution+begin_init)+' ms',fontdict={'fontsize':title_fontsize})
    anim = manimation.FuncAnimation(fig_3, update_fig,frames=np.arange(0,size,1), blit=True)
    anim.save(path+"/"+str(resolution)+"/both.mp4")
    print('END')
analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_1800_mean_400_g_0_1_test_init/_weight_excitatory_8.0_weight_long_100.0/',
               11000,12000,0.1,10.0,'delay : 120.0, g: 0.1 and local strength : 8.0',1,True)
# analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network/_weight_excitatory_7.5_mean_noise_400.0/',
#                11000.0,12000.0,1.0,10.0,'delai : 0.0, g: 0.1 and local strength : 7.5',0,True)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low_2/_g_0.04_tau_long_120.0/',
#                8000.0,10000.0,1.0,10.0,'delay : 120.0, g: 0.04 and local strength : 6.0',120,True)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_limit/_weight_excitatory_18.0_tau_long_120.0/',
#                11000.0,12000.0,1.0,10.0,'delai : 120.0 and local strength : 18.0',120,True)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/sub/sub_noise_gc/_weight_excitatory_25.0_sigma_noise_600.0/',
#                11900.0,12000.0,1.0,10.0,'noise : 600.0 and local strength : 25.0',0,False)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_110.0/',
#                219900.0,220000.0,0.1,10.0,'local strength : 1.3 and delay long range : 110.0',110)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_160.0/',
#                219900.0,220000.0,0.1,10.0,'local strength : 1.3 and delay long range : 160.0',160)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_140.0/',
#                11900.0,12000.0,0.1,10.0,'local strength : 24.0 and delay long range : 140.0',140)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_100.0/',
#                11900.0,12000.0,0.1,10.0,'local strength : 24.0 and delay long range : 100.0',100)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.2_tau_long_140.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 1.2 and delay long range : 140.0',140)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.2_tau_long_120.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 1.2 and delay long range : 120.0',120)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.6_tau_long_140.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 1.6 and delay long range : 140.0',140)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.6_tau_long_120.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 1.6 and delay long range : 120.0',120)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_0.8_tau_long_140.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 0.8 and delay long range : 140.0',140)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_0.8_tau_long_120.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 0.8 and delay long range : 120.0',120)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_140.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 24.0 and delay long range : 140.0',140)
# analysis_local('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_100.0/',
#                11000.0,12000.0,0.1,10.0,'local strength : 24.0 and delay long range : 100.0',100)
#thinkness
# analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.0/',
#                10000,12000,0.1,10.0,'thinkness = 0.0 master seed =42',1200)
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.1/',
#                    10000,12000,0.1,10.0,'thinkness = 0.1 master seed =42',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.2/',
#                10000,12000,0.1,10.0,'thinkness = 0.0 master seed =42',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.7/',
#                10000,12000,0.1,10.0,'thinkness = 0.7 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.8/',
#                10000,12000,0.1,10.0,'thinkness = 0.8 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.9/',
#                10000,12000,0.1,10.0,'thinkness = 0.9 master seed =46',1200)
# except:
#     pass
#gc
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.11_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.11 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.11_master_seed_42/',
#                11500,12000,0.1,10.0,'gc = 0.11 master seed =42',1200)
# except:
#     pass
# try:
#     analysis_local(
#         '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.19_master_seed_46/',
#         11500, 12000, 0.1, 10.0, 'gc = 0.19 master seed =46', 1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.02_master_seed_42/',
#                11500,12000,0.1,10.0,'gc = 0.02 master seed =42',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.02_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.02 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.03_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.03 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.03_master_seed_42/',
#                11500,12000,0.1,10.0,'gc = 0.03 master seed =42',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.04_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.04 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.05_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.05 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.06_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.06 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.09_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.09 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.1_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.10 master seed =46',1200)
# except:
#     pass
#
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.12_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.12 master seed =46',1200)
# except:
#     pass
# try :
#     analysis_local('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.16_master_seed_46/',
#                11500,12000,0.1,10.0,'gc = 0.16 master seed =46',1200)
# except:
#     pass
