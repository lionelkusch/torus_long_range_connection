from elephant.spike_train_correlation import corrcoef,covariance
from neo.core import SpikeTrain
from quantities import ms
from elephant.conversion import BinnedSpikeTrain
import numpy as np
import matplotlib.pylab as plt

def get_gids(path):
    """
    Get the id of the different neurons in the network by population
    :param path: the path to the file
    :return: an array with the range of id for the different population
    """
    gidfile = open(path + '/population_GIDs.dat', 'r')
    gids = []
    for l in gidfile:
        a = l.split()
        gids.append([int(a[0]), int(a[1])])
    return gids


def load_spike_ids(gids,path, begin, end):
    """
    Get the id of the neurons which create the spike
    :param gids: the array of the id
    :param path: the path to the file
    :param begin: the first time
    :param end: the end of time
    :return: The spike of all neurons between end and begin
    """
    data = []
    data_concatenated = np.loadtxt(path + "/spike_detector.gdf")
    data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
    idx_time = ((data_raw[:, 1] >= begin) * (data_raw[:, 1] <= end))
    data_tmp = data_raw[idx_time]
    for i in list(range(len(gids))):
        idx_id = ((data_tmp[:, 0] >= gids[i][0]) * (data_tmp[:, 0] <= gids[i][1]))
        data.append(data_tmp[idx_id])
    return data

def transfom_spike_global(gids, data, begin, end):
    """
    Compute different measure on the spike
    :param gids: the array with the id
    :param data: the times spikes
    :param begin: the first time
    :param end: the end time
    :return:
        synchronisation : hist_0_1, hist_0_3,
        irregularity : cv_list, lv_list,
        the percentage of spike analyse for the irregularity : percentage
    """
    nb_neuron = 0
    for gid in gids:
        nb_neuron = nb_neuron + gid[1] - gid[0]
    spikes = []
    spikes_bin = []
    for j,gid in enumerate(gids):
        for i in np.arange(gid[0], gid[1], 1):
            spike_i = data[j][np.where(data[j][:, 0] == i)]
            spike_i = (spike_i[np.where(spike_i[:, 1] >= begin), 1]).flatten()
            spike_i = (spike_i[np.where(spike_i <= end)])
            spikes.append(spike_i)
            if np.shape(spike_i)[0] > 20: #Filter neurons with enough spikes
                spikes_bin.append(SpikeTrain(spike_i*ms,t_start=begin,t_stop=end))
    print("end spiketrain")
    bin=BinnedSpikeTrain(spikes_bin, binsize=0.1*ms)
    print("Bin")
    # need to paralize this function
    cov = covariance(bin)
    print('cov')
    #need to paralize this function
    corr = corrcoef(bin)
    print('corr')
    return [cov,corr]

def print_matrix_covariance_correlation(path,begin,end):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :return: The different measure on the spike
    """
    #take the data
    gids = get_gids(path)
    data_all = load_spike_ids(gids,path, begin, end)
    # modification of end and begin in order to be independant of parameter
    # However this modification will increase the rate of the neurons
    end = np.max(np.concatenate(data_all,axis=0)[:,1])
    begin = np.min(np.concatenate(data_all,axis=0)[:,1])
    cov,corr=transfom_spike_global(gids, data_all, begin, end)
    plt.figure(figsize=(18, 8))
    plt.matshow(cov)
    plt.title('Covariance')
    plt.figure(figsize=(18, 8))
    plt.matshow(corr)
    plt.title('Correlation')

print_matrix_covariance_correlation('./PG_test_6',0.0,2000.0)