import numpy as np
import matplotlib.pylab as plt
import time
import nest.topology as tp
import nest
from copy import copy
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.simulation_ring import network_initialisation,network_initialisation_neurons,network_connection


def position_one(results_path,param_nest,param_topology):
    """
    Create graphic with the position of neurons in one cell
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :param param_topology: Dictionary with the parameter for the topology
    :return:
    """
    # Number of unit in the axis x of the grid
    nb_cube_x = param_topology['nb_cube_x']
    # Number of unit in the axis y of the grid
    nb_cube_y = param_topology['nb_cube_y']
    # Larger of unit
    dim_x=param_topology['dim_x']
    # Longer of unit
    dim_y=param_topology['dim_y']

    #Create the network in nest
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer=network_initialisation_neurons(results_path,pyrngs,param_topology)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    del dic_layer['connection']
    color = [(0,0,1),(1,0,0)]
    # show position neurons
    fig = plt.figure(figsize=(20, 10))
    for index,(name_pops,layer_pops) in enumerate(dic_layer.items()):
            tp.PlotLayer(layer_pops[0]['layer'], fig=fig, nodecolor=color[index], nodesize=30)

    plt.axis([0.0, dim_x , 0.0, dim_y])
    plt.xlabel('x', {"fontsize": 60.0})
    plt.ylabel('y', {"fontsize": 60.0})
    plt.title('The localisation of neurons\n in one cell', {"fontsize": 60.0})
    plt.savefig(results_path + 'cell', format='eps', dpi=1000)
    # plt.show()

def position(results_path,param_nest,param_topology):
    """
    Create graphic with the position of all neurons
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :param param_topology: Dictionary with the parameter for the topology
    :return:
    """
    # Number of unit in the axis x of the grid
    nb_cube_x = param_topology['nb_cube_x']
    # Number of unit in the axis y of the grid
    nb_cube_y = param_topology['nb_cube_y']
    # Larger of unit
    dim_x=param_topology['dim_x']
    # Longer of unit
    dim_y=param_topology['dim_y']

    #Create the network in nest
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer=network_initialisation_neurons(results_path,pyrngs,param_topology)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    del dic_layer['connection']
    color = [(1,1,0),(1,0,1),(0,1,1), (0,0,1),(0,1,0),(1,0,0),(0.5,0.5,0.5)]
    # plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])))
    # np.random.shuffle(color)
    # show position neurons
    for name_pops,layer_pops in dic_layer.items():
        fig = plt.figure(figsize=(20, 10))
        for i in range(0, len(layer_pops)):
            tp.PlotLayer(layer_pops[i]['layer'], fig=fig, nodecolor=color[(i%201)%len(color)], nodesize=3)

        plt.axis([-10.0, dim_x * nb_cube_x + 10.0, -10.0, dim_y * nb_cube_y + 10.0])
        plt.xlabel('x', {"fontsize": 60.0})
        plt.ylabel('y', {"fontsize": 60.0})
        plt.title('The localisation of ' + name_pops + ' neurons\n in each layer', {"fontsize": 60.0})
        plt.savefig(results_path + 'layer' + name_pops, format='eps', dpi=1000)
        # plt.show()
    plt.close('all')
    fig = plt.figure(figsize=(20, 10))
    for name_pops,layer_pops in dic_layer.items():
        for i in range(0, len(layer_pops)):
            tp.PlotLayer(layer_pops[i]['layer'], fig=fig, nodecolor=color[(i%201)%len(color)], nodesize=3)

    plt.axis([-10.0, dim_x * nb_cube_x + 10.0, -10.0, dim_y * nb_cube_y + 10.0])
    plt.xlabel('x', {"fontsize": 60.0})
    plt.ylabel('y', {"fontsize": 60.0})
    plt.title('The localisation of neurons\n in each layer', {"fontsize": 60.0})
    plt.savefig(results_path + 'layer_all', format='eps', dpi=1000)
    # plt.show()
    plt.close('all')
    save_position = []
    for name_pops,layer_pops in dic_layer.items():
        for i in range(0, len(layer_pops)):
            save_position.append(np.array(nest.GetStatus(layer_pops[i]['layer'])[0]['topology']['positions']))
    np.save(results_path+"/position.npy",save_position)

def position_grid_one(results_path,param_nest,param_topology,id_grid):
    """
    Create graphic with the position of all neurons
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :param param_topology: Dictionary with the parameter for the topology
    :return:
    """
    # Number of unit in the axis x of the grid
    nb_cube_x = param_topology['nb_cube_x']
    # Number of unit in the axis y of the grid
    nb_cube_y = param_topology['nb_cube_y']
    # Larger of unit
    dim_x=param_topology['dim_x']
    # Longer of unit
    dim_y=param_topology['dim_y']

    #Create the network in nest
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer=network_initialisation_neurons(results_path,pyrngs,param_topology)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    del dic_layer['connection']
    color = [(1,1,0),(1,0,1),(0,1,1), (0,0,1),(0,1,0),(1,0,0),(0.5,0.5,0.5)]
    # plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])))
    # np.random.shuffle(color)
    # show position neurons
    for name_pops,layer_pops in dic_layer.items():
        fig = plt.figure(figsize=(20, 10))
        tp.PlotLayer(layer_pops[id_grid]['layer'], fig=fig, nodecolor=color[(id_grid%201)%len(color)], nodesize=3)

        plt.axis([-10.0, dim_x * nb_cube_x + 10.0, -10.0, dim_y * nb_cube_y + 10.0])
        plt.xlabel('x', {"fontsize": 60.0})
        plt.ylabel('y', {"fontsize": 60.0})
        plt.title('The localisation of ' + name_pops + ' neurons\n in each layer', {"fontsize": 60.0})
        plt.savefig(results_path + 'layer' + name_pops+str(id_grid), format='eps', dpi=1000)
        # plt.show()
    plt.close('all')
    fig = plt.figure(figsize=(20, 10))
    for name_pops,layer_pops in dic_layer.items():
            tp.PlotLayer(layer_pops[id_grid]['layer'], fig=fig, nodecolor=color[(id_grid%201)%len(color)], nodesize=3)

    plt.axis([-10.0, dim_x * nb_cube_x + 10.0, -10.0, dim_y * nb_cube_y + 10.0])
    plt.xlabel('x', {"fontsize": 60.0})
    plt.ylabel('y', {"fontsize": 60.0})
    plt.title('The localisation of neurons\n in each layer', {"fontsize": 60.0})
    plt.show()



def connection(results_path,param_nest,param_topology,param_connexion):
    """
    Create graphic with the target of one neurons by population
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :param param_topology: Dictionary with the parameter for the topology
    :param param_connexion: Parameter for the connexions
    :return:
    """
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer=network_initialisation_neurons(results_path,pyrngs,param_topology)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    tic = time.time()
    network_connection(dic_layer,param_topology,param_connexion)
    toc = time.time() - tic
    print("Time to create the connections and devices: %.2f s" % toc)
    color = ['b','r']
    # color = plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])+1))
    # np.random.shuffle(color)

    # show connexion layer
    layer_pops = dic_layer['excitatory']

    for i in range(0, len(layer_pops)):
        fig = plt.figure(figsize=(18, 8))
        ax = plt.axes()
        ax.set_prop_cycle('color', plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])+1)))
        for k in range(len(dic_layer)):
            color_point = color[k]
            for j in range(0, len(dic_layer.values()[k])):
                try:
                    tp.PlotTargets([layer_pops[i]['layer'][0] + 1],
                                   dic_layer.values()[k][j]['layer'],
                                   fig=fig,
                                   src_color='k', src_size=50,
                                   tgt_size=3, tgt_color=color_point
                                   )
                except:
                    pass
        plt.xlabel('x', {"fontsize": 10.0})
        plt.ylabel('y', {"fontsize": 10.0})
        fig.suptitle('Connexions from excitatory neurons '+str(i), fontsize=60.0)
        # plt.show()
        plt.savefig(results_path + 'interaction_excitatory_'+str(i), format='eps', dpi=1000)
        plt.close('all')

    layer_pops = dic_layer['inhibitory']
    for i in range(0, len(layer_pops)):
        fig = plt.figure(figsize=(18, 8))
        ax = plt.axes()
        ax.set_prop_cycle('color', plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])+1)))
        for k in range(len(dic_layer)):
            color_point = color[k]
            for j in range(0, len(dic_layer.values()[k])):
                try:
                    tp.PlotTargets([layer_pops[i]['layer'][0] + 1],
                                   dic_layer.values()[k][j]['layer'],
                                   fig=fig,
                                   src_color='k', src_size=50,
                                   tgt_size=3, tgt_color=color_point
                                   )
                except:
                    pass
        plt.xlabel('x', {"fontsize": 10.0})
        plt.ylabel('y', {"fontsize": 10.0})
        fig.suptitle('Connexions from inhibitory neurons '+str(i), fontsize=60.0)
        # plt.show()
        plt.savefig(results_path + 'interaction_inhibitory_'+str(i), format='eps', dpi=1000)
        plt.close('all')



def connection_one(results_path,param_nest,param_topology,param_connexion,i):
    """
    Create graphic with the target of one neurons of the population i
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :param param_topology: Dictionary with the parameter for the topology
    :param param_connexion: Parameter for the connexions
    :param i : id of the population
    :return:
    """
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer=network_initialisation_neurons(results_path,pyrngs,param_topology)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    tic = time.time()
    network_connection(dic_layer,param_topology,param_connexion)
    toc = time.time() - tic
    print("Time to create the connections and devices: %.2f s" % toc)
    # color = plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])+1))
    # np.random.shuffle(color)
    color =['b','r']

    # show connexion layer
    excitatory = True
    if i <= len(dic_layer['excitatory']):
        layer_pops = dic_layer['excitatory']
    else:
        layer_pops = dic_layer['inhibitory']
        i-=len(dic_layer['excitatory'])
        excitatory = False
    # fig = plt.figure(figsize=(18, 8))
    # ax = plt.axes()
    # ax.set_prop_cycle('color', plt.cm.Spectral(np.linspace(0, 1, len(dic_layer['excitatory'])+1)))
    # for k in range(len(dic_layer)):
    #         color_point = color[k]
    #         for j in range(0, len(dic_layer.values()[k])):
    #             try:
    #                 tp.PlotTargets([layer_pops[i]['layer'][0] + 1],
    #                                dic_layer.values()[k][j]['layer'],
    #                                fig=fig,
    #                                src_color='k', src_size=50,
    #                                tgt_size=3, tgt_color=color_point
    #                                )
    #             except:
    #                 pass
    #
    # fig.suptitle('Connexions from one neurons', fontsize=60.0)
    # # plt.show()
    # plt.savefig(results_path + 'interaction_'+str(i), format='eps', dpi=1000)
    # plt.close('all')

    from scipy.stats import multivariate_normal
    from mpl_toolkits.mplot3d import Axes3D
    mean = tp.GetPosition((layer_pops[i]['layer'][0] + 1,))[0]
    if excitatory:
        distribution = multivariate_normal(mean, [[param_connexion['conn_sigma_excitatory'], 0.0], [0.0, param_connexion['conn_sigma_excitatory']]])
    else:
        distribution = multivariate_normal(mean, [[param_connexion['conn_sigma_inhibitory'], 0.0], [0.0, param_connexion['conn_sigma_inhibitory']]])
    x = np.linspace(0,param_topology['nb_cube_x']*param_topology['dim_x'], 10000)
    y = np.linspace(0,param_topology['nb_cube_y']*param_topology['dim_y'], 10000)
    X,Y =np.meshgrid(x,y)
    pos = np.empty(X.shape + (2,))
    pos[:, :, 0] = X; pos[:, :, 1] = Y
    Z = distribution.pdf(pos)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X,Y,Z,cmap=plt.get_cmap('Greys'))
    ax.xaxis.set_ticks([])
    ax.yaxis.set_ticks([])
    for k in range(len(dic_layer)):
        position = []
        for j in range(0, len(dic_layer.values()[k])):
            try:
                tmp_pos = tp.GetTargetPositions ((layer_pops[i]['layer'][0] + 1,),
                                       dic_layer.values()[k][j]['layer'])
                if len(tmp_pos[0])!=0:
                    position.append(tmp_pos[0])
            except:
                pass
        position =np.concatenate(position)
        Xp = position[:,0]
        Yp = position[:,1]
        pos = np.empty(Xp.shape + (2,))
        pos[:, 0] = Xp; pos[:, 1] = Yp
        Zp = distribution.pdf(pos)
        ax.scatter(Xp, Yp, Zp, color=color[k], s=5)
    # ax.set_title('Connection probability',{"fontsize":30})
    plt.show()

def mean_connexion(results_path,param_nest,param_topology,param_connexion):
    """
    Return the mean of connexion for excitatory neurons
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :param param_topology: Dictionary with the parameter for the topology
    :param param_connexion: Parameter for the connexions
    :return: the mean connexion by type of neurons
    """
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer,gids=network_initialisation_neurons(results_path,pyrngs,param_topology,return_gids=True)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    tic = time.time()
    network_connection(dic_layer,param_topology,param_connexion)
    toc = time.time() - tic
    print("Time to create the connections and devices: %.2f s" % toc)

    id_source = param_topology['nb_cube_x']*param_topology['nb_cube_y']
    if parameter.param_topology['long_range']:
        nb_connexion = 0
        for i in tp.GetTargetNodes(range(gids[id_source][0],gids[id_source][1]), dic_layer['excitatory'][155]['layer']):
            nb_connexion += len(i)
        print("number of neurons from source : %r"%(gids[id_source][1]-gids[id_source][0]))
        print("number of connexion from long range connexion: %r" % (nb_connexion))
        nb_connexion = 0
        for i in tp.GetTargetNodes(range(gids[id_source][0],gids[id_source][1]), dic_layer['inhibitory'][155]['layer']):
            nb_connexion += len(i)
        print("number of neurons from source : %r"%(gids[id_source][1]-gids[id_source][0]))
        print("number of connexion from long range connexion: %r" % (nb_connexion))

    #network analysis
    result = {}
    id_gid=0
    for name_pop, layer_pop in dic_layer.items():
        nb_neuron = 0
        list_neuron = []
        result[name_pop] = {}
        for i in range(id_gid, id_gid+len(layer_pop)):
            list_neuron += range(gids[i][0],gids[i][1])
            nb_neuron += gids[i][1] - gids[i][0]
            id_gid+=1
        print("number of neurons " + name_pop +": %r" % (nb_neuron))
        for name_target,layer_pop_target in dic_layer.items():
            nb_connexion = 0
            for pop in layer_pop_target:
                for i in tp.GetTargetNodes(list_neuron,pop['layer']):
                        nb_connexion += len(i)
            print("mean number of connexion from " + name_pop + " to "+name_target+": %r" % (nb_connexion))
            result[name_pop][name_pop]=[nb_connexion, nb_neuron]

    for name_pop, layer_pop in dic_layer.items():
        result[name_pop] = {}
        for i in range(id_gid, id_gid+len(layer_pop)):
            list_neuron = np.arange(gids[i][0],gids[i][1])
            nb_neuron = gids[i][1] - gids[i][0]
            number_neurons = [160.0,40.0]
            for index,(name_target,layer_pop_target) in enumerate(dic_layer.items()):
                nb_connexion = 0.0
                for j in tp.GetTargetNodes(list(list_neuron),layer_pop_target[i]['layer']):
                    nb_connexion += float(len(j))/number_neurons[index]
                print("mean number of connexion from " + str(i) +": %r" % (float(nb_connexion)/len(list_neuron)))
                result[name_pop][name_pop]=[nb_connexion, nb_neuron]
            id_gid+=1

    return result

import parameter_grid_mechanism as parameter
parameter.param_topology['long_connections'] = np.array([([5,5,'e'],[15,5,'e']),
                                 ([5,5,'e'],[15,5,'i']),])
# parameter.param_topology['param_neuron']['t_ref']=0.4
# parameter.param_topology['dim_x']=1.0
# parameter.param_topology['dim_y']=1.0
# parameter.param_connexion['conn_sigma_excitatory_long']=0.8 #*2.5
# parameter.param_topology['nb_neuron_by_cube_ex']=160
# parameter.param_topology['nb_neuron_by_cube_in']=40
# parameter.param_connexion['g']=1.0
# parameter.param_connexion['weight_excitatory']=80.0
# parameter.param_connexion['weight_long']=80.0
parameter.param_connexion['tau_long']=1.0
parameter.param_connexion['conn_sigma_excitatory']=2.0
parameter.param_connexion['conn_sigma_inhibitory']=0.1
# parameter.param_topology['sigma_I_ext']=0.0
# parameter.param_topology['sigma_V_0']=100.0
# parameter.param_topology['mean_w_0']=10.0
# parameter.param_topology['mean_I_ext']=700.0
parameter.param_topology['long_range']=True
# parameter.param_background['noise']=True
# parameter.param_background['sigma_noise']=50.0
# parameter.param_nest['master_seed']=46



# parameter.param_topology['long_range']=False
# parameter.param_connexion['weight_long']=0.0
# parameter.param_connexion['tau_long']=0.0
# parameter.param_topology['sigma_I_ext']=0.0
# parameter.param_topology['nb_cube_x']=1
# parameter.param_topology['nb_cube_y']=1
# parameter.param_topology['sigma_V_0']=100.0
# parameter.param_topology['mean_w_0']=200.0
# parameter.param_topology['mean_I_ext']=700.0
# parameter.param_background['stimulus']=False
# parameter.param_topology['param_neuron']['t_ref']=1.5
# parameter.param_connexion['weight_excitatory']=1.3
# parameter.param_topology['dim_x']=11.5
# parameter.param_topology['dim_y']=11.5
# parameter.param_background['noise']=False
# parameter.param_background['sigma_noise']=50.0
# parameter.param_nest['master_seed']=46
# parameter.param_topology['nb_neuron_by_cube_ex'] = 1600
# parameter.param_topology['nb_neuron_by_cube_in'] = 400

# mean_connexion('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_mean_connection/',param_nest=parameter.param_nest,param_topology=parameter.param_topology,param_connexion=parameter.param_connexion)

# position('../../example/PG_position/',param_nest=parameter.param_nest,param_topology=parameter.param_topology)
position_grid_one('../../example/PG_position/',param_nest=parameter.param_nest,param_topology=parameter.param_topology,id_grid=200)
# position_one('../../example/PG_position/',param_nest=parameter.param_nest,param_topology=parameter.param_topology)
# for i in range(14):
#     connection_one('../../example/PG_connection/',param_nest=parameter.param_nest,param_topology=parameter.param_topology,param_connexion=parameter.param_connexion,i=i)
# connection_one('../../example/PG_connection/',param_nest=parameter.param_nest,param_topology=parameter.param_topology,param_connexion=parameter.param_connexion,i=200)
# connection('../../example/PG_connection/',param_nest=parameter.param_nest,param_topology=parameter.param_topology,param_connexion=parameter.param_connexion)
# mean_connexion('../../example/PG_mean_connection_one/',param_nest=parameter.param_nest,param_topology=parameter.param_topology,param_connexion=parameter.param_connexion)