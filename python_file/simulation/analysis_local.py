import numpy as np
import os
from elephant.statistics import isi, cv, lv
from elephant.spectral import welch_psd
from scipy.stats import variation
from analysis_global import compute_synchronize_R,get_gids,load_spike, detection_burst
from result_class import Result_analyse

def get_names(path):
    """
    Get the id of the different neurons in the network by population
    :param path: the path to the file
    :return: an array with the name of different populations
    """
    gidfile = open(path + '/population_GIDs.dat', 'r')
    names = []
    counter ={}
    for l in gidfile:
        a = l.split()
        try :
            counter[a[2]] +=1
        except:
            counter[a[2]] = 0
        names.append(a[2]+'_'+str(counter[a[2]]))
    return names

def load_mask(path,gids):
    """
    Get the mask for the no active neurons
    :param path: the path to the file
    :param gids: the array of the id
    :return: the mask
    """
    if not os.path.exists(path + "/population_inactive_GIDs.npy"):
        return None
    no_neurons_id = np.load(path + '/population_inactive_GIDs.npy', 'r')
    mask =[]
    for gid in gids:
        index = no_neurons_id[np.where((gid[0] <= no_neurons_id) & (no_neurons_id <= gid[1]))]
        mask_local = np.zeros((gid[1]-gid[0],))
        mask_local[index - gid[0]] = 1
        mask.append(mask_local)
    return mask

def transfom_spike(gid, spike, begin, end,resolution,limit_burst,mask):
    """
    Compute different measure on the spike
    :param gids: the array with the id
    :param data: the times spikes
    :param begin: the first time
    :param end: the end time
    :param resolution: the resolution of the simulation
    :return:
        synchronisation : hist_0_1, hist_0_3,
        irregularity : cv_list, lv_list,
        the percentage of spike analyse for the irregularity : percentage
    """
    nb_neuron = gid[1] - gid[0]
    nb_neuron -= np.sum(mask) if mask is not None else 0
    if nb_neuron == 0:
        return [None, None, [], [], [], [], [-1,-1], 0.0,
         [], [], [], [],
         [], [], [], [], 0.0, 0.0]
    cv_list = []
    lv_list = []
    isi_list = []
    spikes = []
    burst_list_nb = []
    burst_list_count = []
    burst_list_rate = []
    burst_list_interval = []
    burst_list_begin_cv =[]
    burst_list_begin_lv =[]
    burst_list_end_cv =[]
    burst_list_end_lv =[]
    id_spikes_threshold = []
    for i in np.arange(gid[0], gid[1], 1):
        spike_i = spike[np.where(spike[:, 0] == i)]
        spike_i = (spike_i[np.where(spike_i[:, 1] >= begin), 1]).flatten()
        spike_i = (spike_i[np.where(spike_i <= end)])
        spikes.append(spike_i)
        isis,burst_begin,burst_end,burst_count = detection_burst(spike_i,limit_burst)
        isi_list.append(isis)
        if burst_begin != []:
            burst_list_nb.append(len(burst_begin))
            burst_list_count.append(burst_count)
            burst_list_rate.append(float(len(burst_begin))* 1000. /float(end-begin))
            burst_list_interval.append(burst_end-burst_begin)
            isis_begin = isi(burst_begin)
            isis_end = isi(burst_end)
            if np.shape(burst_begin)[0] > 20:
                burst_list_begin_cv.append(cv(isis_begin))
                burst_list_begin_lv.append(lv(isis_begin))
                burst_list_end_cv.append(cv(isis_end))
                burst_list_end_lv.append(lv(isis_end))
        if np.shape(spike_i)[0] > 20: #Filter neurons with enough spikes
            cv_list.append(cv(isis))
            lv_list.append(lv(isis)) #spike in the same time give nan
            id_spikes_threshold.append([len(spikes)-1,len(spike_i)])
    if id_spikes_threshold:
        R_list,R_times,frequency_phase=compute_synchronize_R(spikes,id_spikes_threshold,resolution)
    else:
        R_list =[]
        R_times=[-1,-1]
        frequency_phase=[-1.0,-1.0]
    percentage = (np.count_nonzero(cv_list) / float(nb_neuron))
    percentage_burst = float(len(burst_list_rate)) / float(nb_neuron)
    percentage_burst_cv = float(len(burst_list_begin_cv)) / float(nb_neuron)
    spikes_concat = np.concatenate(spikes)
    #this suppose the end and begin is in ms
    if int(end - begin) > 0:
        hist_1 = np.histogram(spikes_concat, bins=int(end - begin))  # for bins at 1 milisecond
        welch_hist_1 = welch_psd(hist_1[0],fs=1./1.e-3)
        frequency_hist = [welch_hist_1[0][np.argmax(welch_hist_1[1])],welch_hist_1[1][np.argmax(welch_hist_1[1])]]
    else:
        hist_1 = None
    if int(end - begin) > 3:
        hist_3 = np.histogram(spikes_concat, bins=int((end - begin) / 3))  # for bins at 3 milisecond
    else:
        hist_3 = None
    return [hist_1, hist_3, isi_list, cv_list, lv_list,R_list,R_times, percentage,
             burst_list_nb,burst_list_count,burst_list_rate,burst_list_interval,
            burst_list_begin_cv,burst_list_begin_lv,burst_list_end_cv,burst_list_end_lv,
            percentage_burst,percentage_burst_cv,
            frequency_hist,frequency_phase]

def compute_rate(results_save,gids,data,begin,end,mask,details=False):
    """
    Compute the firing rate
    :param gids: an array with the range of id for the different population
    :param data: the spike of all neurons between end and begin
    :param begin: the time of the first spike
    :param end: the time of the last spike
    :return: the mean and the standard deviation of firing rate, the maximum and minimum of firing rate
    """
    # compute rate
    list_rates_rev = []
    for h in list(range(len(gids))):
        #get data
        n_fil = data[h][:, 0]
        n_fil = n_fil.astype(int)
        #count the number of the same id
        count_of_n = np.bincount(n_fil)
        count_of_n_fil = count_of_n[gids[h][0] :gids[h][1]]
        #compute the rate
        rate_each_n_incomplet = count_of_n_fil * 1000. / (end - begin)
        #fill the table with the neurons which are not firing
        rate_each_n = np.concatenate(
            (rate_each_n_incomplet, np.zeros(-np.shape(rate_each_n_incomplet)[0] - gids[h][0] + gids[h][1] )))
        results_save.save_rate(rate_each_n,mask[h] if mask is not None else None)
        list_rates_rev.append(rate_each_n)
    if details:
        return [list_rates_rev]


def compute_irregularity_synchronization(results_save,gids,data,begin,end,resolution,limit_burst,mask,details=False):
    """
        Compute the irregularity and the synchronisation of neurons
    :param gids: an array with the range of id for the different population
    :param data: the spike of all neurons between end and begin
    :param begin: the time of the first spike
    :param end: the time of the last spike
    :param resolution: the resolution of the simulation
    :return: Irregularity : Mean and standard deviation of cv and lv ISI
             Regularity : Cv of the histogram for a bin of 1 ms and 3 ms
             Percentage of neurons analyse for the Irregularity]
    """
    # Synchronization and irregularity
    list_hist_0_3_rev = []
    list_hist_0_1_rev = []

    list_isis_rev = []
    list_cvs_rev = []
    list_lvs_rev = []

    list_Rs_rev = []

    list_burst_nb_rev= []
    list_burst_count_rev = []
    list_burst_rate_rev = []
    list_burst_interval_rev = []
    list_burst_begin_cv_rev = []
    list_burst_begin_lv_rev = []
    list_burst_end_cv_rev = []
    list_burst_end_lv_rev =[]


    for h in list(range(len(gids))):
        hist_0_1, hist_0_3, isi_list, cv_list, lv_list, R_list,R_times,percentage,\
            burst_list_nb,burst_list_count,burst_list_rate,burst_list_interval,\
            burst_list_begin_cv,burst_list_begin_lv,burst_list_end_cv,burst_list_end_lv,\
            percentage_burst,percentage_burst_cv,\
            frequency_hist,frequency_phase\
            = transfom_spike(gids[h], data[h], begin, end,resolution,limit_burst,
                             mask[h] if mask is not None else None)

        # hist
        if hist_0_3 is None:
            hist_0_3_variation = None
            hist_0_3_max = None
        else:
            hist_0_3_variation = variation(hist_0_3[0])
            hist_0_3_max = variation(hist_0_3[0])
        list_hist_0_3_rev.append(hist_0_3)

        # hist
        if hist_0_1 is None:
            hist_0_1_variation = None
            hist_0_1_max = None
        else:
            hist_0_1_variation = variation(hist_0_1[0])
            hist_0_1_max = variation(hist_0_1[0])
        list_hist_0_1_rev.append(hist_0_1)
        results_save.save_simple_synchronization(hist_0_1_variation,hist_0_1_max,hist_0_3_variation,hist_0_3_max)

        #Inter-spike interval
        list_isis_rev.append(cv_list)
        results_save.save_ISI(isi_list)

        # cv and lv
        list_cvs_rev.append(cv_list)
        list_lvs_rev.append(lv_list)
        results_save.save_irregularity(cv_list,lv_list)

        # R
        list_Rs_rev.append(R_list)
        results_save.save_R_synchronization(R_list,R_times[0],R_times[1])

        # percentage
        results_save.save_percentage(percentage)

        # save frequency
        results_save.save_frequency_hist_1(frequency_hist)
        results_save.save_frequency_phase(frequency_phase)

        #Burst
        results_save.save_burst_nb(burst_list_nb)
        list_burst_nb_rev.append(burst_list_nb)
        results_save.save_burst_count(burst_list_count)
        list_burst_count_rev.append(burst_list_count)
        results_save.save_burst_rate(burst_list_rate)
        list_burst_rate_rev.append(burst_list_rate)
        results_save.save_burst_interval(burst_list_interval)
        list_burst_interval_rev.append(burst_list_interval)
        results_save.save_burst_begin_irregularity(burst_list_begin_cv,burst_list_begin_lv)
        list_burst_begin_cv_rev.append(burst_list_begin_cv)
        list_burst_begin_lv_rev.append(burst_list_begin_lv)
        results_save.save_burst_end_irregularity(burst_list_end_cv,burst_list_end_lv)
        list_burst_end_cv_rev.append(burst_list_end_cv)
        list_burst_end_lv_rev.append(burst_list_end_lv)
        results_save.save_burst_percentage(percentage_burst)
        results_save.save_burst_percentage_cv(percentage_burst_cv)

    if details:
        return [list_cvs_rev,list_lvs_rev,list_hist_0_1_rev,list_hist_0_3_rev,list_isis_rev,list_Rs_rev,
                list_burst_nb_rev,list_burst_count_rev,list_burst_rate_rev,list_burst_interval_rev,
                list_burst_begin_cv_rev,list_burst_begin_lv_rev,list_burst_end_cv_rev,list_burst_end_lv_rev]

def analysis_local(path,begin,end,resolution,limit_burst):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    #take the data
    gids = get_gids(path)
    data_all = load_spike(gids,path, begin, end)
    mask = load_mask(path,gids)
    if data_all == -1:
        return -1
    results_save = Result_analyse()
    results_save.set_name_population(get_names(path))

    compute_rate(results_save,gids,data_all,begin,end,mask)
    compute_irregularity_synchronization(results_save,gids,data_all,begin,end,resolution,limit_burst,mask)
    # results_save.print_result()
    return results_save.result()

# print(analysis_local('./PG_good',2000,5000,0.1))
# print('test_1')
# print(analysis_local('../../data/exploration_t_ref_i_ext_layer/_t_ref_1.9000000000000001_I_ext_650.0',2000.0,2038.5,0.1,10.0))
# print('test_2')
# print(analysis_local('../../data/exploration_t_ref_i_ext_layer/_t_ref_0.0_I_ext_975.0',2000,5000,0.1,10.0))
# print('test_3')
# print(analysis_local('../../data/exploration_g_i_ext_layer/_g_0.6_I_ext_975.0',2000,5000,0.1,10.0))
# print('test_4')
# print(analysis_local('../../data/exploration_g_i_ext_layer/_g_3.6_I_ext_650.0',2000,5000,0.1,10.0))