import sqlite3
import datetime
import os
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.simulation_ring import simulate
from analysis_global import analysis_global
from analysis_local import analysis_local
from analysis_type import analysis_by_type
from analysis_quadrant import analysis_by_quadrant
from result_class import Result_analyse
import sys
import numpy as np
import pathos.multiprocessing as mp
import dill


def generate_parameter(parameter_default,dict_varibale):
    param_nest = parameter_default.param_nest
    param_topology = parameter_default.param_topology
    param_connexion = parameter_default.param_connexion
    param_background = parameter_default.param_background
    for variable in dict_varibale.keys():
        if variable in param_nest.keys():
            param_nest[variable]=dict_varibale[variable]
        elif variable in param_topology.keys():
            param_topology[variable]=dict_varibale[variable]
        elif variable in param_topology['param_neuron'].keys():
            param_topology['param_neuron'][variable]=dict_varibale[variable]
        elif variable in param_connexion.keys():
            param_connexion[variable]=dict_varibale[variable]
        elif variable in param_background.keys():
            param_background[variable]=dict_varibale[variable]
    return param_nest,param_topology,param_connexion,param_background

def get_resolution(parameter_default,dict_varibale):
    if 'sim_resolution' in dict_varibale.keys():
        return dict_varibale['sim_resolution']
    else:
        return parameter_default.param_nest['sim_resolution']

def type_database(variable):
    if hasattr(variable,'dtype'):
        if   np.issubdtype(variable,int):
            return 'INTEGER'
        elif np.issubdtype(variable,long):
            return 'INTEGER'
        elif np.issubdtype(variable,float):
            return 'REAL'
        else:
            sys.stderr.write('ERROR bad type of save variable\n')
            exit(1)
    else:
        if  isinstance(variable,int):
            return 'INTEGER'
        elif isinstance(variable, long):
            return 'INTEGER'
        elif isinstance(variable, float):
            return 'REAL'
        elif isinstance( variable ,str):
            return 'TEXT'
        elif isinstance(variable, unicode):
            return 'TEXT'
        elif isinstance(variable, buffer):
            return	'BLOB'
        else:
            sys.stderr.write('ERROR bad type of save variable\n')
            exit(1)

def init_database(data_base,table_name,dict_variable):
    """
    Iitialise the connection to the database et create the table
    :param data_base: file where is the database
    :param table_name: the name of the table
    :return: the connexion to the database
    """
    variable = ''
    key_variable = ','
    for key in dict_variable.keys():
            variable += key + ' '+type_database(dict_variable[key])+' NOT NULL,'
            key_variable += key +','
    measures_name = Result_analyse().name_measure()
    measures_name.remove('names_population')
    measures = ''
    for name in measures_name:
        measures +=name+' REAL,'

    con = sqlite3.connect(data_base, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,timeout=10000)
    cur = con.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS '
                + table_name
                + '(date TIMESTAMP NOT NULL,'
                  'path_file TEXT NOT NULL,'
                + variable
                + 'names_population TEXT NOT NULL,'
                + measures
                + 'PRIMARY KEY'
                  '(path_file'+key_variable+'names_population))'
                )
    cur.close()
    con.close()

def insert_database(data_base,table_name,results_path,dict_variable,result):
    """
    Insert some result in the database
    :param con: the connexion to the database
    :param table_name: the table where insert the value
    :param results_path: the path where is the results
    :param name_population: the name of the populations
    :param result: the measure of the networks
    :return: nothing
    """
    con = sqlite3.connect(data_base, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,timeout=1000)
    cur = con.cursor()
    list_data = []
    for pop in range(len(result['names_population'])):
         data = {'date':datetime.datetime.now(),
                'path_file':results_path}
         data.update(dict_variable)
         for key in result.keys():
             data[key]=result[key][pop]
         list_data.append(tuple(data.values()))
    keys = ','.join(data.keys())
    question_marks = ','.join(list('?'*len(data)))
    cur.executemany('INSERT INTO ' + table_name + ' (' + keys + ') VALUES (' + question_marks + ')', list_data)
    con.commit()
    cur.close()
    con.close()

def run(results_path,parameter_default,dict_variable,begin,end,print_volt):
    """
    Run one simulation, analyse the simulation and save this result in the database
    :param results_path: the folder where to save spikes
    :param data_base: the file of the database
    :param table_name: the name of the table of the database
    :param dict_variable : dictionnary with the variable change
    :param begin: the beginning of record spike
    :param end: the end of the simulation
    :param print_volt: print the voltage of the membrane of population
    :return: nothing
    """
    print('time: '+str(datetime.datetime.now())+' BEGIN SIMULATION \n')
    #create the folder for result is not exist
    newpath = os.path.join(os.getcwd(),results_path)
    if not os.path.exists(newpath):
            os.makedirs(newpath)

    param_nest,param_topology,param_connexion,param_background = generate_parameter(parameter_default,dict_variable)

    #simulate
    simulate(results_path=results_path,begin=begin,end=end,
        param_nest=param_nest,param_topology=param_topology,
        param_connexion=param_connexion,param_background=param_background,
        multimeter_record=print_volt,print_volt=print_volt)

    print('time: '+str(datetime.datetime.now())+' END SIMULATION \n')

def analysis(results_path,parameter_default,data_base,table_name,dict_variable,begin,end,analyse_global=True,analyse_type=True,analyse_local=True,analyse_quadrant=False):
    print('time: '+str(datetime.datetime.now())+' BEGIN ANALYSIS \n')
    #Save analysis in database
    init_database(data_base,table_name,dict_variable)
    #Global analysis
    if analyse_global:
        result_global = analysis_global(path=results_path,begin=begin,end=end,resolution=get_resolution(parameter_default,dict_variable),limit_burst=10.0)
        try:
            insert_database(data_base,table_name,results_path,dict_variable,result_global)
        except sqlite3.IntegrityError:
            exit(0)
    #type analysis
    if analyse_type:
        result_type = analysis_by_type(path=results_path,begin=begin,end=end,resolution=get_resolution(parameter_default,dict_variable),limit_burst=10.0)
        if result_type != -1:
            try:
                insert_database(data_base,table_name,results_path,dict_variable,result_type)
            except sqlite3.IntegrityError:
                exit(0)
    #quadrant analysis
    if analyse_quadrant:
        result_type = analysis_by_quadrant(path=results_path,begin=begin,end=end,resolution=get_resolution(parameter_default,dict_variable),limit_burst=10.0)
        if result_type != -1:
            try:
                insert_database(data_base,table_name,results_path,dict_variable,result_type)
            except sqlite3.IntegrityError:
                exit(0)
    #local analysis
    if analyse_local:
        result_local = analysis_local(path=results_path,begin=begin,end=end,resolution=get_resolution(parameter_default,dict_variable),limit_burst=10.0)
        if result_local != -1:
            try:
                insert_database(data_base,table_name,results_path,dict_variable,result_local)
            except sqlite3.IntegrityError:
                exit(0)
    print('time: '+str(datetime.datetime.now())+' END ANALYSIS \n')

def run_exploration_2D(path,parameter_default,data_base,table_name,dict_variables,begin,end,print_volt=False,
                       analyse=True,simulation=True,analyse_global=True,analyse_type=True,analyse_local=True,analyse_quadrant=False):
    name_variable_1,name_variable_2 = dict_variables.keys()
    print(path)
    if simulation:
        for variable_1 in  dict_variables[name_variable_1]:
            for variable_2 in  dict_variables[name_variable_2]:
                try:
                    print('SIMULATION : '+name_variable_1+': '+str(variable_1)+' '+name_variable_2+': '+str(variable_2))
                    results_path=path+'_'+name_variable_1+'_'+str(variable_1)+'_'+name_variable_2+'_'+str(variable_2)
                    run(results_path,parameter_default,{name_variable_1:variable_1,name_variable_2:variable_2},begin,end,print_volt)
                except:
                    sys.stderr.write('time: '+str(datetime.datetime.now())+' error: ERROR in simulation \n')
    if analyse :
        list_path = []
        for variable_1 in  dict_variables[name_variable_1]:
            for variable_2 in  dict_variables[name_variable_2]:
                #analysis(path+'_'+name_variable_1+'_'+str(variable_1)+'_'+name_variable_2+'_'+str(variable_2),parameter_default,data_base,table_name,{name_variable_1:variable_1,name_variable_2:variable_2},begin,end)
                list_path.append((path+'_'+name_variable_1+'_'+str(variable_1)+'_'+name_variable_2+'_'+str(variable_2),name_variable_1,variable_1,name_variable_2,variable_2))
        #multithreading analyse
        p = mp.ProcessingPool(processes=parameter_default.param_nest['local_num_threads'])
        def analyse_function (arg):
            path,name_variable_1,variable_1,name_variable_2,variable_2 = arg
            try:
                print('ANALYSIS : '+name_variable_1+': '+str(variable_1)+' '+name_variable_2+': '+str(variable_2)+'\n')
                analysis(path,parameter_default,data_base,table_name,{name_variable_1:variable_1,name_variable_2:variable_2},begin,end,
                         analyse_global=analyse_global,analyse_type=analyse_type,analyse_local=analyse_local,
                         analyse_quadrant=analyse_quadrant)
            except:
                print('ANALYSIS : '+name_variable_1+': '+str(variable_1)+' '+name_variable_2+': '+str(variable_2))
                sys.stderr.write('time: '+str(datetime.datetime.now())+' error: ERROR in analyse \n')
	p.map(dill.copy(analyse_function),list_path)

