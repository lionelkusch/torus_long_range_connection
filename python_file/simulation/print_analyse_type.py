import numpy as np
import pylab as plt
import os
from matplotlib.patches import Polygon
from matplotlib.backends.backend_pdf import PdfPages
from analysis_type import get_gids,load_spike_ids,compute_rate,compute_irregularity_synchronization
from result_class import Result_analyse

def print_analyse(name,path,begin,end,resolution,limit_burst):
    newpath = os.path.join(os.getcwd(), name)
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    pp = PdfPages(name+'/analyse.pdf')

    gids_all = get_gids(path)
    data_pop_all = load_spike_ids(gids_all,path, begin, end)
    name_population = gids_all.keys()
    results_save = Result_analyse()
    results_save.save_name_population(gids_all.keys())

    list_rates_rev= compute_rate(results_save,gids_all, data_pop_all, begin, end,details=True)[0]
    list_cvs_rev,list_lvs_rev,list_hist_0_1_rev,list_hist_0_3_rev,list_isis_rev,list_Rs_rev,\
                list_burst_nb_rev,list_burst_count_rev,list_burst_rate_rev,list_burst_interval_rev,\
                list_burst_begin_cv_rev,list_burst_begin_lv_rev,list_burst_end_cv_rev,list_burst_end_lv_rev= compute_irregularity_synchronization(results_save,gids_all,data_pop_all,begin,end,resolution,limit_burst,details=True)

    pops =[]
    for name,gids in gids_all.items():
        nb_neuron = 0
        for gid in gids:
            nb_neuron = nb_neuron + gid[1] - gid[0]
        pops.append(nb_neuron)
    label_pos = list(range(len(pops), 0, -1))
    color_list = plt.cm.spectral(np.linspace(0,1,len(pops)))
    medianprops = dict(linestyle='-', linewidth=3.5, color='firebrick')

    # print box rate
    fig, ax1 = plt.subplots(figsize=(20, 20))
    bp = plt.boxplot(list(reversed(np.array(list_rates_rev))), 0, 'rs', 0, medianprops=medianprops,showmeans=True)
    plt.setp(bp['boxes'], color='black',linewidth=2.0)
    plt.setp(bp['whiskers'], color='black',linewidth=2.0)
    plt.setp(bp['fliers'], color='red', marker='|', markersize=10)
    for h in list(range(len(pops))):
        boxX = []
        boxY = []
        box = bp['boxes'][h]
        for j in list(range(5)):
            boxX.append(box.get_xdata()[j])
            boxY.append(box.get_ydata()[j])
        boxCoords = list(zip(boxX, boxY))
        k = h % 2
        boxPolygon = Polygon(boxCoords, facecolor=color_list[k], linewidth=2.0)
        ax1.add_patch(boxPolygon)
    plt.xlabel('firing rate [Hz]', fontsize=60)
    plt.yticks(label_pos, name_population, fontsize=30)
    plt.xticks(fontsize=50)
    plt.savefig(pp, format='pdf')

    #print histogram
    for i in list(np.arange(0,len(pops),2)):
        plt.figure(figsize=(20, 20))
        rate_ex=list_rates_rev[i]
        rate_in = list_rates_rev[i+1]
        plt.hist(rate_ex,bins=100,weights=np.zeros_like(rate_ex) + 1. / (gids[i][1]-gids[i][0]+1),alpha=0.5, label=name_population[i] )
        plt.hist(rate_in,bins=100,weights=np.zeros_like(rate_in) + 1. / (gids[i+1][1]-gids[i+1][0]+1),alpha=0.5, label=name_population[i+1])
        plt.title("Distribution firing rate",{"fontsize":60.0})
        plt.xlabel("Hz",{"fontsize":60.0})
        plt.ylabel("Relative frequency",{"fontsize":60.0})
        plt.legend()
        plt.tick_params(axis='both', labelsize=50)
        plt.savefig(pp, format='pdf')



    #print box cv
    fig, ax1 = plt.subplots(figsize=(20,20))
    bp = plt.boxplot(list(reversed(np.array(list_cvs_rev))), 0, 'rs', 0, medianprops=medianprops,showmeans=True)
    plt.setp(bp['boxes'], color='black',linewidth=2.0)
    plt.setp(bp['whiskers'], color='black',linewidth=2.0)
    plt.setp(bp['fliers'], color='red', marker='|', markersize=10)
    for h in list(range(len(pops))):
        boxX = []
        boxY = []
        box = bp['boxes'][h]
        for j in list(range(5)):
            boxX.append(box.get_xdata()[j])
            boxY.append(box.get_ydata()[j])
        boxCoords = list(zip(boxX, boxY))
        k = h % 2
        boxPolygon = Polygon(boxCoords, facecolor=color_list[k],linewidth=2.0)
        ax1.add_patch(boxPolygon)
    plt.xlabel('Cv ISI', fontsize=60)
    plt.yticks(label_pos, name_population, fontsize=20)
    plt.xticks(fontsize=50)
    plt.savefig(pp, format='pdf')

    #print cv
    for i in list(np.arange(0,len(pops),2)):
        plt.figure(figsize=(20, 20))
        cv_ex=list_cvs_rev[i]
        cv_in = list_cvs_rev[i+1]
        plt.hist(cv_ex,bins=100,weights=np.zeros_like(cv_ex) + 1. / (gids[i][1]-gids[i][0]+1),alpha=0.5, label=name_population[i] )
        plt.hist(cv_in,bins=100,weights=np.zeros_like(cv_in) + 1. / (gids[i+1][1]-gids[i+1][0]+1),alpha=0.5, label=name_population[i+1])
        plt.title("Distribution Cv ISI",{"fontsize":60.0})
        plt.xlabel("Cv ISI",{"fontsize":60.0})
        plt.ylabel("Relative Cv ISI",{"fontsize":60.0})
        plt.legend()
        plt.tick_params(axis='both', labelsize=50)
        plt.savefig(pp, format='pdf')

    #print box lv
    fig, ax1 = plt.subplots(figsize=(20,20))
    bp = plt.boxplot(list(reversed(np.array(list_lvs_rev))), 0, 'rs', 0, medianprops=medianprops,showmeans=True)
    plt.setp(bp['boxes'], color='black',linewidth=2.0)
    plt.setp(bp['whiskers'], color='black',linewidth=2.0)
    plt.setp(bp['fliers'], color='red', marker='|', markersize=10)
    for h in list(range(len(pops))):
        boxX = []
        boxY = []
        box = bp['boxes'][h]
        for j in list(range(5)):
            boxX.append(box.get_xdata()[j])
            boxY.append(box.get_ydata()[j])
        boxCoords = list(zip(boxX, boxY))
        k = h % 2
        boxPolygon = Polygon(boxCoords, facecolor=color_list[k],linewidth=2.0)
        ax1.add_patch(boxPolygon)
    plt.xlabel('lv ISI', fontsize=60)
    plt.yticks(label_pos, name_population, fontsize=50)
    plt.xticks(fontsize=50)
    plt.savefig(pp, format='pdf')


    #print lv
    for i in list(np.arange(0,len(pops),2)):
        plt.figure(figsize=(20, 20))
        lv_ex=list_lvs_rev[i]
        lv_in = list_lvs_rev[i+1]
        plt.hist(lv_ex,bins=100,weights=np.zeros_like(lv_ex) + 1. / (gids[i][1]-gids[i][0]+1),alpha=0.5, label=name_population[i] )
        plt.hist(lv_in,bins=100,weights=np.zeros_like(lv_in) + 1. / (gids[i+1][1]-gids[i+1][0]+1),alpha=0.5, label=name_population[i+1])
        plt.title("Distribution lv ISI",{"fontsize":60.0})
        plt.xlabel("ms",{"fontsize":60.0})
        plt.ylabel("Relative lv ISI",{"fontsize":60.0})
        plt.legend()
        plt.tick_params(axis='both', labelsize=50)
        plt.savefig(pp, format='pdf')

    #print histogram
    f, axs = plt.subplots(len(pops), sharex=True, sharey=False,figsize=(30,30))
    axs[0].set_title("Histogram of spike 0.1ms",{"fontsize":60.0})
    axs[-1].set_xlabel("ms",{"fontsize":40.0})
    axs[-1].set_xticks(list_hist_0_1_rev[-1][1])
    for i in list(np.arange(0,len(pops))):
        hist=list_hist_0_1_rev[i]
        bins = hist[1]
        hist = hist[0]
        width = np.diff(bins)
        center = (bins[:-1] + bins[1:]) / 2
        ax= axs[i]
        ax.bar(center, hist, align='center', width=width)
        ax.set_ylabel(name_population[i],{"fontsize":40.0})
        ax.tick_params(labelsize=50)
    plt.savefig(pp, format='pdf')

    f, axs = plt.subplots(len(pops), sharex=True, sharey=False,figsize=(30,30))
    axs[0].set_title("Histogram of spike 0.3ms",{"fontsize":60.0})
    axs[-1].set_xlabel("Lv ISI",{"fontsize":40.0})
    axs[-1].set_xticks(list_hist_0_3_rev[-1][1])
    for i in list(np.arange(0,len(pops))):
        hist=list_hist_0_3_rev[i]
        bins = hist[1]
        hist = hist[0]
        width = np.diff(bins)
        center = (bins[:-1] + bins[1:]) / 2
        ax= axs[i]
        ax.bar(center, hist, align='center', width=width)
        ax.set_ylabel(name_population[i],{"fontsize":40.0})
        ax.tick_params(labelsize=50)
    plt.savefig(pp, format='pdf')

    rates_averaged_all = results_save.result()['rates_average']
    cvs_averaged_all = results_save.result()['cvs_ISI_average']
    hist_0_1s_varation_all = results_save.result()['cvs_IFR_1ms']
    hist_0_3s_varation_all = results_save.result()['cvs_IFR_3ms']
    # horizontal bar
    fig, axs = plt.subplots(2, 2, figsize=(20, 20))

    ax= axs.ravel()[0]
    y_pos = np.arange(len(name_population))
    ax.barh(y_pos, list(reversed(np.array(rates_averaged_all))), align='center', alpha=0.5)
    ax.set_yticks(y_pos, list(reversed(np.array(name_population))))
    ax.set_xlabel('frequence')
    ax.set_title('Frequence network')

    ax= axs.ravel()[0]
    y_pos = np.arange(len(name_population))
    ax.barh(y_pos, list(reversed(np.array(rates_averaged_all))), align='center', alpha=0.5)
    ax.set_yticks(y_pos, list(reversed(np.array(name_population))))
    ax.set_xlabel('Hz')
    ax.set_title('firing rate network')

    ax= axs.ravel()[1]
    y_pos = np.arange(len(name_population))
    ax.barh(y_pos, list(reversed(np.array(cvs_averaged_all))), align='center', alpha=0.5)
    ax.set_yticks(y_pos, list(reversed(np.array(name_population))))
    ax.set_xlabel('Cv ISI')
    ax.set_title('Cv ISI')

    ax= axs.ravel()[2]
    y_pos = np.arange(len(name_population))
    ax.barh(y_pos, list(reversed(np.array(hist_0_1s_varation_all))), align='center', alpha=0.5)
    ax.set_yticks(y_pos, list(reversed(np.array(name_population))))
    ax.set_xlabel('Cv Hist 1ms')
    ax.set_title('Cv Hist 1ms')

    ax= axs.ravel()[3]
    y_pos = np.arange(len(name_population))
    ax.barh(y_pos, list(reversed(np.array(hist_0_3s_varation_all))), align='center', alpha=0.5)
    ax.set_yticks(y_pos, list(reversed(np.array(name_population))))
    ax.set_xlabel('Cv Hist 3ms')
    ax.set_title('Cv Hist 3ms')
    plt.savefig(pp, format='pdf')
    pp.close()
    plt.close('all')

print_analyse('./test_t_ref_0.0/fig_1','./test_t_ref_0.0',2000,5000,0.1,10)
