from analysis_type import load_spike_ids,load_mask, compute_rate,compute_irregularity_synchronization
from result_class import Result_analyse
import os

def get_gids(path):
    """
    Get the id of the different neurons in the network by population
    :param path: the path to the file
    :return: an array with the range of id and name for the different population
    """
    gidfile = open(path + '/population_GIDs.dat', 'r')
    gids = {'source':[],'quadrant_2':[],'target':[],'quadrant_4':[]}
    long_range_index=200+1
    for index,l in enumerate(gidfile):
        a = l.split()
        pop = None
        if index in range(30,80)\
                or index in range(long_range_index+30,long_range_index+80)\
                or index == long_range_index-1:
            pop='source'
        elif index in range(80,130)\
            or index in range(80+long_range_index,130+long_range_index):
            pop='quadrant_2'
        elif index in range(130,180)\
            or index in range(130+long_range_index,180+long_range_index):
            pop='target'
        elif index in range(180,200)\
            or index in range(0,30)\
            or index in range(180+long_range_index,200+long_range_index)\
            or index in range(0+long_range_index,30+long_range_index):
            pop='quadrant_4'
        else:
            raise (Exception('bad configuration simulation'))
        gids[pop].append([int(a[0]), int(a[1])])
    return gids


def analysis_by_quadrant(path,begin,end,resolution,limit_burst):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    #take the data
    gids_all = get_gids(path)
    data_pop_all = load_spike_ids(gids_all,path, begin, end)
    mask = load_mask(path,gids_all)
    if data_pop_all == -1:
        return -1
    results_save = Result_analyse()
    results_save.set_name_population(gids_all.keys())

    compute_rate(results_save,gids_all, data_pop_all, begin, end,mask)
    compute_irregularity_synchronization(results_save,gids_all, data_pop_all, begin, end,resolution,limit_burst,mask)
    # results_save.print_result()
    return results_save.result()

# print(analysis_by_quadrant('/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/_b_0.0_tau_long_40.0/',2000,5000,0.1,10))
# print('test_1')
