#! /bin/bash

cd $1
name_file_spike="spike_detector.gdf"
# not overwrite a previous file
if [ -f $name_file_spike ]
then
    exit 1
fi
#echo "save file :" $name_file_spike
for name in spike_detector*.gdf
do
#	echo "name :"  $name
	cat  $(ls .|grep $name) >> $name_file_spike
	rm $name
done
cd ..
