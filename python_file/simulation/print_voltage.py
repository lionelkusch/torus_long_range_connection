import matplotlib.pylab as plt
import numpy as np

def get_data(path,begin,end):
    """
    Get the id of the neurons which create the spike
    :param path: the path to the file
    :param begin: the first time
    :param end: the end of time
    :return: The spike of all neurons between end and begin
    """
    data = {}
    data['excitatory']={}
    data_concatenated = np.loadtxt(path + "/multimeter_ex.dat")
    data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
    idx_time = ((data_raw[:, 1] > begin) * (data_raw[:, 1] < end))
    data_tmp = data_raw[idx_time]
    data['excitatory']['time'] = data_tmp[:,1]
    data['excitatory']['V'] = data_tmp[:,2]
    data['excitatory']['W'] = data_tmp[:,3]
    data['inhibitory']={}
    data_concatenated = np.loadtxt(path + "/multimeter_in.dat")
    data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
    idx_time = ((data_raw[:, 1] > begin) * (data_raw[:, 1] < end))
    data_tmp = data_raw[idx_time]
    data['inhibitory']['time'] = data_tmp[:,1]
    data['inhibitory']['V'] = data_tmp[:,2]
    data['inhibitory']['W'] = data_tmp[:,3]
    return data

def print_spike(path, begin, end):

    data = get_data(path, begin, end)

    fig = plt.figure(figsize=(18, 8))
    plt.plot(data['excitatory']['time'],data['excitatory']['V'])
    fig.subplots_adjust(bottom=0.16, left=0.15,top=0.8)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('V in mV',{"fontsize":60.0})
    plt.title('Voltage of the membrane potential \nof excitatory neuron',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.show()
    # plt.savefig(path+'/ex_voltage')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(data['excitatory']['time'],data['excitatory']['W'])
    fig.subplots_adjust(bottom=0.16, left=0.15,top=0.8)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('W in mV',{"fontsize":60.0})
    plt.title('Adaptation variable \nof excitatory neuron',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.show()
    # plt.savefig(path+'/ex_adaptation')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(data['excitatory']['V'],data['excitatory']['W'])
    fig.subplots_adjust(bottom=0.16, left=0.15,top=0.8)
    plt.xlabel('V in mV',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('W in mV',{"fontsize":60.0})
    plt.title('Phase plan \nof excitatory neuron',{"fontsize":60.0})
    # plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.show()
    # plt.savefig(path+'/ex_phase_plane')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(data['inhibitory']['time'],data['inhibitory']['V'])
    fig.subplots_adjust(bottom=0.16, left=0.15,top=0.8)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('V in mV',{"fontsize":60.0})
    plt.title('Voltage of the membrane potential \nof inhibitory neuron',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.show()
    # plt.savefig(path+'/ex_voltage')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(data['inhibitory']['time'],data['inhibitory']['W'])
    fig.subplots_adjust(bottom=0.16, left=0.15,top=0.8)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('W in mV',{"fontsize":60.0})
    plt.title('Adaptation variable \nof inhibitory neuron',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.show()
    # plt.savefig(path+'/ex_adaptation')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(data['inhibitory']['V'],data['inhibitory']['W'])
    fig.subplots_adjust(bottom=0.16, left=0.15,top=0.8)
    plt.xlabel('V in mV',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('W in mV',{"fontsize":60.0})
    plt.title('phase plan \nof inhibitory neuron',{"fontsize":60.0})
    # plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.show()
    # plt.savefig(path+'/ex_phase_plane')
    plt.close()

print_spike('../../example/spike/',0.0,10.0)