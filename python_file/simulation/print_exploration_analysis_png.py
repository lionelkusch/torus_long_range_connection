import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from print_exploration_analysis import getData,grid,draw_countour,draw_point, draw_countour_limit,draw_zone_level


def set_lim(ax,ymax,ymin,xmax,xmin,number_size ):
    ax.set_ylim(ymax=ymax,ymin=ymin)
    ax.set_xlim(xmax=xmax,xmin=xmin)
    ax.tick_params(axis='both', labelsize=number_size)
    # ax.xaxis.set_major_locator(plt.MaxNLocator(5))
    # ax.yaxis.set_major_locator(plt.MaxNLocator(5))


def print_exploration_analysis(count,data_base,table_name,list_variable,name_result,title,title_result,min_result=None,max_result=None,percentage=False,resX=None,resY=None,path=None,nbins=10):
    ## pdf
    name_var1=list_variable[0]['name']
    name_var2=list_variable[1]['name']
    title_var1=list_variable[0]['title']
    title_var2=list_variable[1]['title']
    label_size=20.0
    number_size=20.0
    level_percentage = 0.80
    data_global=getData(data_base,table_name,list_variable,'global')
    resolution = resX != None and resY != None
    if resX == None:
        resX=len(np.unique(data_global[name_var1]))
    if resY == None:
        resY=len(np.unique(data_global[name_var2]))
    if 'disp_max' in list_variable[0].keys():
        xmax = list_variable[0]['disp_max']
    else:
        xmax = None
    if 'disp_min' in list_variable[0].keys():
        xmin = list_variable[0]['disp_min']
    else:
        xmin = None
    if 'disp_max'in list_variable[1].keys():
        ymax=list_variable[1]['disp_max']
    else :
        ymax=None
    if 'disp_min'in list_variable[1].keys():
        ymin=list_variable[1]['disp_min']
    else :
        ymin=None

    fig,ax = plt.subplots(figsize=(20,8))
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global[name_result],res=resolution,resX=resX,resY=resY)
    if min_result != None and max_result != None:
        draw_countour_limit(fig,ax,X,Y,Z,resolution,title,title_var1,title_var2,title_result,min_result,max_result,label_size,number_size,nbins=nbins)
    else:
        draw_countour(fig,ax,X,Y,Z,resolution,title,title_var1,title_var2,title_result,label_size,number_size)
    if percentage :
        X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
        draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)
    # fig.set_size_inches(5.14, 3.89, forward=True)
    # plt.subplots_adjust(top=0.93,bottom=0.07,left=0.1,right=1.0)
    # plt.savefig('./test'+name_result+'.png')
    if path:
        if count == 0 :
            plt.savefig(path + 'Figure_1.png')
        else:
            plt.savefig(path + 'Figure_1-'+str(count)+'.png')
        count+=1
        return count
    else:
        plt.show()

def print_exploration_analysis_ex_in(count,data_base,table_name,list_variable,name_result,title,title_result,min_result=None,max_result=None,percentage=False,resX=None,resY=None,path=None,nbins=10):
    ## pdf
    name_var1=list_variable[0]['name']
    name_var2=list_variable[1]['name']
    title_var1=list_variable[0]['title']
    title_var2=list_variable[1]['title']
    label_size=30.0
    number_size=20.0
    level_percentage = 0.95
    data_excitatory=getData(data_base,table_name,list_variable,'excitatory')
    data_inihibtory=getData(data_base,table_name,list_variable,'inhibitory')
    resolution = resX != None and resY != None
    if resX == None:
        resX=len(np.unique(data_excitatory[name_var1]))
    if resY == None:
        resY=len(np.unique(data_excitatory[name_var2]))
    if 'disp_max' in list_variable[0].keys():
        xmax = list_variable[0]['disp_max']
    else:
        xmax = None
    if 'disp_min' in list_variable[0].keys():
        xmin = list_variable[0]['disp_min']
    else:
        xmin = None
    if 'disp_max'in list_variable[1].keys():
        ymax=list_variable[1]['disp_max']
    else :
        ymax=None
    if 'disp_min'in list_variable[1].keys():
        ymin=list_variable[1]['disp_min']
    else :
        ymin=None

    fig,ax = plt.subplots(figsize=(20,8))
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory[name_result],res=resolution,resX=resX,resY=resY)
    if min_result != None and max_result != None:
        draw_countour_limit(fig,ax,X,Y,Z,resolution,title+' excitatory neurons',title_var1,title_var2,title_result,min_result,max_result,label_size,number_size,nbins=nbins)
    else:
        draw_countour(fig,ax,X,Y,Z,resolution,title+' excitatory neurons',title_var1,title_var2,title_result,label_size,number_size)
    if percentage :
        X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
        draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)
    if path:
        if count == 0 :
            plt.savefig(path + 'Figure_1.png')
        else:
            plt.savefig(path + 'Figure_1-'+str(count)+'.png')
        count+=1
    else:
        plt.show()

    fig,ax = plt.subplots(figsize=(20,8))
    X,Y,Z=grid(data_excitatory[name_var1],data_inihibtory[name_var2],data_inihibtory[name_result],res=resolution,resX=resX,resY=resY)
    if min_result != None and max_result != None:
        draw_countour_limit(fig,ax,X,Y,Z,resolution,title+' inhibitory neurons',title_var1,title_var2,title_result,min_result,max_result,label_size,number_size,nbins=nbins)
    else:
        draw_countour(fig,ax,X,Y,Z,resolution,title+' inhibitory neurons',title_var1,title_var2,title_result,label_size,number_size)
    if percentage :
        X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
        draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)
    if path:
        if count == 0 :
            plt.savefig(path + 'Figure_1.png')
        else:
            plt.savefig(path + 'Figure_1-'+str(count)+'.png')
        count+=1
        return count
    else:
        plt.show()

# database = './data_2/big_layer_weight_long_range_connection_t_ref_0_4/database.db'
# t_ref =0.4
# database = './data_2/big_layer_weight_long_range_connection_3/database.db'
# t_ref =1.5
# sigma=0.0
# variable = [
#            {'name':'weight_long','title':'weight of long range connexion','max':50.0,'min':-1.0},
#            {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#            ]
# database = './data_2/size_find_t_ref_0_4/database.db'
# sigma=1.0
# database = './data_2/size_find_t_ref_0_4_I_sigma_0/database.db'
# sigma=0.0
# t_ref =0.4
# database = './data_2/size_find/database.db'
# sigma=1.0
# database = './data_2/size_find_t_ref_1_5_sigma_I_0/database.db'
# sigma=0.0
# t_ref =1.5
# variable = [
#             {'name':'nb_cube_x','title':'nb cube on x','max':100,'min':0,},
#             {'name':'nb_cube_y','title':'nb cube on y','max':100,'min':0}
#             ]
#
# add = ' t_ref='+str(t_ref)+' and sigma='+str(sigma)
# table = 'exploration_1'


# t_ref=1.5
# database = './data_2/random_initial_condition_t_ref_1_5/database.db'
# variable =[  {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#              {'name':'sigma_V_0','title':'variance of voltage in the initial condition','max':100.0,'min':0.0}
#             ]
# database = './data_2/random_initial_condition_w_t_ref_1_5/database.db'
# variable =[  {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#               {'name':'mean_w_0','title':'variance and mean of w','max':100.0,'min':0.0}
#             ]
# database = './data_2/noise_integrator_1_5/database.db'
# variable =[
#              {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#              {'name':'sigma_noise','title':'variance of noise in the voltage','max':100.0,'min':0.0}
#             ]
# database = './data_2/global_coupling_t_ref_1_5/database.db'
# variable =[
#             {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#             {'name':'weight_excitatory','title':'weight on synaptic connections','max':4.0,'min':0.0}
#             ]
# database = './data_2/I_sigma_t_ref_1_5_long/database.db'
# variable = [
#                {'name':'sigma_I_ext','title':'sigma I ext in pA','max':200.0,'min':0.0},
#                {'name': 'tau_long', 'title': 'tau long in ms', 'max': 10000.0, 'min': 110.0}
#             ]
# database = './data_2/gc_delay_t_ref_1_5_long/database.db'
# database = './data_2/gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#                {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                {'name':'weight_excitatory','title':'weight on synaptic connections','max':8.0,'min':3.0}
#            ]
# database = './data_2/stimulus/other_2/database.db'
# variable =[
#                {'name':'stimulus_start','title':' time in ms','max':100000.0,'min':0.0,},
#                {'name':'stimulus_amplitude','title':'stimulus strength in pA','max':10000.0,'min':0.0}
#            ]


# t_ref=0.4
# database = './data_2/random_initial_condition_t_ref_0_4/database.db'
# variable =[  {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#              {'name':'sigma_V_0','title':'variance of voltage in the initial condition','max':1.0,'min':0.0}
#             ]
# database = './data_2/random_initial_condition_w_t_ref_0_4/database.db'
# variable =[  {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#               {'name':'mean_w_0','title':'variance and mean of w','max':100.0,'min':0.0}
#             ]
# database = './data_2/noise_integrator_only_0_4/database.db'
# variable =[
#              {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#              {'name':'sigma_noise','title':'variance of noise in the voltage','max':10.0,'min':0.0}
#             ]
# database = './data_2/global_coupling_t_ref_0_4/database.db'
# variable =[
#             {'name':'master_seed','title':'seed of random generator','max':100,'min':0},
#             {'name':'weight_excitatory','title':'weight on synaptic connections','max':100.0,'min':0.0}
#             ]
# database = './data_2/gc_delay_t_ref_0_4_long/database.db'
# variable =[
#                {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                {'name':'weight_excitatory','title':'weight on synaptic connections','max':200.0,'min':4.5}
#            ]

# add = ' t_ref='+str(t_ref)
# database = '/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#     {'name':'tau_long','title':' tau long in ms','max':600.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':5.0,'min':0.0},
#            ]
# database = '/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double/database.db'
# variable = [
#                {'name':'sigma_I_ext','title':'sigma I ext in pA','max':10.0,'min':0.0},
#                {'name': 'tau_long', 'title': 'tau long in ms', 'max': 10000.0, 'min': 110.0}
#             ]
# add = ''
# database = '/home/kusch/Documents/project/Jirsa/data/connection_E_low_t_ref_1_5_long_double/database.db'
# variable =[
#            {'name': 'distance', 'title': ' x target cell', 'max': 25.0, 'min': 0.0},
#            {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#        ]
# add = ' one way excitatory'
# database = '/home/kusch/Documents/project/Jirsa/data/connection_EE_low_t_ref_1_5_long_double/database.db'list_variable
# variable =[
#            {'name': 'distance', 'title': ' x target cell', 'max': 25.0, 'min': 0.0},
#            {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#        ]
# add = ' two ways excitatory'
# database = '/home/kusch/Documents/project/Jirsa/data/connection_EI_low_t_ref_1_5_long_double/database.db'
# variable =[
#            {'name': 'distance', 'title': ' x target cell', 'max': 25.0, 'min': 0.0},
#            {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#        ]
# add = ' one way excitatory and come back inhibitory'
# database = '/home/kusch/Documents/project/Jirsa/data/per_0_1_gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#     {'name':'tau_long','title':' tau long in ms','max':600.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':15.0,'min':0.0},
#            ]
# add = ' 90% '
# database = '/home/kusch/Documents/project/Jirsa/data/per_0_3_gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#     {'name':'tau_long','title':' tau long in ms','max':600.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':15.0,'min':2.5},
#            ]
# add = ' 70% '
# database = '/home/kusch/Documents/project/Jirsa/data/per_0_5_gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#     {'name':'tau_long','title':' tau long in ms','max':600.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':15.0,'min':2.5},
#            ]
# add = ' 50% '
# database = '/home/kusch/Documents/project/Jirsa/data/per_0_7_gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#     {'name':'tau_long','title':' tau long in ms','max':600.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':15.0,'min':2.5},
#            ]
# add = ' 30% '
# database = '/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#             {'name':'tau_long','title':' tau long in ms','max':320.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':10.0,'min':2.5},
#            ]
# add = ' 100% '
# database = '/home/kusch/Documents/project/Jirsa/data/state_t_ref_1_5_long/database.db'
# variable =[
#            {'name': 'dim_x', 'title': ' space of cell', 'max': 5.0, 'min': 0.0, },
#            {'name':'g','title':' relative strengh inhibitory excitatory','max':2.0,'min':0.0},
#
#         ]
# add = ' only torus'
# database = '/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#             {'name':'tau_long','title':' tau long in ms','max':400.0,'min':1.0,},
#             {'name':'weight_excitatory','title':'strength of local connexion','max':40.0,'min':0.0},
#            ]
# add = ' '
# database = '/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double/database.db'
# variable =[
#             {'name':'tau_long','title':' tau long in ms','max':400.0,'min':1.0,},
#             {'name':'weight_long','title':'weight long range connections','max':4.0,'min':0.0},
#            ]
# add = ' '
# database = '/home/kusch/Documents/project/Jirsa/data/as_t_ref_1_5_long_double_without_noise_long_connection/database.db'
# variable =[
#                {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                {'name': 'conn_sigma_excitatory_long', 'title': ' sigma long range connection', 'max': 5.0, 'min': 0., },
#            ]
# database = '/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/database.db'
# variable =[
#                {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                {'name': 'weight_excitatory','title':'strength of local connexion', 'max': 50.0, 'min': 22.0, },
#            ]
# database = '/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/database.db'
# variable =[
#                {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                {'name': 'weight_excitatory','title':'strength of local connexion', 'max': 50.0, 'min': 0.0, },
#            ]
# add = ' without noise 2000-120000'
# database = '/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/database.db'
# variable =[
#                {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                {'name': 'weight_excitatory','title':'strength of local connexion', 'max': 50.0, 'min': 0.0, },
#            ]
# add = ' without noise 120000-220000'
# database = '/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_3/database.db'
# variable =[
#                {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                {'name': 'weight_excitatory','title':'strength of local connexion', 'max': 50.0, 'min': 0.0, },
#            ]
# add = ' without noise 220000-320000'
# database = '/home/kusch/Documents/project/Jirsa/data/sub/sub_noise_gc/database.db'
# variable =[
#             {'name': 'sigma_noise', 'title': ' sigma noise in pA', 'max': 20000.0, 'min': 0.0},
#             {'name': 'weight_excitatory', 'title': ' local strength ', 'max': 100.0, 'min': 0.0},
#            ]

# database = '/home/kusch/Documents/project/Jirsa/data/sub/delay_gc/database.db'
# add = ' noise : mean=610.0 and variance=600.0'
# database = '/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_limit/database.db'
# add = ' noise : mean=610.0 and variance=120.0'
# database = '/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_low/database.db'
# add = ' noise : mean=400.0 and variance=2500.0'
# database = '/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low/database.db'
# add = ' noise : mean=400.0 ,variance=2500.0 and strength : 10.0'
# database = '/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low_3/database.db'
# add = ' noise : mean=400.0 ,variance=2500.0 and strength : 8.0'
# database = '/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low_2/database.db'
# add = ' noise : mean=400.0 ,variance=2500.0 and strength : 6.0'
# variable =[
#             {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#             {'name': 'g', 'title': ' relative strenght between \n excitatory and inhibitory neurons', 'max': 1.0, 'min': 0.0},
#            ]

# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/database.db'
# add = ' one direction\nnoise : mean=400.0 ,variance=1800.0, g=0.1'
# add=''
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test/10_high_2/database.db'
# add = ' two directions\nnoise : mean=400.0 ,variance=1800.0, g=0.1'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test_1/10_no/database.db'
# add = ' no noise : mean=400.0 ,variance=0.0, g=0.1'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test_1/10_low/database.db'
# add = ' low noise : mean=400.0 ,variance=1400.0, g=0.1'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test_1/10_high/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, g=0.1'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test_1/10_g_02/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, g=0.2'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test_1/10_g_015/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, g=0.15'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/test_1/10_g_004/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, g=0.04'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/database.db'
# add = ' reference : mean=400.0 ,variance=1600.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/database.db'
# add = ' reference : mean=400.0 ,variance=1400.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/database.db'
# add = ' reference : mean=400.0 ,variance=0.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_high/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_medium/database.db'
# add = ' reference : mean=400.0 ,variance=1600.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_low/database.db'
# add = ' reference : mean=400.0 ,variance=1400.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no/database.db'
# add = ' reference : mean=400.0 ,variance=0.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/database.db'
# add = ' reference : mean=400.0 ,variance=0.0 long'

# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_50/database.db'
# add = ' reference : mean=400.0 ,variance=1600.0, tau_w=50.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_100/database.db'
# add = ' reference : mean=400.0 ,variance=1600.0, tau_w=100.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_200/database.db'
# add = ' reference : mean=400.0 ,variance=1600.0, tau_w=200.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_300/database.db'
# add = ' reference : mean=400.0 ,variance=1600.0, tau_w=300.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, tau_w=50.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_100/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, tau_w=100.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_200/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, tau_w=200.0'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/database.db'
# add = ' reference : mean=400.0 ,variance=1800.0, tau_w=300.0'
# variable =[
#             {'name': 'tau_long', 'title': 'delay', 'max': 800.0, 'min': 0.0, 'disp_max':450.0, 'disp_min':0.0},
#             {'name': 'weight_long', 'title': 'weight', 'max': 50.0, 'min': 0.0, 'disp_max':40.0, 'disp_min':0.0},
#            ]
table = 'exploration_1'
# matplotlib.rcParams["savefig.directory"] = '/home/kusch/Documents/Vitkor_meeting/presentation_16/figure'
# path = '/home/kusch/Documents/Vitkor_meeting/presentation_16/figure/single_10_high_200/'
# path = '/home/kusch/Documents/Vitkor_meeting/presentation_16/figure/10_no_long/'


# table = 'experience_1'
# database = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_without_coupling/database_random_w.db'
# variable =[{'name':'mean_w_0','title':'random init w','max':300.0,'min':0.0},
#             {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}]

# database = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/database_random_w.db'
# variable =[{'name':'mean_w_0','title':'random init w','max':300.0,'min':0.0},
#             {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}]
# add = ' with coupling'

# database = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/database.db'
# variable =[{'name':'mean_I_ext','title':'I external','max':1000.0,'min':600.0},
#         {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}]
# add = ' with coupling'

# database = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/database.db'
# variable =[{'name':'weight_excitatory','title':'global coupling','max':0.2,'min':0.0},
#             {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}]
# add = ' with coupling'

# database = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness//database.db'
# variable =[ {'name':'percentage_inactive_neurons','title':'% inactive neurons','max':1.0,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}]
# add = ' thinckness'

variable =[  {'name':'tau_long','title':'delay along long range connection','max':1000.0,'min':0.0},
             {'name':'sigma_noise','title':'sigma noise','max':1900,'min':0},
             ]
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_noise_no_a_2.db'
# add = ' noise no a'
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/noise_no_a/'
database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_noise_with_a_2.db'
add = ' noise a'
path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/noise_a/'

# variable =[  {'name':'tau_long','title':'delay along long range connection','max':1000.0,'min':0.0},
#              {'name':'g','title':'relative strength ex/inh','max':19000,'min':0},
#              ]
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_g_adb_2.db'
# add = ' adaptation'
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/g_a/'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_g_no_a_2.db'
# add = ' no adaptation'
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/g_no_a/'
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_g_no_a_long_2.db'
# add = ' no adaptation long'
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/g_no_a_long/'
# variable =[  {'name':'tau_long','title':'delay along long range connection','max':200.0,'min':0.0},
#              {'name':'g','title':'relative strength ex/inh','max':0.128,'min':0.085},
#              ]
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_g_no_a_2.db'
# add = ' no adaptation zoom'
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/g_no_a_zoom/'

# variable =[  {'name':'tau_long','title':'delay along long range connection','max':10000.0,'min':0.0},
#              {'name':'weight_excitatory','title':'homogenous weight','max':10000.0,'min':0.0},
#              ]
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_local_weight_no_a_2.db'
# add = ' no adaptation'
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/weight_no_a/'

# variable =[  {'name':'tau_long','title':'delay along long range connection','max':10000.0,'min':0.0},
#              {'name':'b','title':'adaptation incrementation','max':10000.0,'min':0.0},
#              ]
# database = '/home/kusch/Documents/project/Jirsa/data/mechanism_adaptation/database_full_long.db'
# add = ' '
# path = '/home/kusch/Documents/poster/torus_paper/image/figure_generate/adaptation/'

count = 0
#Mean global
name_result = 'rates_average'
title = 'Mean firing rate'+add
title_result = 'Firing rate in Hz'
# print_exploration_analysis(count,database,table, variable,name_result,title,title_result)
count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
                                 min_result=0.0,max_result=35.0,
                                 path=path,nbins=17)


#R
name_result = 'synch_Rs_average'
title = 'Mean R synchronization'+add
title_result = 'R synchronization'
count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
                                 min_result=0.5,max_result=1.0,
                                 percentage=True,path=path,nbins=21)
# print_exploration_analysis(count,database,table, variable,name_result,title,title_result,percentage=True)

# CV 3 ms
name_result = 'cvs_IFR_3ms'
title = 'Cv of instantaneous firing rate\n with bins = 3ms and'+add
title_result = 'Cv IFR 3ms'
# print_exploration_analysis(count,database,table, variable,name_result,title,title_result,percentage=True)
count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
                                 min_result=0.0,max_result=2.0,
                                 percentage=True,path=path,nbins=21)

# LV
name_result = 'lvs_ISI_average'
title = 'Lv of interspiking intervalle'+add
title_result = 'Lv ISI'
count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
                                 min_result=0.0,max_result=2.0,
                                 percentage=True,path=path,nbins=13)

# CV
name_result = 'cvs_ISI_average'
title = 'CV of interspiking intervalle'+add
title_result = 'CV ISI'
count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
                                 min_result=0.0,max_result=2.0,
                                 percentage=True,path=path,nbins=11)

#Percentage of Burst
name_result = 'percentage_burst'
title = 'Percentage of neurons with more than 20 burst'+add
title_result = 'percentage of neuron'
count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
                                 min_result=0.0,max_result=1.0,
                                 path=path,nbins=11)
#
# # Mean global Ex and In
# name_result = 'rates_average'
# title = 'Mean firing rate'+add
# title_result = 'Firing rate in Hz'
# # print_exploration_analysis_ex_in(count,database,table, variable,name_result,title,title_result)
# count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
#                                  # min_result=0.0,max_result=16.0,
#                                  path=path,nbins=17)
#
# # R Ex and In
# name_result = 'synch_Rs_average'
# title = 'Mean R synchronization'+add
# title_result = 'R synchronization'
# count=print_exploration_analysis_ex_in(count,database,table, variable,name_result,title,title_result,
#                                        # min_result=0.5,max_result=1.0,
#                                        percentage=True,path=path,nbins=11)
# # print_exploration_analysis_ex_in(count,database,table, variable,name_result,title,title_result,percentage=True)
#
# # CV 3 ms  Ex and In
# name_result = 'cvs_IFR_3ms'
# title = 'Cv of instantaneous firing rate\n with bins = 3ms and'+add
# title_result = 'Cv IFR 3ms'
# count=print_exploration_analysis_ex_in(count,database,table, variable,name_result,title,title_result,
#                                        # min_result=0.0,max_result=5.0,
#                                        percentage=True,path=path,nbins=11)
#
# # LV  Ex and In
# name_result = 'lvs_ISI_average'
# title = 'Lv of interspiking intervalle'+add
# title_result = 'Lv ISI'
# count=print_exploration_analysis_ex_in(count,database,table, variable,name_result,title,title_result,
#                                        # min_result=0.0,max_result=3.0,
#                                        percentage=True,path=path,nbins=7)
#
# #Percentage
# name_result = 'percentage'
# title = 'Percentage of active neurons '+add
# title_result = 'Percentage'
# count=print_exploration_analysis(count,database,table, variable,name_result,title,title_result,
#                                  # min_result=0.0,max_result=1.0,
#                                  path=path,nbins=11)