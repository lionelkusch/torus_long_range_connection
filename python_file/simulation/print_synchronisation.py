import matplotlib.pyplot as plt
import numpy as np
from elephant.statistics import cv,lv,cv2,isi
from analysis_local import get_gids, load_spike
from matplotlib import gridspec
import os
import scipy.signal
from elephant.spectral import welch_psd

def compute_synchronize_R(spikes, id_max,resolution):
    """
    Borges et al. 2017 : tools for synchronize during complexe phase
    :param spikes: the time of each spike by neurons
    :param id_max: the id of neurons with enough spike and the number of spike by neurons
    :param resolution: the resolution of the simulation
    :return:
    """
    max_nb_spike = np.max(np.array(id_max)[:,1])
    spikes_time = np.array(spikes)[np.array(id_max)[:,0]]
    time_spike=np.zeros((len(spikes_time),max_nb_spike+1))
    for i,spikes in enumerate(spikes_time):
            time_spike[i,0:len(np.unique(spikes))]=np.unique(spikes)
    list_R= []
    list_ki = []
    time = []
    current_time_spike=np.array([time_spike[:,0],time_spike[:,1]-time_spike[:,0],time_spike[:,1]])
    id_current = np.zeros_like(time_spike[:,0]).astype(int)
    init = np.max(time_spike[:,0])
    not_time = current_time_spike[2,:]<init
    while np.sum(not_time) !=0:
        id_current[not_time]+=1
        current_time_spike[0,not_time] = current_time_spike[2,not_time]
        current_time_spike[1,not_time] = time_spike[not_time,id_current[not_time]+1]-time_spike[not_time,id_current[not_time]]
        current_time_spike[2,not_time] = time_spike[not_time,id_current[not_time]+1]
        not_time = current_time_spike[2,:]<init
    end = np.max(time_spike)
    t_end=init
    for t in np.arange(init,end,resolution):
            ki_value = 2*np.pi*(t-current_time_spike[0,:])/(current_time_spike[1,:])
            list_ki.append(ki_value)
            time.append(t)
            R=np.abs(np.mean(np.exp(ki_value*1j)))
            list_R.append(R)
            id_update = np.where(current_time_spike[2,:]<t+resolution)
            while np.sum(id_update) !=0:
                id_current[id_update]+=1
                if time_spike[id_update,id_current[id_update]+1].any() == 0:
                    t_end = t
                    break
                current_time_spike[0,id_update] = time_spike[id_update,id_current[id_update]]
                current_time_spike[1,id_update] = time_spike[id_update,id_current[id_update]+1]-time_spike[id_update,id_current[id_update]]
                current_time_spike[2,id_update] = time_spike[id_update,id_current[id_update]+1]
                id_update = np.where(current_time_spike[2,:]<t+resolution)
            if t_end != init:
                break
    return [list_R,[init,t_end],list_ki,time]

def transfom_spike(gid, spike,spike_begin, begin, end):
    '''
    transform the data in array of spike
    :param gid: the id of neurons
    :param spike: the list of spike
    :param spike_begin: the first id of neurons
    :param begin: the starting time
    :param end: the ending time
    :return:
    '''
    spikes = []
    id_spikes_threshold = []
    remove=[]
    for i in np.arange(gid[0], gid[1], 1):
        spike_i = spike[np.where(spike[:, 0] == i)]
        spike_i = (spike_i[np.where(spike_i[:, 1] >= begin), 1]).flatten()
        spike_i = (spike_i[np.where(spike_i <= end)])
        spikes.append(spike_i)
        if np.shape(spike_i)[0] > 20: #Filter neurons with enough spikes
            id_spikes_threshold.append([len(spikes)-1+spike_begin,len(spike_i)])
        else:
            remove.append(i)
            print("miss one neuron; spikes : %i"%np.shape(spike_i)[0])
    return spikes,id_spikes_threshold,remove

def R_list(spikes,id_spikes_threshold,resolution):
    '''
    return the compute R for the given spike
    :param spikes: klist of the spike time
    :param id_spikes_threshold: id of no neurons
    :param resolution: resolution of computing R
    :return: time of R, R, phase, total_time
    '''
    if id_spikes_threshold:
        R_list,R_times,list_ki,time=compute_synchronize_R(spikes,id_spikes_threshold,resolution)
    else:
        R_list =[]
        R_times=[-1,-1]
        list_ki = []
        time = []
    return R_times,R_list,list_ki,time

def ISI_regularity(spikes,id_spikes_threshold,time_simulation):
    '''
    Compute the reglarity of the neurons
    :param spikes: list of spike
    :param id_spikes_threshold: list of excluded neurons
    :param time_simulation: the total time of simulation
    :return: cv, lv, cv2, mean firing rate
    '''
    cv_list =[]
    lv_list=[]
    cv2_list = []
    mean_phase_velocity= []
    if id_spikes_threshold:
        for spike in spikes:
            ISI = isi(spike)
            cv_list.append(cv(ISI))
            lv_list.append(lv(ISI))
            cv2_list.append(cv2(ISI))
        for (index, number_spike) in id_spikes_threshold:
            mean_phase_velocity.append(number_spike/time_simulation)

    return cv_list,lv_list,cv2_list,mean_phase_velocity

def compute_all_element(gids,data,begin,end,resolution):
    '''
    compute all the analisys of the simulation
    :param gids: id of neurons
    :param data: list of all spike
    :param begin: starting time of the analisys
    :param end: ending time of the analysis
    :param resolution: the resolution for R
    :return: all the result
    '''
    list_Rs_rev = []
    list_ki_rev = []
    list_time_rev = []
    list_spikes = []
    list_id_spikes = []
    spike_begin = 0
    list_remove = []
    for h in list(range(len(gids))):
        print('h',h)
        spikes,id_spikes_threshold,remove= transfom_spike(gids[h], data[h], spike_begin, begin, end)
        list_spikes +=spikes
        list_id_spikes += id_spikes_threshold
        spike_begin+=len(spikes)
        list_remove.append(remove)
    list_cv,list_lv,list_cv2,mean_phase_velocity = ISI_regularity(list_spikes,list_id_spikes,end-begin)
    R_times,R_lists,list_ki,time= R_list(list_spikes,list_id_spikes,resolution)
    # R
    list_Rs_rev.append([R_times[0],R_times[1],R_lists])
    list_ki_rev.append(list_ki)
    list_time_rev.append(time)
    if len(list_remove) == 0:
        list_remove=[]
    else:
        list_remove=np.concatenate(list_remove)
    return list_Rs_rev,list_ki_rev,list_time_rev,list_cv,list_lv,list_cv2,mean_phase_velocity,list_remove

## from print spike
def hist_list(spikes,begin,end,resolution):
    '''
    compute the histogram of all the population
    :param spikes:
    :param begin:
    :param end:
    :param resolution:
    :return:
    '''
    spikes_concat = np.concatenate(([begin],np.concatenate(spikes),[end]))
    if int(end - begin) > 0:
        hist_0 = np.histogram(spikes_concat, bins=int((end - begin)/resolution))  # for bins at 1 milisecond
        hist_0[0][0]-=1
        hist_0[0][-1]-=1
    else:
        hist_0 = None
    return hist_0

def compute_hist_element(gids,data,begin,end,resolution):
    '''
    compute histof all the population
    :param gids: id of neurons
    :param data: list of all spike
    :param begin: starting time of the analisys
    :param end: ending time of the analysis
    :param resolution: the resolution for R
    :return: all the result
    '''
    spike_begin = 0
    list_spikes = []
    for h in list(range(len(gids))):
        print('h',h)
        spikes,id_spikes_threshold,remove= transfom_spike(gids[h], data[h], spike_begin, begin, end)
        list_spikes +=spikes
        spike_begin+=len(spikes)
    hist = hist_list(list_spikes,begin,end,resolution)
    return hist


def print_phase_histograme(list_ki_rev,time,begin,end,resolution_angle):
    '''
    print pahse histograme
    :param list_ki_rev:
    :param time:
    :return:
    '''
    for index_t,angle_time_t in enumerate(list_ki_rev):
        t= time[index_t]
        if t>=begin and t<= end:
            N = resolution_angle
            bottom = 0.0
            max_height = 100.0
            hist,bin_edges = np.histogram(angle_time_t,range=[0.0, np.around(2*np.pi,decimals=10)],bins=resolution_angle)
            hist=hist/float(angle_time_t.shape[0])*100.0
            print(np.sum(hist))
            print(np.max(np.diff(bin_edges))-np.min(np.diff(bin_edges)))
            ax = plt.subplot(111, polar=True)
            bars = ax.bar(bin_edges[:-1], hist, width= np.mean(np.diff(bin_edges)), bottom=bottom)
            ax.plot(np.linspace(0.0,2*np.pi,N),np.ones(N)*100.0/N,color='r');

            #color the bar of mean
            omega = np.angle(np.sum(np.exp(angle_time_t*1j)))
            if omega <0.0:
                omega +=2*np.pi

            # # Use custom colors and opacity
            # for r, bar in zip(hist, bars):
            #     bar.set_facecolor(plt.cm.jet(r / 10.))
            #     bar.set_alpha(0.8)
            bars[np.max(np.where((bin_edges<=omega))[0])].set_facecolor('r')
            plt.savefig(path+'/hist_circle/hist'+str(t)+'.png')
            plt.close('all')
            # plt.show()
            plt.hist(angle_time_t,range=[0.0, np.around(2*np.pi,decimals=10)],bins=resolution_angle)
            plt.hlines(angle_time_t.shape[0]/N,0.0,2*np.pi)
            plt.savefig(path+'/hist_flat/hist'+str(t)+'.png')
            plt.close('all')

def print_distribution_synhronization_mean_angle(angle_mean,PLV,time,begin,end):
    fig = plt.figure(figsize=(20,8))
    plt.plot(time[np.where((begin<time) & (time < end))],angle_mean,'bo',markersize=0.5)
    plt.xlabel('time',{"fontsize":40})
    plt.ylabel('rad',{"fontsize":40})
    plt.title('Mean angle',{"fontsize":40})
    plt.tick_params(axis='both', labelsize=30)
    fig = plt.figure(figsize=(20,8))
    plt.plot(range(PLV.shape[1]),np.mean(PLV,axis=0),'bo',markersize=0.5)
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('rad',{"fontsize":40})
    plt.title('Mean difference with mean angle',{"fontsize":40})
    plt.tick_params(axis='both', labelsize=30)
    fig = plt.figure(figsize=(20,8))
    plt.plot(range(PLV.shape[1]),np.std(PLV,axis=0),'bo',markersize=0.5)
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('rad',{"fontsize":40})
    plt.title('Standard deviation difference with mean angle',{"fontsize":40})
    plt.tick_params(axis='both', labelsize=30)
    fig = plt.figure(figsize=(20,8))
    plt.boxplot(PLV[:,24200:24500])
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('rad',{"fontsize":40})
    plt.title('Distribution of difference',{"fontsize":40})
    plt.show()

def print_frequency_and_snapshot(path,resolution,begin,end,list_ki_rev,time):
    if os.path.exists(path+'/hist_'+str(resolution)+'.npy'):
        hist = np.load(path+'/hist_'+str(resolution)+'.npy')
    else:
        gids = get_gids(path)
        data_all = np.load(path+'./data_all.npy')
        hist = compute_hist_element(gids,data_all,begin,end,resolution)[0]
        np.save(path+'/hist_'+str(resolution)+'.npy', hist)
    ## histogram frequency
    wel = welch_psd(hist,fs=1./(resolution*1.e-3))
    f_pic= wel[0][scipy.signal.find_peaks(wel[1])[0]]
    frequency_pic_order = np.flip(f_pic[np.argsort(wel[1][scipy.signal.find_peaks(wel[1])[0]])])
    plt.figure()
    plt.plot(wel[0],wel[1])
    plt.title('histogram frequency')
    frequence_max = frequency_pic_order[0]
    print(frequence_max)
    ## angle frequency
    angle_mean = np.load(path+'/angle_mean.npy')
    wel_phase = welch_psd(angle_mean,fs=1./(resolution*1.e-3))
    plt.figure()
    plt.plot(wel_phase[0],wel_phase[1])
    plt.title('angle frequency')
    print(wel_phase[0][np.argmax(wel_phase[1])])
    # frequence_max = wel_phase[0][np.argmax(wel_phase[1])]
    ## frequency R
    Rs_rev = np.load(path+'R.npy')[0]
    wel_R = welch_psd(Rs_rev,fs=1./(resolution*1.e-3))
    plt.figure()
    plt.plot(wel_R[0],wel_R[1])
    plt.title('R frequency')
    print(wel_R[0][np.argmax(wel_R[1])])
    # frequence_max = wel_R[0][np.argmax(wel_R[1])]
    shift = 50.0
    # mean = np.mean(list_ki_rev,axis=0)
    # mean = np.std(list_ki_rev,axis=0)
    # mean = range(len(list_ki_rev[0]))
    # mean = np.load(path+'/cv.npy')
    # mean = np.load(path+'/lv.npy')
    # mean = np.load(path+'/mean_phase_velocity.npy')
    mean = list_ki_rev[-100]
    # frequence_max = 4.7394
    for frequence_max in np.arange(frequence_max,frequence_max+0.0001,0.0001):
        plt.figure(figsize=(20,8))
        for index_t,angle_time_t in enumerate(list_ki_rev):
            t= time[index_t]
            if t>=begin and t<= end and shift<np.remainder(t,(1/frequence_max*1e3))<shift+resolution:
                plt.plot(range(angle_time_t.shape[0]),angle_time_t[np.argsort(mean)],'bo',markersize=0.05)
                plt.ylim(ymin = -0.5, ymax=6.8)
                print(t)
        plt.xlabel('ID neurons',{"fontsize":40})
        plt.ylabel('Phase of each snapshot',{"fontsize":40})
        plt.title('Typical snapshots : frequency '+str(frequence_max)+'Hz',{"fontsize":40})
        plt.tick_params(axis='both', labelsize=30)
    plt.show()
    plt.close('all')

def print_Kuramotor(time,Rs_rev,angle_mean,begin,end):
    fig = plt.figure(figsize=(20,15))
    gs = gridspec.GridSpec(3, 1)
    ax = fig.add_subplot(gs[2, 0])
    ax.plot(time[np.where((begin<time) & (time < end))],Rs_rev[np.where((begin<time) & (time < end))])
    ax.tick_params(axis='both', labelsize=30)
    ax.set_ylim(ymin=0.0,ymax=1.0)
    ax.set_xlabel('time in ms',{"fontsize":40})
    ax.set_ylabel('r:phase\ncoherence',{"fontsize":30})
    ax2 = fig.add_subplot(gs[0, 0])
    ax2.plot(time[np.where((begin<time) & (time < end))],angle_mean,'ro',markersize=1.0)
    ax2.tick_params(axis='both', labelsize=30)
    ax2.set_ylabel('average phase',{"fontsize":30})
    plt.title('Synchronization oscilator',{"fontsize":40})
    ax3 = fig.add_subplot(gs[1, 0])
    speed = np.diff(angle_mean)
    speed[np.where(speed<0.0)]+=2*np.pi
    ax3.plot(time[np.where((begin<time) & (time < end))][:-1],speed,'r',markersize=1.0)
    ax3.tick_params(axis='both', labelsize=30)
    ax3.set_ylabel('speed of \naverage phase',{"fontsize":30})
    plt.show()
    # plt.savefig(path+'Kuramoto.png')

def print_other_value_synch(path):
        ## CV of ISI by neurons
    cvs_rev = np.load(path+'/cv.npy')
    fig = plt.figure(figsize=(20,8))
    plt.plot(range(len(cvs_rev)),cvs_rev,'bo',markersize=0.5)
    plt.tick_params(axis='both', labelsize=30)
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('CV of ISI',{"fontsize":40})
    plt.title('Coefficiant of variation of Inter-Spiking Interval',{"fontsize":40})
    lvs_rev = np.load(path+'/lv.npy')
    fig = plt.figure(figsize=(20,8))
    plt.plot(range(len(lvs_rev)),lvs_rev,'bo',markersize=0.5)
    plt.tick_params(axis='both', labelsize=30)
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('LV of ISI',{"fontsize":40})
    plt.title('Local of variation of Inter-Spiking Interval',{"fontsize":40})
    cv2s_rev = np.load(path+'/cv2.npy')
    fig = plt.figure(figsize=(20,8))
    plt.plot(range(len(cv2s_rev)),cv2s_rev,'bo',markersize=0.5)
    plt.tick_params(axis='both', labelsize=30)
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('CV2 of ISI',{"fontsize":40})
    plt.title('Cv2 of Inter-Spiking Interval',{"fontsize":40})
    mean_phase_velocity = np.load(path+'/mean_phase_velocity.npy')
    fig = plt.figure(figsize=(20,8))
    plt.plot(range(len(mean_phase_velocity)),mean_phase_velocity*1e3,'bo',markersize=0.5)
    plt.xlabel('ID neurons',{"fontsize":40})
    plt.ylabel('Mean phase in Hz',{"fontsize":40})
    plt.title('mean phase velocity of neurons',{"fontsize":40})
    plt.tick_params(axis='both', labelsize=30)
    plt.show()

def analysis_R(path,begin,end,resolution,resolution_angle,name,delay,long_range=True):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    #take the data
    if not os.path.exists(path+'/ki.npy'):
        gids = get_gids(path)
        # gids=[[2, 162], [204, 364],[163,203],[2183,2223]]
        begin = begin-delay-10.0
        data_all = load_spike(gids,path, begin, end)
        if data_all == -1:
            return -1
        np.save(path+'/data_all.npy',np.array(data_all))
        print('save data')
        data_all = np.load(path+'./data_all.npy')
        list_Rs_rev,list_ki_rev,list_time_rev,list_cv,list_lv,list_cv2,mean_phase_velocity,remove = compute_all_element(gids,data_all,begin,end,resolution)

        #save the result of the analisys
        np.save(path+'/ki.npy', list_ki_rev)
        np.save(path+'/time.npy', list_time_rev)
        np.save(path+'/cv.npy',list_cv)
        np.save(path+'/lv.npy',list_lv)
        np.save(path+'/cv2.npy',list_cv2)
        np.save(path+'/mean_phase_velocity.npy',mean_phase_velocity)
        np.save(path+'/remove.npy',remove)
        #save R
        begin_R=[]
        end_R=[]
        R=[]
        for element in list_Rs_rev:
            begin_R.append(element[0])
            end_R.append(element[1])
            R.append(element[2])
        list_R = np.array(R)
        np.save(path+'R.npy', list_R)
        print('save R')
        list_begin_R = np.array(begin_R)
        np.save(path+'R_begin.npy', list_begin_R)
        print('save R begin')
        list_end_R = np.array(end_R)
        np.save(path+'R_end.npy', list_end_R)
        print('save R end')

    if not os.path.exists(path+'/angle_mean.npy'):
        list_ki_rev = np.load(path+'/ki.npy')[0]
        time = np.load(path+'/time.npy')[0]
        ## PLV : phase locking value
        angle_mean = []
        PLV = []
        PLV_r = []
        angle_mean_r = []
        for index_t,angle_time_t in enumerate(list_ki_rev):
            t= time[index_t]
            if t>=begin and t<= end:
                omega = np.angle(np.sum(np.exp(angle_time_t*1j)))
                if omega <0.0:
                    omega +=2*np.pi
                angle_mean.append(omega)
                diff = np.abs(angle_time_t-omega)
                diff[np.where(diff > np.pi)]=-(diff[np.where(diff > np.pi)]-2*np.pi)
                PLV.append(diff)
                print(t)
        np.save(path+'/angle_mean.npy',angle_mean)
        np.save(path+'/PLV.npy',PLV)

    #####Print differet element ########
    ### Print hist
    # list_ki_rev = np.load(path+'/ki.npy')[0]
    # time = np.load(path+'/time.npy')[0]
    # print_phase_histograme(list_ki_rev,time,begin,end,resolution_angle)

    ###Print Kuramoto
    # time = np.load(path+'/time.npy')[0]
    # angle_mean = np.load(path+'/angle_mean.npy')
    # PLV = np.load(path+'/PLV.npy')
    # print_distribution_synhronization_mean_angle(angle_mean,PLV,time,begin,end)

    #print frequency and snapshot
    # list_ki_rev = np.load(path+'/ki.npy')[0]
    # time = np.load(path+'/time.npy')[0]
    # print_frequency_and_snapshot(path,resolution,begin,end,list_ki_rev,time)

    #print Kuramoto
    # Rs_rev = np.load(path+'/R.npy')[0]
    # time = np.load(path+'/time.npy')[0]
    # angle_mean = np.load(path+'/angle_mean.npy')
    # print_Kuramotor(time,Rs_rev,angle_mean,begin,end)

    #print other
    # print_other_value_synch(path)


# path = '/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low/_g_0.1_tau_long_160.0/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_2500_mean_400_g_0_05/_weight_excitatory_6.0_weight_long_0.0/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_5000_mean_0_g_0_05/_weight_excitatory_6.0_weight_long_0.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_2.0_tau_long_240.0/'
# analysis_R(path,2000.0,12000.0,1.0,100,' ',0,False)
# analysis_R('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_2500_mean_400_g_0_05/_weight_excitatory_6.0_weight_long_0.0/',
#           1921.0,100000.0,1.0,1000,' ',0,False)
# analysis_R('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_2500_mean_400_g_0_05/_weight_excitatory_8.0_weight_long_0.0/',
#            0.0,100000.0,1.0,1000,' ',0,False)
# analysis_R('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_5000_mean_0_g_0_05/_weight_excitatory_6.0_weight_long_0.0/',
#            0.0,100000.0,1.0,1000,' ',0,False)
# analysis_R('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_5000_mean_0_g_0_05/_weight_excitatory_8.0_weight_long_0.0/',
#            0.0,100000.0,1.0,1000,' ',0,False)
pathes=[
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_5.0_tau_long_350.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_2.0_tau_long_90.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_2.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_350.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_5.0_tau_long_60.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_5.0_tau_long_130.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_2.0_tau_long_90.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_2.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_5.0_tau_long_350.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_2.0_tau_long_90.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_2.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_2.0_tau_long_90.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_2.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/8/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/8/_weight_long_2.0_tau_long_90.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/8/_weight_long_2.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_5.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_5.0_tau_long_60.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_130.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_10.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_5.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_10.0_tau_long_140.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_90.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_120.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_330.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_400.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_10.0_tau_long_260.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_120.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_400.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_0.0_tau_long_0.1/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high/_weight_long_0.0_tau_long_0.1/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_0.0_tau_long_0.1/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_5.0_tau_long_60.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_5.0_tau_long_210.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_5.0_tau_long_380.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_5.0_tau_long_370.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_25.0_tau_long_380.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_40.0_tau_long_210.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_15.0_tau_long_200.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_15.0_tau_long_290.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_10.0_tau_long_200.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_10.0_tau_long_260.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_35.0_tau_long_70.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_35.0_tau_long_50.0/',
      # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_2.0_tau_long_70.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_10.0_tau_long_380.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_40.0_tau_long_300.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_25.0_tau_long_280.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_35.0_tau_long_340.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_35.0_tau_long_430.0/',
    '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no/_weight_long_25.0_tau_long_330.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_25.0_tau_long_330.0/',
      ]
for path in pathes:
    # analysis_R(path,12000.0,32000.0,1.0,100,' ',0,False)
    analysis_R(path,2000.0,12000.0,1.0,100,' ',0,False)
