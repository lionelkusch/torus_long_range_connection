import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from analysis_local import get_gids, load_spike,compute_synchronize_R
import numpy as np
from matplotlib import gridspec
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle


def analysis_local(path,begin,end,resolution,limit_burst,name):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    # # #take the data
    gids = get_gids(path)
    hist01 = np.load(path+'/hist01.npy')

    # list_hist_0_001_rev = compute_hist_01_element(gids,data_all,begin,end,resolution)
    # hist01 = np.array([a[0] for a in list_hist_0_001_rev])
    # np.save(path+'hist01.npy', hist01)
    # print('save hist01')

    hist01[55,:]=hist01[55,:]+hist01[200,:]

    my_dpi=100
    fig_1 = plt.figure(figsize=(4000/my_dpi, 1600/my_dpi), dpi=my_dpi)
    gs = gridspec.GridSpec(2, 3, width_ratios=[1,1, 20])
    ax1 = fig_1.add_subplot(gs[0,0])
    ax2 = fig_1.add_subplot(gs[1,0])
    ax3 = fig_1.add_subplot(gs[0,1])
    ax4 = fig_1.add_subplot(gs[1,1])
    ax5 = fig_1.add_subplot(gs[:,2])
    time_interesting = np.unique(np.where(hist01 > np.ones(hist01[1,:].shape))[1])
    indice = np.where(time_interesting[0:-1]-time_interesting[1:]<-100)[0]

    begin = time_interesting [indice[0]+53]
    # begin = 2198
    #parameter fig
    src_x = 5
    src_y = 4
    target_x = 15
    target_y = 4
    x_src = [src_x, src_x, src_x+1, src_x+1, src_x] # abscisses des sommets
    y_src = [src_y, src_y+1, src_y+1, src_y, src_y] # ordonnees des sommets
    x_target = [target_x, target_x, target_x+1, target_x+1, target_x] # abscisses des sommets
    y_target = [target_y, target_y+1, target_y+1, target_y, target_y] # ordonnees des sommets
    text_fontsize = 20.0
    title_fontsize = 20.0
    delay = 1500
    # source
    if begin-delay >= 0:
        src_data_previous = np.ones((1, 1)) * ([hist01[256, begin-delay]])
    else :
        src_data_previous= np.ones((1, 1)) * np.NAN
    src_data = np.ones((1,1))*([hist01[256,begin]])
    ax1.text(1.0,-1.0,'source',fontdict={'fontsize':text_fontsize})
    ax1.matshow(src_data_previous, vmin = 0.0, vmax =40.0,extent=[0.0,1.0,1.0,0.0])
    ax1.set_yticklabels(range(src_y,src_y+2),{'fontsize':text_fontsize})
    ax1.set_xticklabels(range(src_x,src_x+2),{'fontsize':text_fontsize})
    ax1.text(0.0,2.0,'t='+str(begin-delay),fontdict={'fontsize':text_fontsize - 5.0})
    ax3.matshow(src_data, vmin=0.0, vmax=40.0, extent=[0.0, 1.0, 1.0, 0.0])
    ax3.set_yticklabels(range(src_y, src_y+2), {'fontsize': text_fontsize})
    ax3.set_xticklabels(range(src_x, src_x+2), {'fontsize': text_fontsize})
    ax3.text(0.0,2.0, 't=' + str(begin), fontdict={'fontsize': text_fontsize - 5.0})

    #target
    if begin-delay >= 0:
        target_data_previous = np.ones((1, 1)) * ([hist01[356, begin-delay]])
    else :
        target_data_previous= np.ones((1, 1)) * np.NAN
    target_data =np.ones((1,1))*([hist01[356,begin]])
    ax2.text(1.0,-1.0,'target', fontdict={'fontsize': text_fontsize})
    ax2.matshow(target_data_previous, vmin = 0.0, vmax =40.0,extent=[0.0,1.0,1.0,0.0],origin='lower')
    ax2.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
    ax2.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
    ax2.text(0.0,2.0,'t='+str(begin-delay),fontdict={'fontsize':text_fontsize - 5.0})
    ax4.matshow(target_data, vmin = 0.0, vmax =40.0,extent=[0.0,1.0,1.0,0.0],origin='lower')
    ax4.set_yticklabels(range(target_y,target_y+2),{'fontsize':text_fontsize})
    ax4.set_xticklabels(range(target_x,target_x+2),{'fontsize':text_fontsize})
    ax4.text(0.0,2.0,'t='+str(begin),fontdict={'fontsize':text_fontsize - 5.0})

    #plot
    data = np.transpose(np.reshape(hist01[201:,begin],(20,10)))
    im = ax5.matshow( data ,vmin = 0.0, vmax =40.0,extent=[0.0,20.0,10.0,0.0],origin='lower')
    col = fig_1.colorbar(im, cax = fig_1.add_axes([0.91, 0.2, 0.05, 0.6]))
    col.set_ticklabels(range(0,41),{'fontsize':text_fontsize})
    ax5.set_yticks(range(0,10))
    ax5.set_yticklabels(range(0,10),{'fontsize':text_fontsize})
    ax5.set_xticks(range(0,21))
    ax5.set_xticklabels(range(0,21),{'fontsize':text_fontsize})
    ax5.grid(color='r', linestyle='--', linewidth=0.3)

    ax5.plot(x_src,y_src,'b', linewidth=2.0)
    ax5.plot(x_target,y_target,'b', linewidth=2.0)
    ax5.text(0.0,-1.0,name+ ' t='+str(begin)+' ms',fontdict={'fontsize':title_fontsize})
    plt.show()



analysis_local('/home/lionelkusch/Documents/project_python/remake_paper/Jirsa_V_Stefanescu_A_2010/data/data_2/gc_delay_t_ref_1_5_long/_weight_excitatory_4.0_tau_long_150.0/',
               2000,12000,0.1,10.0,'4-150.0')