import sqlite3
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from numpy import linspace, meshgrid
from matplotlib.ticker import MaxNLocator
from matplotlib.mlab import griddata
import numpy as np


def getData(data_base,table_name,list_variable, name_population='global'):
    con = sqlite3.connect(data_base, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
    cursor = con.cursor()
    cursor.execute(
        ' SELECT * FROM ( '
        ' SELECT *'
        ' FROM ' + table_name + ' '
                                " WHERE names_population = '"+name_population+"'"
                                " AND " + list_variable[0]['name'] + ' >= ' + str(list_variable[0]['min']) +
        " AND " + list_variable[0]['name'] + ' <= ' + str(list_variable[0]['max']) +
        " AND " + list_variable[1]['name'] + ' >= ' + str(list_variable[1]['min']) +
        " AND " + list_variable[1]['name'] + ' <= ' + str(list_variable[1]['max']) +
        " ORDER BY " + list_variable[0]['name'] + ')'
                                                  " ORDER BY " + list_variable[1]['name']
    )
    data_all = cursor.fetchall()
    name_column = [description[0] for description in cursor.description]
    datas = {}
    for id, name in enumerate(name_column):
        datas[name] = []
        for data in data_all:
            datas[name].append(data[id])
    return datas

def grid(x, y, z,res,resX,resY):
    "Convert 3 column data to matplotlib grid"
    if res:
            xi = linspace(min(x), max(x), resX)
            yi = linspace(min(y), max(y), resY)
            Z = griddata(x, y, z, xi, yi,interp='linear')
            X, Y = meshgrid(xi, yi)
            return X, Y, Z
    id = np.where(np.array(z)!=None)
    result_x = np.array(x,dtype='float64')[id]
    result_y = np.array(y,dtype='float64')[id]
    result_z = np.array(z,dtype='float64')[id]
    return [result_x,result_y,result_z]

def draw_countour(fig,ax,X,Y,Z,resolution,title,xlabel,ylabel,zlabel,label_size,number_size):
    if len (Z) > 4 :
        if resolution:
            CS = ax.contourf(X,Y,Z,extend='both')
        else:
            CS = ax.tricontourf(X,Y,Z,extend='both')
        cbar = fig.colorbar(CS,ax=ax)
        cbar.ax.set_ylabel(zlabel,{"fontsize":label_size})
        cbar.ax.tick_params(labelsize=number_size)
        # ax.clabel(CS, inline=1, fontsize=10)
        ax.set_xlabel(xlabel,{"fontsize":label_size})
        ax.set_ylabel(ylabel,{"fontsize":label_size})
        ax.set_title(title,{"fontsize":label_size})

def draw_countour_limit(fig,ax,X,Y,Z,resolution,title,xlabel,ylabel,zlabel,zmin,zmax,label_size,number_size,nbins=10):
    if len(Z)>3:
        levels = MaxNLocator(nbins=nbins).tick_values(zmin,zmax)
        if resolution:
            CS = ax.contourf(X,Y,Z,resolution,levels=levels,vmin=zmin, vmax=zmax,extend='both')
        else:
            CS = ax.tricontourf(X,Y,Z,resolution,levels=levels,vmin=zmin, vmax=zmax,extend='both')
        cbar = fig.colorbar(CS,ax=ax)
        cbar.ax.set_ylabel(zlabel,{"fontsize":label_size})
        cbar.ax.tick_params(labelsize=number_size)
        # ax.clabel(CS, inline=1, fontsize=10)
        ax.set_xlabel(xlabel,{"fontsize":label_size})
        ax.set_ylabel(ylabel,{"fontsize":label_size})
        ax.set_title(title,{"fontsize":label_size})

def draw_point(ax,X,Y,param='w.'):
    ax.plot(X, Y, param)

def draw_zone_level(ax,X,Y,Z,resolution,level,color):
    if len(Z)>3:
        if np.min(Z) <= level:
            draw_ligne_level(ax,X,Y,Z,resolution,level,color)
            if resolution:
                ax.contourf(X,Y,Z, levels=[np.min(Z),level], colors=[color],alpha=0.2)
            else:
                ax.tricontourf(X,Y,Z, levels=[np.min(Z),level], colors=[color],alpha=0.2)
            # ax.clabel(level1, inline=1, fontsize=10)

def draw_ligne_level(ax,X,Y,Z,resolution,level,color):
    if len (Z) > 3 :
        if resolution:
            ax.contour(X,Y,Z, levels=[level], colors=[color],linewidths=2.0)
        else:
            ax.tricontour(X,Y,Z, levels=[level], colors=[color],linewidths=2.0)
        # ax.clabel(level1, inline=1, fontsize=10)

def set_lim(ax,ymax,ymin,xmax,xmin,number_size):
    ax.set_ylim(ymax=ymax,ymin=ymin)
    ax.set_xlim(xmax=xmax,xmin=xmin)
    ax.tick_params(axis='both', labelsize=number_size)


def print_exploration_analysis_pdf(path_figure,data_base,table_name,list_variable,resX=None,resY=None):
    ## pdf
    name_var1=list_variable[0]['name']
    name_var2=list_variable[1]['name']
    title_var1=list_variable[0]['title']
    title_var2=list_variable[1]['title']
    label_size=30.0
    number_size=20.0
    level_percentage = 0.95
    data_global=getData(data_base,table_name,list_variable,'global')
    resolution = resX != None and resY != None
    if resX == None:
        resX=len(np.unique(data_global[name_var1]))
    if resY == None:
        resY=len(np.unique(data_global[name_var2]))
    if 'disp_max' in list_variable[0].keys():
        xmax = list_variable[0]['disp_max']
    else:
        xmax = None
    if 'disp_min' in list_variable[0].keys():
        xmin = list_variable[0]['disp_min']
    else:
        xmin = None
    if 'disp_max'in list_variable[1].keys():
        ymax=list_variable[1]['disp_max']
    else :
        ymax=None
    if 'disp_min'in list_variable[1].keys():
        ymin=list_variable[1]['disp_min']
    else :
        ymin=None

    file = PdfPages(path_figure)

    ## GLOBAL
    ### Rate
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean rate
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['rates_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean firing rate of the all network',title_var1,title_var2,'Firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean rate
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['rates_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Mean firing rate of the all network',title_var1,title_var2,'Firing rate in Hz',0.0,30.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max rate
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['rates_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max firing rate of the all network',title_var1,title_var2,'Max firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min rate
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['rates_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min firing rate of the all network',title_var1,title_var2,'Min firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ## GLOBAL
    ### Rate 2
    fig_rate_2, axs_rate_2 = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate_2.subplots_adjust(hspace=1.0)

    #### Mean rate phase
    ax = axs_rate_2.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['frequency_phase_freq'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate_2,ax,X,Y,Z,resolution,'Frequency of mean phase rate of the all network',title_var1,title_var2,'Firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean rate hist
    ax = axs_rate_2.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['frequency_phase_val'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate_2,ax,X,Y,Z,resolution,'Power of frequency of mean phase of the all network',title_var1,title_var2,'power spectrum',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max rate
    ax = axs_rate_2.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['frequency_hist_1_freq'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate_2,ax,X,Y,Z,resolution,'Frequency rate of the all network',title_var1,title_var2,'Firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min rate
    ax = axs_rate_2.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['frequency_hist_1_val'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate_2,ax,X,Y,Z,resolution,'Power of frequence hist of the all network',title_var1,title_var2,'Power spectrum',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Synchronize
    fig_synch, axs_synch = plt.subplots(2, 2,figsize=(20, 20))
    fig_synch.subplots_adjust(hspace=1.0)

    #### CV_IFR_1ms
    ax = axs_synch.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['cvs_IFR_1ms'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,1.0,'red')
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Cv of instantaneous firing rate\n with bins = 1ms',title_var1,title_var2,'Cv IFR',label_size,number_size)
    # draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'Cv of instantaneous firing rate\n with bins = 1ms',title_var1,title_var2,'Cv IFR',0.0,2.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### CV_IFR_3ms
    ax = axs_synch.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['cvs_IFR_3ms'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,1.0,'red')
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Cv of instantaneous firing rate\n with bins = 3ms',title_var1,title_var2,'Cv IFR',label_size,number_size)
    # draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'Cv of instantaneous firing rate\n with bins = 3ms',title_var1,title_var2,'Cv IFR',0.0,2.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### R_synch
    ax = axs_synch.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['synch_Rs_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Mean R synchronization',title_var1,title_var2,'R synchronization',label_size,number_size)
    # draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'Mean R synchronization',title_var1,title_var2,'R synchronization',0.5,1.0,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['cvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,0.5,'blue')
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Synchronization 2
    fig_reg, axs_sync_2 = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### R_synch min
    ax = axs_sync_2.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['synch_Rs_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Min R synchronization',title_var1,title_var2,'R synchronization',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### R_synch max
    ax = axs_sync_2.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['synch_Rs_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Max R synchronization',title_var1,title_var2,'R synchronization',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### R_synch std
    ax = axs_sync_2.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['synch_Rs_std'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Variation R synchronization',title_var1,title_var2,'R synchronization',label_size,number_size)
    # draw_zone_level(ax,X,Y,Z,resolution,0.015,'green')
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### R_synch cv
    ax = axs_sync_2.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],np.divide(data_global['synch_Rs_std'],data_global['synch_Rs_average'],where=np.array(data_global['synch_Rs_average']) != None),res=resolution,resX=resX,resY=resY)
    draw_countour(fig_synch,ax,X,Y,Z,resolution,'Coefficient of Variation R synchronization',title_var1,title_var2,'R synchronization',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Regularity
    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### CV_ISI
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['cvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'Cv of interspiking intervalle',title_var1,title_var2,'Cv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### LV_ISI
    ax = axs_reg.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['lvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Percentage
    ax = axs_reg.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,level_percentage,'red')
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['percentage'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'Percentage of analysed neuron',title_var1,title_var2,'percentage',0.0,1.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### ISI
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean ISI
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean of Inter-Spiking interval\n of the all network',title_var1,title_var2,'ISI in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Std ISI
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['ISI_std'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Std Inter-Spiking interval\n of the all network',title_var1,title_var2,'ISI in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max ISI
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['ISI_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max Inter-Spiking interval\n of the all network',title_var1,title_var2,'Max ISI in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min ISI
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['ISI_min'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,11.0,'blue')
    draw_ligne_level(ax,X,Y,Z,resolution,0.1,'red')
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min Inter-Spiking interval\nof the all network',title_var1,title_var2,'Min ISI in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ## Global Burst
    ### Burst Rate
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean Burst rate
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_rate_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean burst firing rate of the all network',title_var1,title_var2,'Firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean Burst rate
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_rate_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Mean burst firing rate of the all network',title_var1,title_var2,'Firing rate in Hz',0.0,30.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max Burst rate
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_rate_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max burst firing rate of the all network',title_var1,title_var2,'Max firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min Burst rate
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_rate_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min burst firing rate of the all network',title_var1,title_var2,'Min firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Nb Burst
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean Nb Burst
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_count_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean number of spike in burst of the all network',title_var1,title_var2,'Number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Std Nb Burst
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_count_std'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Std number of spike in burst\n of the all network',title_var1,title_var2,'Number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max Nb Burst
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_count_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max number of spike in burst\n of the all network',title_var1,title_var2,'Max number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min Nb Burst
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_count_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min number of spikes in burst\n of the all network',title_var1,title_var2,'Min number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Time Burst
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean Time of Burst
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_interval_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean time of burst of the all network',title_var1,title_var2,'Time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Std time Burst
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_interval_std'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Std time of burst of the all network',title_var1,title_var2,'Time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max time Burst
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_interval_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max time of burst of the all network',title_var1,title_var2,'Max time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min time Burst
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_interval_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min time of burst of the all network',title_var1,title_var2,'Min time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')


    ### Burst Regularity
    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### Begin CV_ISI
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_cv_begin_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'Cv of interspiking intervalle begin burst',title_var1,title_var2,'Cv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Begin LV_ISI
    ax = axs_reg.ravel()[1]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_lv_begin_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### End CV_ISI
    ax = axs_reg.ravel()[2]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_cv_end_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'Cv of interspiking intervalle end burst',title_var1,title_var2,'Cv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### End LV_ISI
    ax = axs_reg.ravel()[3]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['burst_lv_end_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',label_size,number_size)
    X_mor,Y_more,Z_more=grid(data_global[name_var1],data_global[name_var2],data_global['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    #Burst percentage

    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### Percentage
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,level_percentage,'red')
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'Burst Percentage of analysed neuron',title_var1,title_var2,'percentage',0.0,1.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')


    ## INHIBITORY EXCITATORY
    data_excitatory=getData(data_base,table_name,list_variable,'excitatory')
    data_inihibtory=getData(data_base,table_name,list_variable,'inhibitory')
    ### Rate
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean rate E
    zmin=np.min(np.concatenate((data_excitatory['rates_average'],data_inihibtory['rates_average'])))
    zmax=np.max(np.concatenate((data_excitatory['rates_average'],data_inihibtory['rates_average'])))
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['rates_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Mean firing rate of excitatory neurons',title_var1,title_var2,'Firing rate in Hz',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean rate I
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['rates_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Mean firing rate of inhibitory neurons',title_var1,title_var2,'Firing rate in Hz',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean rate E
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['rates_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Mean firing rate of excitatory neurons',title_var1,title_var2,'Firing rate in Hz',0.0,30.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean rate I
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['rates_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Mean firing rate of inhibitory neurons',title_var1,title_var2,'Firing rate in Hz',0.0,30.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Max rate E
    zmin=np.min(np.concatenate((data_excitatory['rates_max'],data_inihibtory['rates_max'])))
    zmax=np.max(np.concatenate((data_excitatory['rates_max'],data_inihibtory['rates_max'])))
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['rates_max'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Max firing rate of excitatory neurons',title_var1,title_var2,'Max firing rate in Hz',
                  zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max rate I
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['rates_max'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Max firing rate of inhibitory neurons',title_var1,title_var2,'Max firing rate in Hz',
                  zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min rate E
    zmin=np.min(np.concatenate((data_excitatory['rates_min'],data_inihibtory['rates_min'])))
    zmax=np.max(np.concatenate((data_excitatory['rates_min'],data_inihibtory['rates_min'])))
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['rates_min'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Min firing rate of exitatory neurons',title_var1,title_var2,'Min firing rate in Hz',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min rate I
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['rates_min'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_rate,ax,X,Y,Z,resolution,'Min firing rate of inhibitory neurons',title_var1,title_var2,'Min firing rate in Hz',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)
    # plt.savefig(file, format='pdf')
    plt.close('all')

    ### Synchronize
    fig_synch, axs_synch = plt.subplots(2, 2,figsize=(20, 20))
    fig_synch.subplots_adjust(hspace=1.0)

    #### CV_IFR_1ms E
    zmin=np.min(np.concatenate((data_excitatory['cvs_IFR_1ms'],data_inihibtory['cvs_IFR_1ms'])))
    zmax=np.max(np.concatenate((data_excitatory['cvs_IFR_1ms'],data_inihibtory['cvs_IFR_1ms'])))
    if zmin == None:
        zmin=0.0
    # zmin = 0.0
    # zmax = 2.0
    ax = axs_synch.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['cvs_IFR_1ms'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,1.0,'red')
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'E:Cv of instantaneous firing rate\n with bins = 1ms',title_var1,title_var2,'Cv IFR',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### CV_IFR_1ms I
    ax = axs_synch.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['cvs_IFR_1ms'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,1.0,'red')
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'I:Cv of instantaneous firing rate\n with bins = 1ms',title_var1,title_var2,'Cv IFR',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### CV_IFR_3ms E
    zmin=np.min(np.concatenate((data_excitatory['cvs_IFR_3ms'],data_inihibtory['cvs_IFR_3ms'])))
    zmax=np.max(np.concatenate((data_excitatory['cvs_IFR_3ms'],data_inihibtory['cvs_IFR_3ms'])))
    if zmin == None:
        zmin=0.0
    # zmin = 0.0
    # zmax = 2.0
    ax = axs_synch.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['cvs_IFR_3ms'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,1.0,'red')
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'E:Cv of instantaneous firing rate\n with bins = 3ms',title_var1,title_var2,'Cv IFR',
                  zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### CV_IFR_3ms I
    ax = axs_synch.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['cvs_IFR_3ms'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,1.0,'red')
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'I:Cv of instantaneous firing rate\n with bins = 3ms',title_var1,title_var2,'Cv IFR',
                        zmin,zmax,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    fig_synch, axs_synch = plt.subplots(2, 2,figsize=(20, 20))
    fig_synch.subplots_adjust(hspace=1.0)

    #### R_synch E
    zmin=np.min(np.concatenate((data_excitatory['synch_Rs_average'],data_inihibtory['synch_Rs_average'])))
    zmax=np.max(np.concatenate((data_excitatory['synch_Rs_average'],data_inihibtory['synch_Rs_average'])))
    if zmin == None:
        zmin=0.0
    ax = axs_synch.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['synch_Rs_average'],res=resolution,resX=resX,resY=resY)
    # draw_countour(fig_synch,ax,X,Y,Z,resolution,'E:Mean R synchronization',title_var1,title_var2,'R synchronization',
    #                     label_size,number_size)
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'E:Mean R synchronization',title_var1,title_var2,'R synchronization',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['cvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,0.5,'blue')
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### R_synch I
    ax = axs_synch.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['synch_Rs_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'I:Mean R synchronization',title_var1,title_var2,'R synchronization',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['cvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,0.5,'blue')
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### std R_synch E
    zmin=np.min(np.concatenate((data_excitatory['synch_Rs_std'],data_inihibtory['synch_Rs_std'])))
    zmax=np.max(np.concatenate((data_excitatory['synch_Rs_std'],data_inihibtory['synch_Rs_std'])))
    if zmin == None:
        zmin=0.0
    ax = axs_synch.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['synch_Rs_std'],res=resolution,resX=resX,resY=resY)
    # draw_countour(fig_synch,ax,X,Y,Z,resolution,'E:Mean R synchronization',title_var1,title_var2,'R synchronization',
    #                     label_size,number_size)
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'E:std R synchronization',title_var1,title_var2,'R synchronization',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### R_synch I
    ax = axs_synch.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['synch_Rs_std'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_synch,ax,X,Y,Z,resolution,'I:std R synchronization',title_var1,title_var2,'R synchronization',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Regularity
    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### CV_ISI E
    zmin=np.min(np.concatenate((data_excitatory['cvs_ISI_average'],data_inihibtory['cvs_ISI_average'])))
    zmax=np.max(np.concatenate((data_excitatory['cvs_ISI_average'],data_inihibtory['cvs_ISI_average'])))
    if zmin == None:
        zmin=0.0
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['cvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'E:Cv of interspiking intervalle',title_var1,title_var2,'Cv ISI',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### CV_ISI I
    ax = axs_reg.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['cvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'I:Cv of interspiking intervalle',title_var1,title_var2,'Cv ISI',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### LV_ISI E
    zmin=np.min(np.concatenate((data_excitatory['lvs_ISI_average'],data_inihibtory['lvs_ISI_average'])))
    zmax=np.max(np.concatenate((data_excitatory['lvs_ISI_average'],data_inihibtory['lvs_ISI_average'])))
    if zmin == None:
        zmin=0.0
    ax = axs_reg.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['lvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'E:Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### LV_ISI I
    ax = axs_reg.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['lvs_ISI_average'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'I:Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',
                        zmin,zmax,label_size,number_size)
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')
    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### Percentage
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,level_percentage,'red')
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'E:Percentage of analysed neuron',title_var1,title_var2,'percentage',0.0,1.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Percentage
    ax = axs_reg.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,level_percentage,'red')
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'I:Percentage of analysed neuron',title_var1,title_var2,'percentage',0.0,1.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)


    plt.savefig(file, format='pdf')
    plt.close('all')

    ## Global Burst
    ### Burst Rate
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean Burst rate
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_rate_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean burst firing rate of excitatory neurons',title_var1,title_var2,'Firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean Burst rate
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_rate_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean burst firing rate of inhibitory neurons',title_var1,title_var2,'Firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max Burst rate
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_rate_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max burst firing rate of excitatory neurons',title_var1,title_var2,'Max firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max Burst rate
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_rate_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max burst firing rate of inhibitory neurons',title_var1,title_var2,'Max firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Min Burst rate
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_rate_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min burst firing rate of excitatory neurons',title_var1,title_var2,'Min firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min Burst rate
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_rate_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min burst firing rate of inhibitory neurons',title_var1,title_var2,'Min firing rate in Hz',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean Nb Burst
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_count_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean number of spike in burst of excitatory neurons',title_var1,title_var2,'Number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Mean Nb Burst
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_count_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean number of spike in burst of inhibitory neurons',title_var1,title_var2,'Number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Nb Burst
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Max Nb Burst
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_count_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max number of spike in burst\n of excitatory neurons',title_var1,title_var2,'Max number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max Nb Burst
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_count_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max number of spike in burst\n of inhibitory neurons',title_var1,title_var2,'Max number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min Nb Burst
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_count_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min number of spikes in burst\n of excitatory neurons',title_var1,title_var2,'Min number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min Nb Burst
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_count_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min number of spikes in burst\n of inhibitory neurons',title_var1,title_var2,'Min number of spikes',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)


    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Time Burst
    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Mean Time of Burst
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_interval_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean time of burst of excitatory neurons',title_var1,title_var2,'Time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    ### Mean Time of Burst
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_interval_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Mean time of burst of inhibitory neurons',title_var1,title_var2,'Time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max time Burst
    ax = axs_rate.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_interval_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max time of burst of excitatory neurons',title_var1,title_var2,'Max time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Max time Burst
    ax = axs_rate.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_interval_max'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Max time of burst of inhibitory neurons',title_var1,title_var2,'Max time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    fig_rate, axs_rate = plt.subplots(2, 2,figsize=(20, 20))
    fig_rate.subplots_adjust(hspace=1.0)

    #### Min time Burst
    ax = axs_rate.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_interval_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min time of burst of excitatory neurons ',title_var1,title_var2,'Min time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Min time Burst
    ax = axs_rate.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_interval_min'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_rate,ax,X,Y,Z,resolution,'Min time of burst of inhibitory neurons',title_var1,title_var2,'Min time of burst in ms',label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    ### Burst Regularity
    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### Begin CV_ISI
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_cv_begin_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'E : Cv of interspiking intervalle begin burst',title_var1,title_var2,'Cv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Begin CV_ISI
    ax = axs_reg.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_cv_begin_average'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,0.2,'blue')
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'I : Cv of interspiking intervalle begin burst',title_var1,title_var2,'Cv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Begin LV_ISI
    ax = axs_reg.ravel()[2]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['burst_lv_begin_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'E : Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Begin LV_ISI
    ax = axs_reg.ravel()[3]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['burst_lv_begin_average'],res=resolution,resX=resX,resY=resY)
    draw_countour(fig_reg,ax,X,Y,Z,resolution,'I : Lv of interspiking intervalle',title_var1,title_var2,'Lv ISI',label_size,number_size)
    X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    #Burst percentage
    fig_reg, axs_reg = plt.subplots(2, 2,figsize=(20, 20))
    fig_reg.subplots_adjust(hspace=1.0)

    #### Percentage
    ax = axs_reg.ravel()[0]
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,level_percentage,'red')
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'E : Burst Percentage of analysed neuron',title_var1,title_var2,'percentage',0.0,1.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    #### Percentage
    ax = axs_reg.ravel()[1]
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_ligne_level(ax,X,Y,Z,resolution,level_percentage,'red')
    X,Y,Z=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage_burst'],res=resolution,resX=resX,resY=resY)
    draw_countour_limit(fig_reg,ax,X,Y,Z,resolution,'I : Burst Percentage of analysed neuron',title_var1,title_var2,'percentage',0.0,1.0,label_size,number_size)
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)

    plt.savefig(file, format='pdf')
    plt.close('all')

    file.close()
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_no_long.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])


# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_medium_300.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_300/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_medium_200.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_200/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_medium_100.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_100/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_medium_50.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_50/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_high_300.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_high_200.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_200/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_high_100_zoom.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_100/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 40.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_high_50_zoom.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 40.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_high.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_10_high/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_low.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_10_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_medium.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_10_medium/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_no.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_10_no/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_high.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_high/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_low.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_medium.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/single_10_no.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])




# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/test/ex.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/test/10_high_ex/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/test/2.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/test/10_high_2/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/test/single.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/test/single_10_high_2/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])

# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/8.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/8/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/10_low.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/10_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/10_high.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/10_high/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/10_no.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/10_no/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/10_g_15.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/10_g_15/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/10_g_02.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/10_g_02/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/single/pdf/10_g_004.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/single/10_g_004/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])


# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/8.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/8/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_low.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/10_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_high.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/10_high/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_no.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/10_no/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_g_15.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/10_g_015/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_g_02.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/10_g_02/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])
# print_exploration_analysis_pdf('/home/kusch//Documents/project/Jirsa/data/mechanism/pdf/10_g_004.pdf','/home/kusch//Documents/project/Jirsa/data/mechanism/10_g_004/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 800.0, 'min': 0.0},
#                                    {'name': 'weight_long', 'title': ' weight along long range connection', 'max': 50.0, 'min': 0.0},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/g_delay_low_3_precise_2.pdf','/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low_3/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'g', 'title': 'relative strenght between excitatory and inhibitory ', 'max': 0.2, 'min': 0.15},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/g_delay_low_2_precision_2.pdf','/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low_2/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'g', 'title': 'relative strenght between excitatory and inhibitory ', 'max': 0.06, 'min': 0.05},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/g_delay_low_0_low_2.pdf','/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'g', 'title': 'relative strenght between excitatory and inhibitory ', 'max': 0.15, 'min': 0.07},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/delay_gc_2_low.pdf','/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'weight_excitatory', 'title': 'local strength ', 'max': 2.0, 'min': 0.0},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/delay_gc_2_high.pdf','/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_low/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'weight_excitatory', 'title': 'local strength ', 'max': 50.0, 'min': 4.0},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/delay_gc_limit_2_2.pdf','/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_limit_2/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'weight_excitatory', 'title': 'local strength ', 'max': 200.0, 'min': 1.0},
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/delay_gc_limit_inter.pdf','/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_limit/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'weight_excitatory', 'title': 'local strength ', 'max': 16.0, 'min': 11.0},
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/delay_gc_low.pdf','/home/kusch/Documents/project/Jirsa/data/sub/delay_gc/database.db','exploration_1',
#                                [
#                                    {'name': 'tau_long', 'title': ' delay along long range connection', 'max': 400.0, 'min': 0.0},
#                                    {'name': 'weight_excitatory', 'title': 'local strength ', 'max': 1.0, 'min': 0.0},
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/sub/pdf/sub_noise_gc_6.pdf','/home/kusch/Documents/project/Jirsa/data/sub/sub_noise_gc/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_noise', 'title': ' sigma noise in pA', 'max': 300.0, 'min': 100.0},
#                                    {'name': 'weight_excitatory', 'title': 'local strenght ', 'max': 100.0, 'min': 8.0},
#
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/presentation/Without_long.pdf','/home/kusch/Documents/project/Jirsa/data/presentation/Without_long/database.db','exploration_1',
#                                [
#                                    {'name': 'weight_excitatory', 'title': 'weight_excitatory', 'max': 200.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_low_2.pdf','/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_low/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'external curent', 'max': 20.0, 'min': 10.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_without_long_low.pdf','/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_without_long_low/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'external curent', 'max': 20.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_without_long.pdf','/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_without_long/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'external curent', 'max': 20.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_high.pdf','/home/kusch/Documents/project/Jirsa/data/presentation/I_sigma_as_high/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'external curent', 'max': 20.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])


# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/zerlaut_parameter/pdf/delay_vs_gc_low_3.pdf','/home/kusch/Documents/project/Jirsa/data/zerlaut_parameter/delay_vs_gc/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': 'strengh local connection', 'max': 0.6, 'min': 0.0, },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/zerlaut_parameter/pdf/delay_vs_long_without_long.pdf','/home/kusch/Documents/project/Jirsa/data/zerlaut_parameter/delay_vs_long_without_long/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': '  weight along long connection', 'max': 500, 'min': 0.0, },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/zerlaut_parameter/pdf/delay_vs_long.pdf','/home/kusch/Documents/project/Jirsa/data/zerlaut_parameter/delay_vs_long/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': '  weight along long connection', 'max': 500, 'min': 0.0, },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/gc_delay_as_t_ref_1_5_long_double_without_noise_long_3.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_3/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 1.4, 'min': 1.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 1.4, 'min': 1.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/gc_delay_as_t_ref_1_5_long_double_without_noise_long.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 1.4, 'min': 1.0, },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_3_t_ref_1_5_long_double_without_noise_low_1.pdf','/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 1.4, 'min': 1.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_3_t_ref_1_5_long_double_without_noise_low.pdf','/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 2.0, 'min': 0., },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_3_t_ref_1_5_long_double_without_noise_high.pdf','/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 50.0, 'min': 21., },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_3_t_ref_1_5_long_double_without_noise.pdf','/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': '  weight local', 'max': 50.0, 'min': 0., },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_t_ref_1_5_long_double_without_noise_long_connection_2.pdf','/home/kusch/Documents/project/Jirsa/data/as_t_ref_1_5_long_double_without_noise_long_connection_2/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'conn_sigma_excitatory_long', 'title': ' sigma long range connection', 'max': 4.0, 'min': 0., },
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_t_ref_1_5_long_double_without_noise_long_connection.pdf','/home/kusch/Documents/project/Jirsa/data/as_t_ref_1_5_long_double_without_noise_long_connection/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'conn_sigma_excitatory_long', 'title': ' sigma long range connection', 'max': 5.0, 'min': 0., },
#                                ])


# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_without_noise_long_zoom_0.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise_2/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 4.0, 'min': 0., },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_without_noise_long_zoom.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise_2/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 4.0, 'min': 0.1, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_without_noise_long_0.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise_2/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 50.0, 'min': 0., },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_without_noise_long.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise_2/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 50.0, 'min': 0.1, },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_5_5.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_5_5/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 50.0, 'min': 0.0, },
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_without_noise_0.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 1.0, 'min': 0., },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_zoom_2.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 4.0, 'min': 0.01, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double_zoom.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 5.0, 'min': 0.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_2_gc_delay_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_long', 'title': ' weight', 'max': 50.0, 'min': 0.0, },
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_1_bis.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 30.0, 'min': 24.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_2.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 10.0, 'min': 0.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_3.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 10.0, 'min': 0.1, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_4.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 5.0, 'min': 0.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_5.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 5.0, 'min': 0.01, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_6.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 2.0, 'min': 0.0, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double_zoom_7.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 2.0, 'min': 0.01, },
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/as_gc_delay_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/as_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'tau_long','title':' tau ','max':600.0,'min':0.0},
#                                    {'name': 'weight_excitatory', 'title': ' weight local', 'max': 30.0, 'min': 0.0, },
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/state_zoom_2.pdf','/home/kusch/Documents/project/Jirsa/data/state_t_ref_1_5_long/database.db','exploration_1',
#                                [
#                                    {'name': 'dim_x', 'title': ' space of cell', 'max': 5.0, 'min': 0.0, },
#                                    {'name':'g','title':' relative strengh inhibitory excitatory','max':2.0,'min':0.0},
#
#                                 ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/state_zoom.pdf','/home/kusch/Documents/project/Jirsa/data/state_t_ref_1_5_long/database.db','exploration_1',
#                                [
#                                    {'name': 'dim_x', 'title': ' space of cell', 'max': 50.0, 'min': 0.0, },
#                                    {'name':'g','title':' relative strengh inhibitory excitatory','max':2.0,'min':0.0},
#
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/state.pdf','/home/kusch/Documents/project/Jirsa/data/state_t_ref_1_5_long/database.db','exploration_1',
#                                [
#                                    {'name': 'dim_x', 'title': ' space of cell', 'max': 50.0, 'min': 0.0, },
#                                    {'name':'g','title':' relative strengh inhibitory excitatory','max':50,'min':0.0},
#
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/per_7_bis.pdf','/home/kusch/Documents/project/Jirsa/data/per_0_7_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':50,'min':11.0},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 320.0, 'min': 80.0, }
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/per_5.pdf','/home/kusch/Documents/project/Jirsa/data/per_0_5_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':50,'min':0.0},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 320.0, 'min': 80.0, }
#                                 ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/per_3.pdf','/home/kusch/Documents/project/Jirsa/data/per_0_3_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':50,'min':0.0},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 320.0, 'min': 80.0, }
#                                 ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/per_01.pdf','/home/kusch/Documents/project/Jirsa/data/per_0_1_gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':1000.0,'min':0.0},
#                                     {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0, }
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/pdf/per_0.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':6.0,'min':2.5},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 320.0, 'min': 80.0, }
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_E_high_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_E_high_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_E_medium_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_E_medium_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_E_low_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_E_low_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_EI_high_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_EI_high_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_EI_medium_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_EI_medium_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_EI_low_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_EI_low_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_EE_high_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_EE_high_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_EE_medium_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_EE_medium_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/connection_EE_low_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/connection_EE_low_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'distance', 'title': 'distance', 'max': 25.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double_zoom_4.pdf','/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'I external', 'max': 5.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double_zoom_3.pdf','/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'I external', 'max': 6.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double_zoom_2.pdf','/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'I external', 'max': 10.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double_zoom.pdf','/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name': 'sigma_I_ext', 'title': 'I external', 'max': 50.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#                                ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/I_sigma_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                 {'name': 'sigma_I_ext', 'title': 'I external', 'max': 1000.0, 'min': 0.0},
#                                    {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0},
#
#
#                                 ])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':1000.0,'min':0.0},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0, }
#                                 ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double_zoom.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':5.0,'min':0.0},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0, }
#                                 ])
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double_zoom_2.pdf','/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db','exploration_1',
#                                [
#                                    {'name':'weight_excitatory','title':'global coupling','max':4.5,'min':2.5},
#                                 {'name': 'tau_long', 'title': ' tau long in ms', 'max': 600.0, 'min': 0.0, }
#                                 ])

# print_exploration_analysis_pdf('./data_2/size_find.pdf','./data_2/size_find/database.db','exploration_1',
#                                [{'name':'nb_cube_x','title':'nb cube on x','max':100,'min':0,},
#                                 {'name':'nb_cube_y','title':'nb cube on y','max':100,'min':0}])

# print_exploration_analysis_pdf('./data_2/size_find_t_ref_0_4_I_sigma_0.pdf','./data_2/size_find_t_ref_0_4_I_sigma_0/database.db','exploration_1',
#                                [{'name':'nb_cube_x','title':'nb cube on x','max':100,'min':0,},
#                                 {'name':'nb_cube_y','title':'nb cube on y','max':100,'min':0}])

# print_exploration_analysis_pdf('./data_2/size_find_t_ref_0_4.pdf','./data_2/size_find_t_ref_0_4/database.db','exploration_1',
#                                [{'name':'nb_cube_x','title':'nb cube on x','max':100,'min':0,},
#                                 {'name':'nb_cube_y','title':'nb cube on y','max':100,'min':0}])
# print_exploration_analysis_pdf('./data_2/size_find_t_ref_1_5_sigma_I_0.pdf','./data_2/size_find_t_ref_1_5_sigma_I_0/database.db','exploration_1',
#                                [{'name':'nb_cube_x','title':'nb cube on x','max':100,'min':0,},
#                                 {'name':'nb_cube_y','title':'nb cube on y','max':100,'min':0}])

# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_t_ref_0_4.pdf','./data_2/big_layer_weight_long_range_connection_t_ref_0_4/database.db','exploration_1',
#                                [
#                                    {'name':'weight_long','title':'weight of long range connexion','max':10.0,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])
#
# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_3.pdf','./data_2/big_layer_weight_long_range_connection_3/database.db','exploration_1',
#                                [
#                                    {'name':'weight_long','title':'weight of long range connexion','max':10.0,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])


# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_2_zoom_4.pdf','./data_2/big_layer_weight_long_range_connection_2/database.db','exploration_1',
#                                [
#                                    {'name':'weight_long','title':'weight of long range connexion','max':0.2,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])
# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_2_zoom_3.pdf','./data_2/big_layer_weight_long_range_connection_2/database.db','exploration_1',
#                                [
#                                    {'name':'weight_long','title':'weight of long range connexion','max':0.2,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])
# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_2_zoom_2.pdf','./data_2/big_layer_weight_long_range_connection_2/database.db','exploration_1',
#                                [
#                                    {'name':'weight_long','title':'weight of long range connexion','max':1.0,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])
# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_2_zoom.pdf','./data_2/big_layer_weight_long_range_connection_2/database.db','exploration_1',
#                                [
#                                 {'name':'weight_long','title':'weight of long range connexion','max':5.0,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])
# print_exploration_analysis_pdf('./data_2/big_layer_weight_long_range_connection_2.pdf','./data_2/big_layer_weight_long_range_connection_2/database.db','exploration_1',
#                                [
#                                    {'name':'weight_long','title':'weight of long range connexion','max':100.0,'min':-1.0},
#                                    {'name':'tau_long','title':' tau long in ms','max':600.0,'min':0.0,},
#                                ])

# print_exploration_analysis_pdf('./big_layer_long_range_t_ref_1_5.pdf','./big_layer_long_range_t_ref_1_5/database.db','exploration_1',
#                                 [
#                                  {'name':'sigma_I_ext','title':'sigma I ext in pA','max':1200.0,'min':0.0},
#                                  {'name': 'tau_long', 'title': 'tau long in ms', 'max': 10000.0, 'min': -1.0}
#                                 ])
# print_exploration_analysis_pdf('./data_2/big_layer_g_I_ext_t_ref_0_4.pdf','./data_2/big_layer_g_I_ext_t_ref_0_4/database.db','exploration_1',
#                                 [{'name':'g','title':'g','max':10.0,'min':-1.0,},
#                                  {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])
# print_exploration_analysis_pdf('./big_layer_g_I_ext_t_ref_1.5_zoom_2.pdf','./big_layer_g_I_ext/database.db','exploration_1',
#                                [{'name':'g','title':'g','max':1.7,'min':-1.0,},
#                                 {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])
# print_exploration_analysis_pdf('./big_layer_g_I_ext_t_ref_1.5_zoom.pdf','./big_layer_g_I_ext/database.db','exploration_1',
#                                [{'name':'g','title':'g','max':2.0,'min':-1.0,},
#                                 {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])
# print_exploration_analysis_pdf('./data_2/big_layer_g_I_ext_t_ref_1_5.pdf','./data_2/big_layer_g_I_ext_t_ref_1_5/database.db','exploration_1',
#                                 [{'name':'g','title':'g','max':10.0,'min':-1.0,},
#                                  {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])


# print_exploration_analysis_pdf('./data_2/big_layer_g_I_ext_t_ref_0_4_zoom.pdf','./data_2/big_layer_g_I_ext_t_ref_0_4/database.db','exploration_1',
#                                 [{'name':'g','title':'g','max':2.0,'min':-1.0,},
#                                  {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])
# print_exploration_analysis_pdf('./data_2/big_layer_g_I_ext_t_ref_1_5_zoom.pdf','./data_2/big_layer_g_I_ext_t_ref_1_5/database.db','exploration_1',
#                                 [{'name':'g','title':'g','max':2.0,'min':-1.0,},
#                                  {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('./t_ref_I_ext_test4.pdf','../example/database_t_ref_3.db','test1',
#                                [{'name':'t_ref','title':'t refactory in ms','max':3.0,'min':-1.0},
#                                 {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])
# print_exploration_analysis_pdf('./g_I_ext_test4.pdf','../example/database_g_3.db','test1',
#                                [{'name':'g','title':'g','max':4.0,'min':0.0},
#                                 {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}])


# print_exploration_analysis_pdf('./t_ref_I_ext_test4.pdf','../example/database_t_ref_3.db','test1',
#                                [{'name':'t_ref','title':'t refactory in ms','max':3.0,'min':0.15},
#                                 {'name':'mean_I_ext','title':'I ext in pA','max':920.0,'min':840.0}])
# print_exploration_analysis_pdf('./t_ref_I_ext_test3.pdf','../example/database_t_ref_3.db','test1',
#                                [{'name':'t_ref','title':'t refactory in ms','max':3.0,'min':0.15},
#                                 {'name':'mean_I_ext','title':'I ext in pA','max':1200.0,'min':0.0}],resX=20,resY=20)

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/database_random_w.db','experience_1',
#                                [{'name':'mean_w_0','title':'random init w','max':100.0,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])
#
#
# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/analysis_zoom.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/database_random_w.db','experience_1',
#                                [{'name':'mean_w_0','title':'random init w','max':20.0,'min':10.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/analysis_zoom_2.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/database_random_w.db','experience_1',
#                                [{'name':'mean_w_0','title':'random init w','max':17.0,'min':14.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/analysis_previous_1.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/database_1.db','experience_1',
#                                [{'name':'percentage_inactive_neurons','title':'% inactive neurons','max':1.0,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_without_coupling/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_without_coupling/database_random_w.db','experience_1',
#                               [{'name':'mean_w_0','title':'random init w','max':300.0,'min':0.0},
#                                {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/database.db','experience_1',
#                                [{'name':'mean_I_ext','title':'random init w','max':1000.0,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/analysis_zoom.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/database.db','experience_1',
#                                [{'name':'mean_I_ext','title':'I external','max':1000.0,'min':600.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])


# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/database.db','experience_1',
#                                [{'name':'weight_excitatory','title':'global coupling','max':1000.0,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/analysis_zoom_4.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/database.db','experience_1',
#                                [{'name':'weight_excitatory','title':'global coupling','max':0.18,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])


# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse/database_random_w.db','experience_1',
#                               [{'name':'mean_w_0','title':'random init w','max':300.0,'min':0.0},
#                                {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])
#

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_long_v2_0/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_long_2/database_random_w.db','experience_1',
#                               [{'name':'mean_w_0','title':'random init w','max':300.0,'min':0.0},
#                                {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])

# print_exploration_analysis_pdf('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_long_v2_4/analysis.pdf','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_long_v3_current/database.db','experience_1',
#                                [{'name':'mean_w_0','title':'random init w','max':300.0,'min':0.0},
#                                 {'name':'master_seed','title':'master seed','max':1200.0,'min':0.0}])
