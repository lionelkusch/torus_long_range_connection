import numpy as np
import matplotlib.pylab as plt
from matplotlib import gridspec
from elephant.statistics import isi
from analysis_type import get_gids
import os

def hist_list(spikes,begin,end,resolution):
        spikes_concat = np.concatenate(([begin],spikes,[end]))
        if int(end - begin) > 0:
            hist_0 = np.histogram(spikes_concat, bins=int((end - begin)/resolution))  # for bins at 1 milisecond
            hist_0[0][0]-=1
            hist_0[0][-1]-=1
        else:
            hist_0 = None
        return hist_0

def compute_hist_element(gids,data,begin,end,resolution):
    spike_time = np.concatenate([ data[h][1,:] for h in gids.keys()])
    hist = hist_list(spike_time,begin,end,resolution)
    return hist


def load_spike_ids(gids_all,path, begin, end):
    """
    Get the id of the neurons which create the spike
    :param gids: the array of the id and name
    :param path: the path to the file
    :param begin: the first time
    :param end: the end of time
    :return: The spike of all neurons between end and begin
    """
    data = {}
    data_concatenated = np.loadtxt(path + "/spike_detector.gdf")
    if data_concatenated.size < 5:
        print('empty file')
        return -1
    data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
    idx_time = ((data_raw[:, 1] > begin) * (data_raw[:, 1] < end))
    data_tmp = data_raw[idx_time]
    for name,gids in gids_all.items():
        data[name]=[]
        for i in list(range(len(gids))):
            idx_id = ((data_tmp[:, 0] >= gids[i][0]) * (data_tmp[:, 0] <= gids[i][1]))
            data[name].append(data_tmp[idx_id])
        data[name]=np.swapaxes(np.concatenate(data[name],axis=0),0,1)
    return data

def detection_burst(gids, data, begin, end,limit_burst,limit_spike=20):
    """
    Compute different measure on the spike
    :param gids: the array with the id
    :param data: the times spikes
    :param begin: the first time
    :param end: the end time
    :return:
    """
    nb_neuron = 0
    for gid in gids:
        nb_neuron = nb_neuron + gid[1] - gid[0]
    spikes = []
    list_burst=[]
    for j,gid in enumerate(gids):
        for i in np.arange(gid[0], gid[1], 1):
            spike_i = data[1,np.where(data[0, :] == i)]
            spike_i = (spike_i[np.where(spike_i >= begin)]).flatten()
            spike_i = (spike_i[np.where(spike_i <= end)])
            spikes.append(spike_i)
            isis = isi(spike_i)
            isis_add = np.insert(isis,0,limit_burst+10.0)
            isis_add = np.append(isis_add,limit_burst+10.0)
            id_begin_burst = np.where((isis_add>limit_burst) & (np.roll(isis_add,-1) <= limit_burst))[0]
            id_end_burst = np.where((isis_add <= limit_burst) & (np.roll(isis_add,-1)> limit_burst))[0]
            time_start_burst = spike_i[id_begin_burst]
            time_stop_burst = spike_i[id_end_burst]
            nb_burst = id_end_burst-id_begin_burst+1
            list_burst.append(np.array([i*np.ones_like(time_start_burst),time_start_burst,time_stop_burst,nb_burst]))
    if list_burst == []:
        return []
    else:
        return np.concatenate(list_burst,axis=1)

def print_spike(path, begin, end):
    '''
    print spike train
    :param path:
    :param begin:
    :param end:
    :return:
    '''
    gids_all = get_gids(path)
    data_pop_all = load_spike_ids(gids_all,path, begin, end)

    # # print spike
    fig = plt.figure(figsize=(18, 8))
    color=['blue','red']
    for pop,[neurons_id,times_spike] in enumerate(data_pop_all.values()):
            plt.plot(times_spike, neurons_id,'.',color=color[pop],markersize=1)
    # for pop,(name,gids) in enumerate(gids_all.items()):
        # burst = detection_burst(gids, data_pop_all[name], begin, end,limit_burst=10.0,limit_spike=5)
        # if burst != []:
        #     plt.plot([burst[1],burst[2]],[burst[0],burst[0]],color=color[pop],linewidth=1.0,alpha=0.2)
        # else:
        #     print("No Burst for "+name)
    fig.subplots_adjust(bottom=0.16, left=0.15)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50)
    plt.ylabel('numero neuron',{"fontsize":60.0})
    plt.title('The spike in the network',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    plt.show()
    # plt.savefig(path+'/spike_detector')
    plt.close()

    #zoom on the 200.0 ms at the begning and some neurons
    begin = end-300.0
    data_pop_all = load_spike_ids(gids_all,path, begin, end)
    end = np.max([np.max(times_spike) if len(times_spike) != 0 else begin
                  for [neurons_id,times_spike] in data_pop_all.values()])
    begin = np.min([np.min(times_spike) if len(times_spike) != 0 else end
                    for [neurons_id,times_spike] in data_pop_all.values()])
    fig = plt.figure(figsize=(18, 8))
    for pop,[neurons_id,times_spike] in enumerate(data_pop_all.values()):
            plt.plot(times_spike,neurons_id,'.',color=color[pop],markersize=1)
    for pop,(name,gids) in enumerate(gids_all.items()):
        burst = detection_burst(gids, data_pop_all[name], begin, end,limit_burst=6.0,limit_spike=5)
        if burst != []:
            plt.plot([burst[1],burst[2]],[burst[0],burst[0]],color=color[pop],linewidth=1.0,alpha=0.2)
        else:
            print("No Burst for "+name)
    ##for add a grid
    # ax = plt.axes()
    # plt.xticks(np.arange(begin,end, resolution))
    # plt.yticks(np.arange(ax.get_ylim()[0], ax.get_ylim()[1]+1, 1))
    # ax.grid()
    fig.subplots_adjust(bottom=0.16, left=0.15)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50.0)
    plt.ylabel('numero neuron',{"fontsize":60.0})
    plt.title('The spike in the first population',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.savefig(path+'/spike_detector_zoom_1.eps',format='eps')
    # plt.show()
    plt.close()

    #zoom on the 200.0 ms at the begning and some neurons
    begin = end-1000.0
    data_pop_all = load_spike_ids(gids_all,path, begin, end)
    end = np.max([np.max(times_spike) if len(times_spike) != 0 else begin for [neurons_id,times_spike] in data_pop_all.values()])
    begin = np.min([np.min(times_spike) if len(times_spike) != 0 else end for [neurons_id,times_spike] in data_pop_all.values()])
    fig = plt.figure(figsize=(18, 8))
    for pop,[neurons_id,times_spike] in enumerate(data_pop_all.values()):
            plt.plot(times_spike,neurons_id,'.',color=color[pop],markersize=1)
    for pop,(name,gids) in enumerate(gids_all.items()):
        burst = detection_burst(gids, data_pop_all[name], begin, end,limit_burst=6.0,limit_spike=5)
        if burst != []:
            plt.plot([burst[1],burst[2]],[burst[0],burst[0]],color=color[pop],linewidth=1.0,alpha=0.2)
        else:
            print("No Burst for "+name)
    ##for add a grid
    # ax = plt.axes()
    # plt.xticks(np.arange(begin,end, resolution))
    # plt.yticks(np.arange(ax.get_ylim()[0], ax.get_ylim()[1]+1, 1))
    # ax.grid()
    fig.subplots_adjust(bottom=0.16, left=0.15)
    plt.xlabel('t in ms',{"fontsize":40.0})
    plt.tick_params(axis='both', labelsize=30.0)
    plt.ylabel('numero neuron',{"fontsize":40.0})
    plt.title('The spike in the first population',{"fontsize":60.0})
    plt.xlim(xmax=end, xmin=begin)
    # plt.ylim(ymax=500.0, ymin=0.0)
    plt.savefig(path+'/spike_detector_zoom_2.eps',format='eps')

def print_kuramoto_spike(path,begin,end):
    '''
    print kuramoto parameter
    :param path:
    :param begin:
    :param end:
    :return:
    '''
    import  matplotlib.ticker as ticker
    def myticks(x,pos):
        return r"${:.2f}$".format(x)
    Rs_rev = np.load(path+'R.npy')[0]
    color=['blue','red']
    angle_mean = np.load(path+'/angle_mean.npy')
    time = np.load(path+'time.npy')[0]
    fig = plt.figure(figsize=(20,8))
    gs = gridspec.GridSpec(2, 1)
    gs.update(left=0.2, right=0.95, hspace=0.2, wspace=0.0, bottom=0.15, top=0.7)
    ax = fig.add_subplot(gs[0, 0])
    ax.plot(time[np.where((begin<time) & (time < end))],Rs_rev[np.where((begin<time) & (time < end))],linewidth=10)
    ax.tick_params(axis='both', labelsize=200)
    ax.set_ylim(ymin=0.0,ymax=1.0)
    ax.set_ylabel('R',{"fontsize":200})
    ax.xaxis.set_visible(False)
    ax.yaxis.set_major_locator(plt.MaxNLocator(2))
    ax3 = fig.add_subplot(gs[1, 0])
    speed = np.diff(angle_mean)
    speed[np.where(speed<0.0)]+=2*np.pi
    ax3.plot(time[np.where((begin<time) & (time < end))][:-1]/1000.0,speed,'r',markersize=1.0,linewidth=10)
    ax3.tick_params(axis='both', labelsize=200)
    ax3.set_ylabel('speed of phi',{"fontsize":200})
    ax3.set_xlabel('time in s',{"fontsize":200})
    ax3.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax3.yaxis.set_major_locator(plt.MaxNLocator(3))
    ax3.yaxis.set_major_formatter(ticker.FuncFormatter(myticks))

    gs = gridspec.GridSpec(1, 1)
    ax2 = fig.add_subplot(gs[0, 0])
    gs.update(left=0.2, right=0.95, hspace=0.00, wspace=0.0, bottom=0.77, top=0.99)
    gids_all = get_gids(path)
    begin_2 = end-1000.0
    data_pop_all = load_spike_ids(gids_all,path, begin_2, end)
    for pop,[neurons_id,times_spike] in enumerate(data_pop_all.values()):
            ax2.plot(times_spike/1000.0,neurons_id,'.',color=color[pop],markersize=10)
    ax2.tick_params(axis='both', labelsize=200)
    ax2.set_ylabel('neurons id\n\n',{"fontsize":200})
    ax2.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax2.tick_params(labelleft=False)

    fig.set_size_inches(51.4, 38.9, forward=True)
    plt.subplots_adjust(top=0.99,bottom=0.1,left=0.2,right=0.95)
    # plt.show()
    # plt.savefig('./test.png')
    plt.savefig(path+'Kuramoto_spike.png')

def print_histogram(path, begin, end):
    '''
    print historgrame
    :param path:
    :param begin:
    :param end:
    :return:
    '''
    gids_all = get_gids(path)
    data_pop_all = load_spike_ids(gids_all,path, begin, end)

    # print spike
    fig = plt.figure(figsize=(18, 8))
    ax = plt.gca()
    hist_binwidth=1.0
    t_bins = np.arange(begin,end,hist_binwidth)
    plt.hist(data_pop_all['excitatory'][1],bins=t_bins, color = 'blue', alpha=0.5, edgecolor="blue")
    plt.hist(data_pop_all['inhibitory'][1],bins=t_bins, color = 'red', alpha=0.5, edgecolor="red")
    plt.tick_params(axis='both', labelsize=30.0)
    ax.set_xlabel('times in ms',{"fontsize":30.0})
    ax.set_ylabel('number of spike in on bins',{"fontsize":30.0})
    ax.set_title('histogramme with bins = '+str(hist_binwidth),{"fontsize":50.0})
    ax.set_xlim(xmax=end,xmin=begin)
    # plt.show()
    plt.savefig(path+'/hist_'+str(hist_binwidth)+'.png')
    plt.close()

    # zoom at the end
    data_pop_all = load_spike_ids(gids_all,path, end-1000.0, end)

    # print spike
    fig = plt.figure(figsize=(18, 8))
    ax = plt.gca()
    hist_binwidth=1.0
    t_bins = np.arange(end-1000.0,end,hist_binwidth)
    plt.hist(data_pop_all['excitatory'][1],bins=t_bins, color = 'blue', alpha=0.5, edgecolor="blue")
    plt.hist(data_pop_all['inhibitory'][1],bins=t_bins, color = 'red', alpha=0.5, edgecolor="red")
    plt.tick_params(axis='both', labelsize=30.0)
    ax.set_xlabel('times in ms',{"fontsize":30.0})
    ax.set_ylabel('number of spike in on bins',{"fontsize":30.0})
    ax.set_title('histogramme with bins = '+str(hist_binwidth),{"fontsize":50.0})
    ax.set_xlim(xmax=end,xmin=end-1000.0)
    # plt.show()
    plt.savefig(path+'/hist_zoom_'+str(hist_binwidth)+'.png')
    plt.close()

def spectral_analisys(path, begin, end):
    '''
    print spectral analysis
    :param path:
    :param begin:
    :param end:
    :return:
    '''
    gids_all = get_gids(path)
    data_pop_all = load_spike_ids(gids_all,path, begin, end)

    # print spike
    sampling =1 # ms
    if os.path.exists(path+'/hist_'+str(sampling)+'.npy'):
        hist = np.load(path+'/hist_'+str(sampling)+'.npy')
    else:
        hist = compute_hist_element(gids_all,data_pop_all,begin,end,sampling)[0]
        np.save(path+'/hist_'+str(sampling)+'.npy', hist)
    tfd = np.fft.fft(hist)

    N=len(hist)
    spectre = np.absolute(tfd)*2/N
    time = ((end-begin)*1.e-3) # time of the simulation
    frequency_sampling = 1/(1.e-3*sampling)
    freq=np.arange(N)*1.0/time
    spectre_db = 20*np.log10(spectre/spectre.max())
    spectre_db[0] = np.NaN
    tfd[0] = np.NaN
    # norm_tfd = np.sort(np.flip(freq[np.argsort(np.absolute(tfd[:tfd.shape[0]/2]),)[-20:]]))
    # print(norm_tfd)
    from elephant.spectral import welch_psd
    wel = welch_psd(hist,fs=1./(sampling*1.e-3))
    print(np.max(wel[1]))
    print(wel[0][np.argmax(wel[1])])
    # print(np.flip(wel[0][np.argsort(wel[1],)[-10:]]))


    fig = plt.figure(figsize=(18, 8))
    plt.plot(wel[0],wel[1])
    plt.xlabel('frenquency in Hz',{"fontsize":30.0})
    plt.ylabel('power in B',{"fontsize":30.0})
    plt.xlim(xmax=frequency_sampling/2.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=30.0)
    plt.title("Estimates power spectrum density using Welch's method for bins="+str(sampling)+'ms',{"fontsize":40.0})
    plt.savefig(path+'/spectral_whelch_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(wel[0],wel[1])
    plt.xlabel('frenquency in Hz',{"fontsize":30.0})
    plt.ylabel('power in B',{"fontsize":30.0})
    plt.xlim(xmax=100.0,xmin=-10.0)
    plt.title("Estimates power spectrum density using Welch's method for bins="+str(sampling)+'ms',{"fontsize":40.0})
    plt.grid()
    plt.tick_params(axis='both', labelsize=30.0)
    plt.savefig(path+'/spectral_whelch_100_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(wel[0],wel[1])
    plt.xlabel('frenquency in Hz',{"fontsize":30.0})
    plt.ylabel('power in B',{"fontsize":30.0})
    plt.title("Estimates power spectrum density using Welch's method for bins="+str(sampling)+'ms',{"fontsize":40.0})
    plt.xlim(xmax=20.0,xmin=-2.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=30.0)
    plt.savefig(path+'/spectral_whelch_20_'+str(sampling)+'.png')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq, np.absolute(tfd))
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in B')
    plt.xlim(xmax=frequency_sampling/2.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_norm_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq, np.absolute(tfd))
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in B')
    plt.xlim(xmax=frequency_sampling/2.0)
    plt.xlim(xmax=100.0,xmin=-10.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_norm_100_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq, np.absolute(tfd))
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in B')
    plt.xlim(xmax=frequency_sampling/2.0)
    plt.xlim(xmax=20.0,xmin=-2.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_norm_20_'+str(sampling)+'.png')
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq, tfd.real, freq, tfd.imag)
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in B')
    plt.xlim(xmax=frequency_sampling/2.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq, tfd.real, freq, tfd.imag)
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in B')
    plt.xlim(xmax=100.0,xmin=-10.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_100_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq, tfd.real, freq, tfd.imag)
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in B')
    plt.xlim(xmax=20.0,xmin=-2.0)
    # plt.ylim(ymax=1.0e7)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_20_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq,spectre_db,color='r')
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in dB')
    plt.xlim(xmax=frequency_sampling/2.0)
    plt.grid()
    plt.savefig(path+'/spectral_db_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq,spectre_db,color='r')
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in dB')
    plt.xlim(xmax=100.0,xmin=-10.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_db_100_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq,spectre_db,color='r')
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in dB')
    plt.xlim(xmax=20.0,xmin=-2.0)
    plt.grid()
    plt.savefig(path+'/spectral_db_20_'+str(sampling)+'.png')
    plt.close()
    fig = plt.figure(figsize=(18, 8))
    plt.plot(freq,spectre_db,color='r')
    plt.xlabel('frenquency in Hz')
    plt.ylabel('power in dB')
    plt.xlim(xmax=10.0,xmin=-2.0)
    plt.grid()
    plt.tick_params(axis='both', labelsize=50.0)
    plt.savefig(path+'/spectral_db_10_'+str(sampling)+'.png')
    plt.close()


# path = '/home/kusch/Documents/project/Jirsa/data/sub/g_delay_low/_g_0.04_tau_long_110.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/sub/delay_gc_limit/_weight_excitatory_22.0_tau_long_150.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_10.0_tau_long_130.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_130.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_40.0_tau_long_200.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_low/_weight_long_0.0_tau_long_0.1/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_medium/_weight_long_30.0_tau_long_330.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_high/_weight_long_40.0_tau_long_130.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no/_weight_long_25.0_tau_long_330.0/'
path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_25.0_tau_long_330.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_400.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high/_weight_long_0.0_tau_long_0.1/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_35.0_tau_long_70.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_10.0_tau_long_200.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium_300/_weight_long_10.0_tau_long_180.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_0.0_tau_long_0.1/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_25.0_tau_long_380.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_0.0_tau_long_0.1/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_2.0_tau_long_90.0/'
# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_0.0_tau_long_0.1/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network/_weight_excitatory_7.0_mean_noise_400.0/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_test/_weight_excitatory_6.0_g_0.05/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_test_4_network_noise_sigma_1600_mean_400_g_0_1_test_init/_weight_long_0.0_tau_long_0.0/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_5000_mean_0_g_0_05/_weight_excitatory_8.0_weight_long_0.0/'
# print_histogram(path,22000,32000)
# spectral_analisys(path,2000,12000)
print_spike(path,12000,32000)
# print_kuramoto_spike(path,2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/sub/sub_noise_gc/_weight_excitatory_1.0_sigma_noise_1900.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/sub/sub_noise_gc/_weight_excitatory_2.0_sigma_noise_600.0',2000,12000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_160.0',120000,220000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_110.0',120000,220000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_100.0',2000,12000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_140.0',2000,12000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_100.0',2000,120000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_110.0',120000,220000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_120.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_130.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_140.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_150.0',2000,120000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_160.0',120000,220000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long_2/_weight_excitatory_1.3_tau_long_170.0',120000,220000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_180.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_190.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_200.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_210.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_220.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_230.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_240.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_250.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_260.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_270.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_280.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_290.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_300.0',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/data/gc_delay_as_t_ref_1_5_long_double_without_noise_long/_weight_excitatory_1.3_tau_long_310.0',2000,120000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_100.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_24.0_tau_long_140.0',2000,12000)
# spectral_analisys('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.6_tau_long_120.0',2000,12000)
# spectral_analisys('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.6_tau_long_140.0',2000,12000)
# spectral_analisys('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.2_tau_long_120.0',2000,12000)
# spectral_analisys('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_1.2_tau_long_140.0',2000,12000)
# spectral_analisys('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_0.8_tau_long_120.0',2000,12000)
# spectral_analisys('/home/kusch/Documents/project/Jirsa/data/as_3_t_ref_1_5_long_double_without_noise/_weight_excitatory_0.8_tau_long_140.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise/_weight_long_1.6_tau_long_110.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise/_weight_long_1.6_tau_long_100.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise/_weight_long_3.8_tau_long_270.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise/_weight_long_2.1_tau_long_260.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_without_noise/_weight_long_0.0_tau_long_90.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/data/as_2_gc_delay_t_ref_1_5_long_double_5_5/_weight_long_1.0_tau_long_90.0',2000,12000)
# print_spike('../../../data/data_2/big_layer_weight_long_range_connection_t_ref_0_4/_weight_long_0.1_tau_long_200.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_weight_long_range_connection_t_ref_0_4/_weight_long_0.1_tau_long_150.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_weight_long_range_connection_3/_weight_long_0.1_tau_long_200.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_weight_long_range_connection_3/_weight_long_0.1_tau_long_150.0',2000.0,12000.0)
# print_spike('../../../data/data_2/size_find_t_ref_0_4/_nb_cube_x_10.0_nb_cube_y_20.0',2000.0,12000.0)
# print_spike('../../../data/data_2/size_find_t_ref_0_4_I_sigma_0/_nb_cube_x_9.0_nb_cube_y_20.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_g_I_ext_t_ref_0_4/_mean_I_ext_750.0_g_1.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_g_I_ext_t_ref_0_4/_mean_I_ext_650.0_g_1.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_g_I_ext_t_ref_0_4/_mean_I_ext_700.0_g_1.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_g_I_ext_t_ref_0_4/_mean_I_ext_800.0_g_1.0',2000.0,12000.0)
# print_spike('../../../data/data_2/big_layer_g_I_ext_t_ref_0_4/_mean_I_ext_850.0_g_1.0',2000.0,12000.0)
# print_spike('../../../data/size_find/_nb_cube_x_3.0_nb_cube_y_3.0',2000.0,12000.0)
# print_spike('../../../data/big_layer_weight_long_range_connection/_weight_long_15.0_tau_long_130.0',2000.0,12000.0)
# print_spike('../../data/exploration_t_ref_i_ext_layer/_t_ref_1.9000000000000001_I_ext_650.0',2000.0,2038.5)
# print_spike('../../data/exploration_t_ref_i_ext_layer/_t_ref_0.0_I_ext_975.0',2000,5000)
# print_spike('../../data/exploration_g_i_ext_layer/_g_0.6_I_ext_975.0',2000,5000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_5_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_8_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_10_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_15_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_20_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_25_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_30_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_35_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_40_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_45_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_50_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_5_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_8_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_10_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_15_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_20_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_25_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_30_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_35_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_40_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_45_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_50_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_19_master_seed_42',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w/_mean_w_0_19_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse_200_begin/_mean_w_0_200_master_seed_42',000,2000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse_beging/_mean_w_0_50_master_seed_46',000,2000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse_beging/_mean_w_0_50_master_seed_42',000,2000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse_beging/_mean_w_0_10_master_seed_46',000,2000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse_beging/_mean_w_0_10_master_seed_42',000,2000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_sparse/_mean_w_0_200_master_seed_46',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/random_w_without_coupling/_mean_w_0_45_master_seed_42',2000,3000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.1',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.2',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.3',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.4',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.5',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.6',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.7',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.8',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_42_percentage_inactive_neurons_0.9',6000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.0',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.1',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.2',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.3',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.4',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.5',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.6',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.7',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.8',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/thinkness/_master_seed_46_percentage_inactive_neurons_0.9',2000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_10.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_10.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_20.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_20.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_50.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_50.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_70.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_70.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_690.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_690.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_660.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_660.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_670.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_670.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_680.0_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/mean_I_ext/_mean_I_ext_680.0_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.001_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.001_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.005_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.005_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.11_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.11_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.12_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.12_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.13_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.13_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.14_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.14_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.15_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.15_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.16_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.16_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.17_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.17_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.18_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.18_master_seed_46',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.19_master_seed_42',0000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.19_master_seed_46',0000,12000)


# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.1_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.1_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.12_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.12_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.13_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.13_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.14_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.14_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.15_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.15_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.16_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.16_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.17_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.17_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.18_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.18_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.19_master_seed_42',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/gc/_weight_excitatory_0.19_master_seed_46',10000,12000)
# print_spike('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_long_v2_3/_mean_w_0_200_master_seed_42',2000,120000)
# print_histogram('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_long_v3_current/_mean_w_0_200_master_seed_46',119500,120000)
# print_spike('./PG_test_1',1000,2000)