import numpy as np

param_nest={
    # Resolution of the simulation (in ms).
    'sim_resolution': 0.1,
    # Masterseed for NEST and NumPy.
    'master_seed': 46,
    # Number of threads per MPI process.
    'local_num_threads': 16,
    # If True, data will be overwritten,
    # If False, a NESTError is raised if the files already exist.
    'overwrite_files': True,
    # Print the time progress, this should only be used when the simulation
    # is run on a local machine.
    'print_time': True,
    #verbosity of Nest :M_ALL=0, M_DEBUG=5, M_STATUS=7, M_INFO=10, M_WARNING=20, M_ERROR=30, M_FATAL=40, M_QUIET=100
    'verbosity':20
}

param_topology={
    #condition of periodic boundary condition of not
    'periodic_condition' :True,
    # Number of unit in the axis x of the grid
    'nb_cube_x':20,
    # Number of unit in the axis y of the grid
    'nb_cube_y':10,
    # Larger of unit
    'dim_x':3,
    # Longer of unit
    'dim_y':3,
    # Number of excitatory neurons by unit
    'nb_neuron_by_cube_ex':160,
    # Number of inhibitory neurons by unit
    'nb_neuron_by_cube_in':40,
    # Define the present or not of the long range
    'long_range':False,
    # Long range connection parameter
    'long_connections':np.array([([5,5,'e'],[15,5,'e']),
                                 ]),
    'distance':0, # this parameter doesn't impact the simulation but allow to analyse and simulate for different long_conection
    # Percentage of neurons which are a longue range connexion in the source
    'percentage_src':0.4,
    # Type of neuron
    'neuron_type':'aeif_cond_alpha',
    # Parameter of neuron (different to default value)
    'param_neuron': {
        't_ref':1.5
    },
    # Mean of external input
    'mean_I_ext':0.0,
    # Standard deviation of the external input
    'sigma_I_ext':0.0,
    # Standard deviation of initial condition
    'sigma_V_0':100.0,
    # Mean deviation of initial condition
    'mean_w_0':200.0,
    # percentage of inactive neurons
    'percentage_inactive_neurons':0.0
}

param_connexion={
    # weigth from excitatory neurons
    'weight_excitatory':1.0,
    # ratio between excitatory weight and inhibitory weight
    'g':1.0,
    # the standard deviation of two-dimensional Gaussian
    # probability connexion for excitatory neurons
    'conn_sigma_excitatory':0.8,
    # the standard deviation of two-dimensional Gaussian
    # probability connexion for excitatory neurons of the long range connection
    'conn_sigma_excitatory_long':2.4,
    # the standard deviation of two-dimensional Gaussian
    # probability connexion for inhibitory neurons
    'conn_sigma_inhibitory':1.2,
    #Time of the long range connexion
    'tau_long': 0.0,
    #Weight on the long range connexion
    'weight_long': 0.0
}

param_background={
    #define if the simulation use or not a poisson generator
    'poisson':False,
    #the firing rate of poisson_generator
    'rate': 10.0,
    #the number of connexion from other region of the brain
    'nb_connexion':2000,
    #the weight on the connexion
    'weight':param_connexion['weight_excitatory'],
    #define if the simulation have or not noise
    'noise':True,
    # Mean of the noise in pA
    'mean_noise':610.0,
    # Standard deviation of the noise in pA
    'sigma_noise':0.0,
    #the weight on the connexion
    'weight_noise':1.0,
    #stimulus
    'stimulus':False,
    #stimulus amplitude
    'stimulus_amplitude':0.0,
    #stimulus time to start
    'stimulus_start':0.0,
    #stimulus duration
    'stimulus_duration':0.0,
    #stimulus populatin target
    'stimulus_target':0
}
