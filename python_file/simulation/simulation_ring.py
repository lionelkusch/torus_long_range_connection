import nest.topology as tp
import nest
import numpy as np
import os
import subprocess
import time
import sys

def network_initialisation(results_path,param_nest):
    """
    Initialise the kernel of Nest
    :param results_path: Folder for saving the result of device
    :param param_nest: Dictionary with the parameter for Nest
    :return: Random generator for each thread
    """
    master_seed = param_nest['master_seed']
    local_num_threads = param_nest['local_num_threads']
    # Numpy random generator
    np.random.seed(master_seed)
    pyrngs = [np.random.RandomState(s) for s in range(master_seed, master_seed + local_num_threads)]
    # Nest Kernel
    nest.set_verbosity(param_nest['verbosity'])
    nest.ResetKernel()
    nest.SetKernelStatus({
        # Resolution of the simulation (in ms).
        "resolution": param_nest['sim_resolution'],
        # Print the time progress, this should only be used when the simulation
        # is run on a local machine.
        "print_time": True,
        # If True, data will be overwritten,
        # If False, a NESTError is raised if the files already exist.
        "overwrite_files": True,
        # Number of threads per MPI process.
        'local_num_threads': local_num_threads,
        # Path to save the output data
        'data_path': results_path,
        # Masterseed for NEST and NumPy
        'grng_seed': master_seed + local_num_threads,
        # Seeds for the individual processes
        'rng_seeds': range(master_seed + 1 + local_num_threads, master_seed + 1 + (2 * local_num_threads)),
        })
    return pyrngs

def network_initialisation_neurons(results_path,pyrngs,param_topology,return_gids=False):
    """
    Create all neuron in each unit in Nest. An unit is composed by two populations (excitatory and inhibitory)
    :param results_path: Folder for saving the result of device
    :param pyrngs: Random generator for each thread
    :param param_topology: Dictionary with the parameter for the topology
    :param return_gids: Boolean to choose to return the ids of population or not
    :return: Dictionary with the id of different layer and the index of the layer out
    """
    #condition of periodic boundary condition of not
    periodic_condition = param_topology['periodic_condition']
    # Number of unit in the axis x of the grid
    nb_cube_x = param_topology['nb_cube_x']
    # Number of unit in the axis y of the grid
    nb_cube_y = param_topology['nb_cube_y']
    # Larger of unit
    dim_x = param_topology['dim_x']
    # Longer of unit
    dim_y = param_topology['dim_y']
    if param_topology['long_range']:
        # Unit from the long range connexion
        id_connection = param_topology['long_connections']
    else:
        id_connection = np.array([((-1,-1,''),(-1,-1,''))])
    # Number of excitatory neurons by unit
    nb_neuron_by_cube_ex = param_topology['nb_neuron_by_cube_ex']
    # Number of inhibitory neurons by unit
    nb_neuron_by_cube_in = param_topology['nb_neuron_by_cube_in']
    # Percentage of neurons which are a longue range connexion in the source
    percentage_src = param_topology['percentage_src']
    # Type of neuron
    neuron_type = param_topology['neuron_type']
    # Parameter of neuron (different to default value)
    params = param_topology['param_neuron']
    # Mean of external input
    mean_I_ext = param_topology['mean_I_ext']
    # Sigma of external input
    sigma_I_ext = param_topology['sigma_I_ext']
    # Sigma of initial voltage
    sigma_init_V0 = param_topology['sigma_V_0']
    # mean of initial voltage
    mean_init_w0 = param_topology['mean_w_0']

    # Initialisation of the neuron type
    nest.SetDefaults(neuron_type, params)

    # Create all Units
    list_layer_ex = [] # List of excitatory population
    list_layer_in = [] # List of inhibitory population
    layer_srcs = [] # Population from the longue range connexion
    gids_srcs = [] # Id of the population from the longue range connexion
    previous_layer = 2  # Counter for the Id of layer
    gids_ex = [] # save the Id of all excitatory population
    gids_in = [] # save the Id of all inhibitory population
    connection = np.ones((id_connection.shape[0],3),dtype=int)*-1
    for i in range(0, nb_cube_x):
        for j in range(0, nb_cube_y):
            # location of unit and neurons
            min_x = i * dim_x
            max_x = (i + 1) * dim_x
            min_y = j * dim_y
            max_y = (j + 1) * dim_y
            extent = [float(nb_cube_x * dim_x),float(nb_cube_y * dim_y)] #Bondary condition for the ring
            center_network = [float(dim_x * nb_cube_x) / 2.0, float(dim_y * nb_cube_y) / 2.0]

            #Excitatory layer
            pos_ex = [[np.random.uniform(min_x, max_x), np.random.uniform(min_y, max_y)] for neuron in
                      range(nb_neuron_by_cube_ex)]
            if not (id_connection[:,0] == (i, j, 'e')).all(axis=1).any():
                # Create the layer
                l_ex = tp.CreateLayer({'positions': pos_ex,
                                       'extent': extent,
                                       'elements': neuron_type,
                                       'center': center_network,
                                       'edge_wrap': periodic_condition
                                       })
                list_layer_ex.append({'layer': l_ex, 'numero': len(list_layer_ex), 'position':(i,j)})
                gids_ex.append([previous_layer, previous_layer + len(pos_ex)])
                previous_layer = previous_layer + len(pos_ex) + 1
                if (id_connection[:,1] == (i, j, 'e')).all(axis=1).any():
                    connection[np.where((id_connection[:,1] == (i, j, 'e')).all(axis=1)),1]=l_ex
            else:
                # src layer
                # take only the element in the center of the layer
                center_layer = [(max_x + min_x) / 2.0, (min_y + max_y) / 2.0]
                tpm = []
                for element in pos_ex:
                    dist = np.sqrt((element[0] - center_layer[0]) ** 2 + (element[1] - center_layer[1]) ** 2)
                    tpm.append((element, dist))
                pos_dist = np.array(tpm, dtype=[('element', list), ('dist', float)])
                pos_sort = np.sort(pos_dist, order='dist')
                src = pos_sort[:int(nb_neuron_by_cube_ex * percentage_src)]['element'].tolist()
                rest = pos_sort[int(nb_neuron_by_cube_ex * percentage_src):]['element'].tolist()
                # create the layer
                l_ex = tp.CreateLayer({'positions': rest,
                                       'extent': extent,
                                       'elements': neuron_type,
                                       'center': center_network,
                                       'edge_wrap': periodic_condition
                                       })
                list_layer_ex.append({'layer': l_ex, 'numero': len(list_layer_ex), 'position':(i,j)})
                layer_src = tp.CreateLayer({'positions': src,
                                             'extent': extent,
                                             'elements': neuron_type,
                                             'center': center_network,
                                             'edge_wrap': periodic_condition
                                             })
                gids_ex.append([previous_layer, previous_layer + len(rest)])
                previous_layer = previous_layer + len(rest) + 1
                gids_srcs.append([previous_layer, previous_layer + len(src)])
                previous_layer = previous_layer + len(src) + 1
                layer_srcs.append((layer_src,(i,j)))
                connection[np.where((id_connection[:,0] == (i, j, 'e')).all(axis=1)),0]=layer_src
                if (id_connection[:,1] == (i, j, 'e')).all(axis=1).any():
                    connection[np.where((id_connection[:,1] == (i, j, 'e')).all(axis=1)),1]=l_ex
                    connection[np.where((id_connection[:,1] == (i, j, 'e')).all(axis=1)),2]=layer_src

            # Inhibitory layer
            pos_in = [[np.random.uniform(min_x, max_x), np.random.uniform(min_y, max_y)] for neuron in
                      range(nb_neuron_by_cube_in)]
            l_in = tp.CreateLayer({'positions': pos_in,
                                   'extent': extent,
                                   'elements': neuron_type,
                                   'center': center_network,
                                   'edge_wrap': periodic_condition
                                   })
            list_layer_in.append({'layer': l_in, 'numero': len(list_layer_in), 'position':(i,j)})
            gids_in.append([previous_layer, previous_layer + len(pos_in)])
            previous_layer = previous_layer + len(pos_in) + 1
            if (id_connection[:,0] == (i, j, 'i')).all(axis=1).any():
                connection[np.where((id_connection[:,0] == (i, j, 'i')).all(axis=1)),0]= l_in
            if (id_connection[:,1] == (i, j, 'i')).all(axis=1).any():
                connection[np.where((id_connection[:,1] == (i, j, 'i')).all(axis=1)),1]= l_in
    # add the src population
    if param_topology['long_range']:
        for index,(layer,position) in enumerate(layer_srcs):
            list_layer_ex.append({'layer': layer, 'numero': len(list_layer_ex) , 'position':position})
            gids_ex.append(gids_srcs[index])

    dic_layer = {'excitatory': list_layer_ex, 'inhibitory': list_layer_in, 'connection':connection}

    # save id of each population
    pop_file = open(os.path.join(results_path, 'population_GIDs.dat'), 'w+')
    for gid in gids_ex:
        pop_file.write('%d  %d %s\n' % (gid[0], gid[1],'excitatory'))
    for gid in gids_in:
        pop_file.write('%d  %d %s\n' % (gid[0], gid[1],'inhibitory'))
    pop_file.close()

    # external input and initial condition
    for thread in np.arange(nest.GetKernelStatus('local_num_threads')):
        # Get all nodes on the local thread
        # Using GetNodes is a work-around until NEST 3.0 is released. It
        # will issue a deprecation warning.
        local_nodes = nest.GetNodes([0], {
            'model': neuron_type,
            'thread': thread
        }, local_only=True
                                    )[0]
        # Get number of current virtual process
        # vp is the same for all local nodes on the same thread
        vp = nest.GetStatus(local_nodes)[0]['vp']
        # Create random initial conditions using pyrngs
        nest.SetStatus(
            local_nodes, 'I_e', pyrngs[vp].normal(
                mean_I_ext,
                sigma_I_ext,
                len(local_nodes)
            )
        )
        nest.SetStatus(
            local_nodes, 'V_m', pyrngs[vp].normal(
                nest.GetDefaults(neuron_type)['E_L'],
                sigma_init_V0,
                len(local_nodes)
            )
        )
        nest.SetStatus(
            local_nodes, 'w', pyrngs[vp].normal(
                mean_init_w0,
                mean_init_w0,
                len(local_nodes)
            )
        )
    if param_topology['percentage_inactive_neurons'] != 0.0:
        gids_list = np.empty((0,0),dtype=np.int)
        for gid in gids_ex + gids_in:
            gids_list = np.append(gids_list,np.arange(gid[0],gid[1],1))

        indice =np.random.choice(gids_list, int(len(gids_list)*param_topology['percentage_inactive_neurons']),replace=False)
        nest.SetStatus(indice.tolist(), {'V_peak':10100.0,'V_th':10000.0})
        # save id of each population
        np.save(os.path.join(results_path, 'population_inactive_GIDs'),indice)

    if return_gids:
        return dic_layer,gids_ex + gids_in
    else:
        return dic_layer

def network_connection(dic_layer,param_topology,param_connexion):
    """
    Create the connexion between all the neurons
    :param dic_layer: Dictionary with all the layer
    :param param_topology: Parameter for the topology
    :param param_connexion: Parameter for the connexions
    :return: nothing
    """
    # Larger of unit
    dim_x = param_topology['dim_x']
    # Longer of unit
    dim_y = param_topology['dim_y']

    #Parameter for the number of connexion
    kernel_dict_ex = {'gaussian2D': {'sigma_x':param_connexion['conn_sigma_excitatory'],
                                  'sigma_y':param_connexion['conn_sigma_excitatory'],
                                  'anchor':[0.0,0.0]},
                   }
    kernel_dict_in = {'gaussian2D': {'sigma_x':param_connexion['conn_sigma_inhibitory'],
                                  'sigma_y':param_connexion['conn_sigma_inhibitory'],
                                  'anchor':[0.0,0.0]},
                   }
    conndict_ex = {'connection_type': 'divergent',
                   'weights' :param_connexion['weight_excitatory'],
                   'delays' : nest.GetKernelStatus("min_delay"), # without delay
                   'kernel' : kernel_dict_ex}
    conndict_in = {'connection_type': 'divergent',
                'weights': - param_connexion['g'] * param_connexion['weight_excitatory'],
                'delays' : nest.GetKernelStatus("min_delay"), # without delay
                'kernel' : kernel_dict_in}

    # connection between each population
    list_layer_ex = dic_layer['excitatory']
    list_layer_in = dic_layer['inhibitory']
    for i in list_layer_ex:
        for j in list_layer_ex:
            tp.ConnectLayers(i['layer'], j['layer'], conndict_ex)
        for j in list_layer_in:
            tp.ConnectLayers(i['layer'], j['layer'], conndict_ex)
    for i in list_layer_in:
        for j in list_layer_ex:
            tp.ConnectLayers(i['layer'], j['layer'], conndict_in)
        # Not connexion between inhibitory neurons

    if param_topology['long_range']:
        # Unit from the long range connexion
        connections = param_topology['long_connections']
        for index,(layer_src,layer_out) in enumerate(connections):
            kernel_dict_ex_long = {'gaussian2D': {'sigma_x':param_connexion['conn_sigma_excitatory_long'],
                              'sigma_y':param_connexion['conn_sigma_excitatory_long'],
                              'anchor':[dim_x * (abs(int(layer_out[0]) - int(layer_src[0]))*1.0),
                                        dim_y * (abs(int(layer_out[1]) - int(layer_src[1]))*1.0)]
                                                  },
                              }
            long_conndict = {'connection_type': 'divergent',
                    'weights': param_connexion['weight_long'],
                    'delays' : param_connexion['tau_long'],
                    'kernel' : kernel_dict_ex_long
                         }
            # long range connexion
            tp.ConnectLayers((dic_layer['connection'][index,0],), (dic_layer['connection'][index,1],),long_conndict)
            if dic_layer['connection'][index,2] != -1.0:
                tp.ConnectLayers((dic_layer['connection'][index,0],), (dic_layer['connection'][index,2],),long_conndict)
    del dic_layer['connection']

def network_device(dic_layer,multimeter_record,min_time,time_simulation,param_background):
    """
    Create and Connect different record or input device
    :param dic_layer: Dictionary with all the layer
    :param multimeter_record: Boolean for not if we need to record or not the membrane voltmeter
    :param min_time: Beginning time of recording
    :param time_simulation: End of simulation
    :return: the list of multimeter and spike detector
    """
    #Spike Detector
    #parameter of spike detector
    param_spike_dec= {"start": min_time,
                "stop": time_simulation,
                "withtime": True,
                "withgid": True,
                'to_file': True,
                'to_memory': False,
                'label': 'spike_detector'
                }
    nest.CopyModel('spike_detector','spike_detector_record')
    nest.SetDefaults("spike_detector_record",param_spike_dec)
    #list_record
    dict_spike_detector = {}
    #node
    spike_detector_layer = tp.CreateLayer({'rows': 1,
                                           'columns': 1,
                                           'elements': 'spike_detector_record'
                                           })

    #  Multimeter
    dict_multimeter = {}
    if multimeter_record:
        # parameter of the multimeter
        param_mul = {"start": min_time,
                     "stop": time_simulation,
                     "interval": 1.0,
                     "record_from": ["V_m"],
                     "withtime": True,
                     "withgid": True,
                     'to_memory': True,
                     'label': 'multimeter'
                     }
        nest.CopyModel('multimeter', 'multimeter_record')
        nest.SetDefaults("multimeter_record", param_mul)
        # node
    	multimeter_layer = tp.CreateLayer({'rows': 1,
                                                   'columns': 1,
                                                   'elements': 'multimeter_record'
                                                   })

    #Connection to population
    for name_pops,list_pops in dic_layer.items():
        dict_multimeter[name_pops]=[]
        dict_spike_detector[name_pops]=[]
        for population in list_pops:
            if multimeter_record:
                tp.ConnectLayers(multimeter_layer, population['layer'], {'connection_type': 'divergent'})
                dict_multimeter[name_pops].append(
                    {'detector': multimeter_layer, 'numero': population['numero']})

            tp.ConnectLayers(population['layer'], spike_detector_layer, {'connection_type': 'convergent'})
            dict_spike_detector[name_pops].append(
                {'detector': spike_detector_layer, 'numero': population['numero']})

    # create and connect device
    ##External current input
    # external generator
    if param_background['stimulus']:
        param_dc = {"amplitude": param_background['stimulus_amplitude'],
                    "start": param_background['stimulus_start'],
                    "stop": param_background['stimulus_start']+param_background['stimulus_duration']}
        nest.CopyModel("dc_generator",'dc_stim')
        nest.SetDefaults("dc_stim",param_dc)
        dc_stim = tp.CreateLayer({'rows':1,
                                    'columns':1,
                            'elements': 'dc_stim'
                            })
        tp.ConnectLayers(dc_stim,dic_layer['excitatory'][param_background['stimulus_target']]['layer'],{'connection_type': 'divergent'})
    # external generator
    # param_dc = {"amplitude": 0.0,
    #             "start": 0.0,
    #             "stop": 1.0}
    # nest.CopyModel("dc_generator",'dc')
    # nest.SetDefaults("dc",param_dc)
    # list_dc = []
    # list_dc.append( tp.CreateLayer({'rows':1,
    #                             'columns':1,
    #                     'elements': 'dc'
    #                     }))
    # for list_pop in [list_layer_ex,list_layer_in]:
    #     for population in list_pop:
    #        for dc_layer in list_dc:
    #             tp.ConnectLayers(dc_layer,population['layer'],{'connection_type': 'divergent'})

    #poisson_generator input
    if param_background['poisson']:
        param_poisson_generator = {
            "rate": param_background['rate']*param_background['nb_connexion'],
            "start": 0.0,
            "stop": time_simulation}
        nest.CopyModel("poisson_generator",'poisson_generator_by_population')
        nest.SetDefaults('poisson_generator_by_population',param_poisson_generator)

        conndict_poisson_generator = {'connection_type': 'divergent',
                       'weights' :param_background['weight'],
                       'delays' : nest.GetKernelStatus("min_delay"), # without delay
                        }
        for name_pops,list_pops in dic_layer.items():
            for population in list_pops:
                poisson_generator=tp.CreateLayer({
                    'rows':1,
                    'columns':1,
                    'elements': 'poisson_generator_by_population'
                    })
                tp.ConnectLayers(poisson_generator,population['layer'],conndict_poisson_generator)

    #add noise in current
    if param_background['noise']:
        param_noise_generator = {
            "mean":param_background['mean_noise'],
            "std": param_background['sigma_noise'],
            'dt':nest.GetKernelStatus('resolution'),
            "start": 0.0,
            "stop": time_simulation}
        nest.CopyModel("noise_generator",'noise_generator_global')
        nest.SetDefaults('noise_generator_global',param_noise_generator)
        conndict_noise = {'connection_type': 'divergent',
                       'weights' :param_background['weight_noise'],
                       'delays' : nest.GetKernelStatus("min_delay"), # without delay
                        }
        noise_generator=tp.CreateLayer({
                    'rows':1,
                    'columns':1,
                    'elements': 'noise_generator_global'
                    })
        for name_pops,list_pops in dic_layer.items():
            for population in list_pops:
                tp.ConnectLayers(noise_generator,population['layer'],conndict_noise)

    #record only one neurons
        # parameter of the multimeter
        param_mul_ex = {"start": min_time,
                     "stop": time_simulation,
                     "interval": nest.GetKernelStatus('resolution'),
                     "record_from": ["V_m","w"],
                     "withtime": True,
                     "withgid": True,
                     'to_file': True,
                     'to_memory': False,
                     'label': 'multimeter_ex'
                     }
        nest.CopyModel('multimeter', 'multimeter_record_ex')
        nest.SetDefaults("multimeter_record_ex", param_mul_ex)
    	multimeter_ex = nest.Create('multimeter_record_ex')
        nest.Connect(multimeter_ex, (dic_layer['excitatory'][len(dic_layer['excitatory'])/2]['layer'][0]+1,), {'rule': 'fixed_indegree','indegree':1})
        nest.CopyModel('multimeter_record_ex', 'multimeter_record_in')
        nest.SetDefaults("multimeter_record_in", {'label': 'multimeter_in'})
        multimeter_in = nest.Create('multimeter_record_in')
        nest.Connect(multimeter_in, (dic_layer['inhibitory'][len(dic_layer['inhibitory'])/2]['layer'][0]+1,), {'rule': 'fixed_indegree','indegree':1})


    return dict_multimeter,dict_spike_detector

def print_mean_voltage_population(min_time,time_simulation,results_path,dict_multimeter,param_topology):
    """
    Save the graphic of the mean voltage for all population
    :param min_time: Beginning time of recording
    :param time_simulation: End of simulation
    :param results_path: Folder for saving the result of device
    :param dict_multimeter: List of multimeter device
    :param param_topology: Dictionary with the parameter for the topology
    :return:
    """
     # Number of unit in the axis x of the grid
    nb_cube_x = param_topology['nb_cube_x']
    # Number of unit in the axis y of the grid
    nb_cube_y = param_topology['nb_cube_y']

    import matplotlib.pylab as plt

    list_multimeter_all=[]

    ## By type
    for name_pop,list_multimeter in dict_multimeter.items():
        fig=plt.figure(figsize=(18, 8))
        ax = plt.axes()
        color = plt.cm.spectral(np.linspace(0,1,nb_cube_y*nb_cube_x+1))
        np.random.shuffle(color)
        ax.set_prop_cycle('color',color)
        for numero_pop,multimeter in enumerate(list_multimeter):
            dmm = nest.GetStatus(tp.GetElement(multimeter['detector'],[0,0]))[0]["events"]
            ts = np.array(dmm["times"])
            ts_unique = np.unique(ts)
            nb_neuron = ts.shape[0]/ts_unique.shape[0]
            Vms = np.array(dmm["V_m"])
            Vms_order = Vms[np.argsort(ts)]
            Vms_mean = np.mean(Vms_order.reshape(Vms_order.shape[0] / nb_neuron , nb_neuron), axis=1)
            Vms_mean = np.mean(Vms_mean.reshape(1, len(ts_unique)), axis=0)
            plt.plot(np.arange(min_time,min_time+len(ts_unique)), Vms_mean,label = name_pop+'_'+str(numero_pop))
            plt.tick_params(axis='both', labelsize=50)
            list_multimeter_all.append((name_pop+'_'+str(numero_pop),multimeter))
        plt.xlabel('t in ms',{"fontsize":60.0})
        plt.ylabel('Vm in V',{"fontsize":60.0})
        plt.tick_params(axis='both', labelsize=50)
        fig.subplots_adjust(bottom=0.16, top=0.8, left=0.14)
        plt.xlim(xmax=time_simulation, xmin=min_time)
        plt.title('The mean of the voltage of membrane',{"fontsize":60.0})
        plt.legend(fontsize=20.0,loc='upper center',bbox_to_anchor=(1.05, 1.0) )
        plt.savefig( results_path + 'multimeter_'+name_pop)
        # plt.show()
        plt.close('all')

    #All population
    fig=plt.figure(figsize=(18, 8))
    ax = plt.axes()
    color = plt.cm.spectral(np.linspace(0,1,nb_cube_y*nb_cube_x+1))
    np.random.shuffle(color)
    ax.set_prop_cycle('color',color)
    for (name_pop,multimeter) in list_multimeter_all:
        dmm = nest.GetStatus(tp.GetElement(multimeter['detector'],[0,0]))[0]["events"]
        ts = np.array(dmm["times"])
        ts_unique = np.unique(ts)
        nb_neuron = ts.shape[0]/ts_unique.shape[0]
        Vms = np.array(dmm["V_m"])
        Vms_order = Vms[np.argsort(ts)]
        Vms_mean = np.mean(Vms_order.reshape(Vms_order.shape[0] / nb_neuron , nb_neuron), axis=1)
        Vms_mean = np.mean(Vms_mean.reshape(1, len(ts_unique)), axis=0)
        plt.plot(np.arange(min_time,min_time+len(ts_unique)), Vms_mean,label = name_pop)
        plt.tick_params(axis='both', labelsize=50)
    plt.xlabel('t in ms',{"fontsize":60.0})
    plt.ylabel('Vm in V',{"fontsize":60.0})
    plt.tick_params(axis='both', labelsize=50)
    fig.subplots_adjust(bottom=0.16, top=0.8, left=0.14)
    plt.xlim(xmax=time_simulation, xmin=min_time)
    plt.title('The mean of the voltage of membrane',{"fontsize":60.0})
    plt.legend(fontsize=20.0,loc='upper center',bbox_to_anchor=(1.05, 1.0) )
    plt.savefig( results_path + 'multimeter')
    # plt.show()
    plt.close('all')
    list_multimeter_all.append(list_multimeter)

def simulate (results_path,begin,end,
              param_nest,param_topology,param_connexion,param_background,
              multimeter_record=False, print_volt=False):
    """
    Run one simulation of simple network
    :param results_path: the name of file for recording
    :param tau_long: the time delay of the long range connectivity
    :param begin : time of beginning to record
    :param end : time of end simulation
    :param multimeter_record: add a record the voltage of the membrane of all neurons each 1 ms
    :param print_volt : create figure with the mean voltage of all population
    """
    # Initialisation of the network
    tic = time.time()
    pyrngs=network_initialisation(results_path,param_nest)
    dic_layer=network_initialisation_neurons(results_path,pyrngs,param_topology)
    toc = time.time() - tic
    print("Time to initialize the network: %.2f s" % toc)

    # Connection and Device
    tic = time.time()
    network_connection(dic_layer,param_topology,param_connexion)
    dict_multimeter,dict_spike_detector=network_device(dic_layer,multimeter_record,begin,end,param_background)
    toc = time.time() - tic
    print("Time to create the connections and devices: %.2f s" % toc)

    # Simulation
    tic = time.time()
    nest.Simulate(end)
    toc=time.time()-tic
    print("Time to simulate: %.2f s" % toc)

    # Multimeter analysis
    if print_volt:
        print_mean_voltage_population(begin,end,results_path,dict_multimeter,param_topology)

    # Concatenate the different spike files
    if subprocess.call([os.path.join(os.path.dirname(__file__),'script.sh'),results_path]) == 1:
        sys.stderr.write('ERROR bad concatenation of spikes file\n')
        exit(1)

