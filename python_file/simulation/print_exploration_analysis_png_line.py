import matplotlib.pyplot as plt
import numpy as np
from print_exploration_analysis import getData,grid
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import BoundaryNorm,ListedColormap

def draw_countour(fig,ax,Y,Z,title,xlabel,ylabel,zlabel,label_size,number_size):
        CS = ax.pcolormesh(np.expand_dims(Z,1))
        cbar = fig.colorbar(CS,ax=ax)
        cbar.ax.set_ylabel(zlabel,{"fontsize":label_size})
        cbar.ax.tick_params(labelsize=number_size)
        ax.xaxis.set_ticks([])
        ax.set_ylabel(ylabel,{"fontsize":label_size})
        ax.yaxis.set_ticks(np.arange(0,len(Z),1))
        ax.set_yticklabels(Y)
        ax.set_title(title,{"fontsize":label_size})

def draw_countour_limit(fig,ax,Y,Z,title,xlabel,ylabel,zlabel,label_size,number_size,zmin,zmax,nbins,colorbar,y_label):
        levels = MaxNLocator(nbins=nbins).tick_values(zmin,zmax)
        cnorm = plt.Normalize(vmin=levels[0],vmax=levels[-1])
        clevels = [levels[0]] + list(0.5*(levels[1:]+levels[:-1])) + [levels[-1]]
        colors=plt.cm.viridis(cnorm(clevels))
        norm=BoundaryNorm(levels, ncolors=len(levels)-1)
        cmap = ListedColormap(colors[1:-1], N=len(levels)-1)
        cmap.set_under(colors[0])
        cmap.set_over(colors[-1])
        cmap.colorbar_extend = "both"
        CS = ax.pcolormesh(np.expand_dims(Z,1),cmap=cmap, norm=norm)
        cbar = fig.colorbar(CS,ax=ax,extend='both')
        cbar.ax.set_ylabel(zlabel,{"fontsize":label_size})
        cbar.ax.tick_params(labelsize=number_size)
        ax.xaxis.set_ticks([])
        ax.set_xlabel(title, {"fontsize": label_size, 'horizontalalignment':'right'})
        ax.yaxis.set_ticks(np.arange(0,len(Z),1)+0.5)
        ax.set_yticklabels(Y)
        ax.set_yticks(np.arange(0,len(Z),1), minor='true')
        ax.grid(which='minor')
        ax.tick_params(axis='both', labelsize=number_size)
        if not colorbar:
            cbar.remove()
        if y_label:
            ax.set_ylabel(ylabel,{"fontsize":label_size})
def print_exploration_analysis(data_base,table_name,list_variable,name_result,title,title_result,percentage=False,min_result=None,max_result=None,nbins=1.0,colorbar=True,y_label=True):
    ## pdf
    name_var1=list_variable[0]['name']
    name_var2=list_variable[1]['name']
    title_var1=list_variable[0]['title']
    title_var2=list_variable[1]['title']
    label_size=30.0
    number_size=20.0
    level_percentage = 0.95
    data_global=getData(data_base,table_name,list_variable,'global')
    fig,ax = plt.subplots(figsize=(20,8))
    X,Y,Z=grid(data_global[name_var1],data_global[name_var2],data_global[name_result],res=False,resX=None,resY=None)
    if min_result != None and max_result != None:
        draw_countour_limit(fig,ax,Y,Z,title,title_var1,title_var2,title_result,label_size,number_size,min_result,max_result,nbins,colorbar,y_label)
    else:
        print('badddddd')
    plt.show()

def print_exploration_analysis_ex_in(data_base,table_name,list_variable,name_result,title,title_result,min_result=None,max_result=None,percentage=False,resX=None,resY=None):
    ## pdf
    name_var1=list_variable[0]['name']
    name_var2=list_variable[1]['name']
    title_var1=list_variable[0]['title']
    title_var2=list_variable[1]['title']
    label_size=30.0
    number_size=20.0
    level_percentage = 0.95
    data_excitatory=getData(data_base,table_name,list_variable,'excitatory')
    data_inihibtory=getData(data_base,table_name,list_variable,'inhibitory')
    resolution = resX != None and resY != None
    if resX == None:
        resX=len(np.unique(data_excitatory[name_var1]))
    if resY == None:
        resY=len(np.unique(data_excitatory[name_var2]))
    if 'disp_max' in list_variable[0].keys():
        xmax = list_variable[0]['disp_max']
    else:
        xmax = None
    if 'disp_min' in list_variable[0].keys():
        xmin = list_variable[0]['disp_min']
    else:
        xmin = None
    if 'disp_max'in list_variable[1].keys():
        ymax=list_variable[1]['disp_max']
    else :
        ymax=None
    if 'disp_min'in list_variable[1].keys():
        ymin=list_variable[1]['disp_min']
    else :
        ymin=None

    fig,ax = plt.subplots(figsize=(20,8))
    X,Y,Z=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory[name_result],res=resolution,resX=resX,resY=resY)
    if min_result != None and max_result != None:
        draw_countour_limit(fig,ax,X,Y,Z,resolution,title+' excitatory neurons',title_var1,title_var2,title_result,min_result,max_result,label_size,number_size)
    else:
        draw_countour(fig,ax,X,Y,Z,resolution,title+' excitatory neurons',title_var1,title_var2,title_result,label_size,number_size)
    if percentage :
        X_more,Y_more,Z_more=grid(data_excitatory[name_var1],data_excitatory[name_var2],data_excitatory['percentage'],res=resolution,resX=resX,resY=resY)
        draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)
    plt.show()

    fig,ax = plt.subplots(figsize=(20,8))
    X,Y,Z=grid(data_excitatory[name_var1],data_inihibtory[name_var2],data_inihibtory[name_result],res=resolution,resX=resX,resY=resY)
    if min_result != None and max_result != None:
        draw_countour_limit(fig,ax,X,Y,Z,resolution,title+' inhibitory neurons',title_var1,title_var2,title_result,min_result,max_result,label_size,number_size)
    else:
        draw_countour(fig,ax,X,Y,Z,resolution,title+' inhibitory neurons',title_var1,title_var2,title_result,label_size,number_size)
    if percentage :
        X_more,Y_more,Z_more=grid(data_inihibtory[name_var1],data_inihibtory[name_var2],data_inihibtory['percentage'],res=resolution,resX=resX,resY=resY)
        draw_zone_level(ax,X_more,Y_more,Z_more,resolution,level_percentage,'red')
    draw_point(ax,X,Y)
    set_lim(ax,ymax,ymin,xmax,xmin,number_size)
    plt.show()

database = '/home/kusch/Documents/project/Jirsa/data/gc_delay_t_ref_1_5_long_double/database.db'
t_ref =1.5
table = 'exploration_1'
variable =[
    {'name':'tau_long','title':' tau long in ms','max':0.0,'min':-10.0,},
    {'name':'weight_excitatory','title':'weight on synaptic connections','max':5.0,'min':0.0},
           ]
add = 'no LRC'
colorbar=False
y_label=False
#Mean global
name_result = 'rates_average'
title = add
title_result = 'Firing rate in Hz'
print_exploration_analysis(database,table, variable,name_result,title,title_result,min_result=4.0,max_result=18.0,nbins='auto',colorbar=colorbar,y_label=y_label)

# # Percentage
# name_result = 'percentage'
# title = 'Percentage of active neurons '+add
# title_result = 'Percentage'
# print_exploration_analysis(database,table, variable,name_result,title,title_result)

#R
name_result = 'synch_Rs_average'
title = add
title_result = 'R synchronization'
print_exploration_analysis(database,table, variable,name_result,title,title_result,min_result=0.0,max_result=1.0,percentage=True,nbins=10,colorbar=colorbar,y_label=y_label)

# CV 3 ms
name_result = 'cvs_IFR_3ms'
title = add
title_result = 'Cv IFR 3ms'
print_exploration_analysis(database,table, variable,name_result,title,title_result,min_result=0.0,max_result=5.6,percentage=False,nbins='auto',colorbar=colorbar,y_label=y_label)

# LV
name_result = 'lvs_ISI_average'
title = add
title_result = 'Lv ISI'
print_exploration_analysis(database,table, variable,name_result,title,title_result,min_result=0.0,max_result=3.0,percentage=True,nbins=10,colorbar=colorbar,y_label=y_label)

#
# #Percentage of Burst
# name_result = 'percentage_burst'
# title = add
# title_result = 'percentage of neuron'
# print_exploration_analysis(database,table, variable,name_result,title,title_result,min_result=0.0,max_result=1.0)

# # Mean global Ex and In
# name_result = 'rates_average'
# title = add
# title_result = 'Firing rate in Hz'
# print_exploration_analysis_ex_in(database,table, variable,name_result,title,title_result)
