import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm
import os
from mpl_toolkits.mplot3d import Axes3D

# path = '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_2.0_tau_long_240.0/'
pathes=[
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_5.0_tau_long_350.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_0.0_tau_long_0.1/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_2.0_tau_long_90.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_350.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_0.0_tau_long_0.1/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_5.0_tau_long_60.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_5.0_tau_long_130.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_2.0_tau_long_90.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_2.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_5.0_tau_long_350.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_0.0_tau_long_0.1/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_2.0_tau_long_90.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_015/_weight_long_2.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_0.0_tau_long_0.1/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_2.0_tau_long_90.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_g_004/_weight_long_2.0_tau_long_140.0/',
    #     '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_5.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_10.0_tau_long_130.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_130.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_10.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_5.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_low/_weight_long_10.0_tau_long_140.0/',
]
for path in pathes:
    ids=[31875,34202,31945,38200,16736,36200,5742,33400,21613,37400]
    # ids=[31945,34202,24739,38200,16800,36200,5742,33400,21677,37400]
    # list_ki_rev = np.load(path+'/ki.npy')[0]
    # nb_neurons = list_ki_rev.shape[1]
    # PLV_i = np.ones((len(ids),nb_neurons))*1j
    # for index,id in enumerate(ids):
    #         tmp = np.mean(np.exp(np.add(list_ki_rev[:,:],np.reshape(-list_ki_rev[:,id],(list_ki_rev.shape[0],1)))*1j),axis=0)
    #         PLV_i[index,:]=tmp
    #         print("%d"%(index))
    # np.save(path+'/PLV_i_some.npy',PLV_i)

    PLV_i = np.load(path+'/PLV_i_some.npy')
    plt.matshow(np.abs(PLV_i),vmin=0.0,vmax=1.0)
    ax = plt.gca()
    ax.set_aspect('auto')
    rect = patches.Rectangle((31872,-1),64,12,linewidth=1,edgecolor='r',facecolor='none');ax.add_patch(rect)
    rect = patches.Rectangle((31972,-1),64,12,linewidth=1,edgecolor='r',facecolor='none');ax.add_patch(rect)
    cbar = plt.colorbar()
    ax.set_xticklabels(ids)
    plt.savefig(path+'/mat_PLV.png')
    position = np.concatenate(np.load('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position_2.npy',encoding='latin1'))
    # position = np.concatenate(np.load('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position.npy',encoding='latin1'))
    x= position[:,0]
    y = position[:,1]
    if os.path.exists(path+'/remove.npy'):
        x=np.delete(x,np.load(path+'/remove.npy'),None)
        y=np.delete(y,np.load(path+'/remove.npy'),None)
    label_size = 200
    number_size = 200
    src_x = 5
    src_y = 5
    target_x = 15
    target_y = 5
    x_src = [src_x*3, (src_x)*3, (src_x + 1)*3, (src_x + 1)*3, (src_x)*3]  # abscisses des sommets
    y_src = [src_y*3, (src_y + 1)*3, (src_y + 1)*3, (src_y)*3, (src_y)*3]  # ordonnees des sommets
    x_target = [(target_x)*3, (target_x)*3, (target_x + 1)*3, (target_x + 1)*3, (target_x)*3]  # abscisses des sommets
    y_target = [(target_y)*3, (target_y + 1)*3, (target_y + 1)*3, (target_y)*3, (target_y)*3]  # ordonnees des sommets
    for index,id in enumerate(ids):
        fig = plt.figure(figsize=(20,15))
        plt.subplot(2,1,1)
        ax=plt.gca()
        ax.tick_params(labelleft=False,labelbottom=False)
        plt.scatter(x,y,s=500.0,c=np.abs(PLV_i)[index,:],vmin=0.0,vmax=1.0);
        plt.tick_params(labelsize=number_size)
        # plt.xlabel('x',{"fontsize":label_size})
        plt.ylabel('y',{"fontsize":label_size})
        cbar = plt.colorbar()
        cbar.ax.set_ylabel('PLV',{"fontsize":label_size})
        cbar.ax.tick_params(labelsize=number_size)
        # plt.title('PLV with the neurons '+str(id),{"fontsize":label_size})
        plt.scatter(x[id],y[id],s=1000.0,c='r');
        plt.plot(x_src,y_src,'b', linewidth=20.0)
        plt.plot(x_target,y_target,'b', linewidth=20.0)

        plt.subplot(2,1,2)
        ax=plt.gca()
        ax.tick_params(labelleft=False,labelbottom=False)
        plt.scatter(x,y,s=500.0,c=np.angle(PLV_i)[index,:],vmin=-np.pi,vmax=np.pi,cmap=cm.get_cmap('twilight_shifted'))
        plt.tick_params(labelsize=number_size)
        plt.xlabel('x',{"fontsize":label_size})
        plt.ylabel('y',{"fontsize":label_size})
        cbar = plt.colorbar()
        cbar.ax.set_ylabel('phase difference',{"fontsize":label_size})
        cbar.ax.tick_params(labelsize=number_size)
        # plt.title('phase difference with the neurons '+str(id),{"fontsize":label_size})
        plt.scatter(x[id],y[id],s=1000.0,c='r');
        plt.subplots_adjust(hspace=0.4)
        plt.plot(x_src,y_src,'b', linewidth=20.0)
        plt.plot(x_target,y_target,'b', linewidth=20.0)

        plt.savefig(path+'PLV_i_'+str(id)+'.png')
        fig.set_size_inches(51.4, 38.9, forward=True)
        plt.subplots_adjust(hspace=0.1,top=0.95,bottom=0.12,left=0.07,right=0.9)
        # plt.show()
        plt.savefig('/home/kusch/Documents/poster/nest_2019/image/double/5_140/PLV_i_'+str(id)+'.png')
        # plt.savefig('/home/kusch/Documents/poster/nest_2019/image/single/5_60/PLV_i_'+str(id)+'.png')
        # plt.close('all')
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # ax.scatter3D(x,y,np.abs(PLV_i)[0,:])
        # plt.figure()
        # plt.scatter(x,y,c=np.abs(PLV_i)[0,:])