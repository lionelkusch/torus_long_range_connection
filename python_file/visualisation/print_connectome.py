# -*- coding: utf-8 -*-
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
import numpy as np
import matplotlib.pyplot as plt

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts['distance'] = 10
w.show()
w.setWindowTitle('Connectome')
def visualise():
    connectome_center = np.loadtxt('/home/kusch/Documents/project/mouse/mouse_population/example/centres.txt')
    save_item=[]
    for i in connectome_center:
        md = gl.MeshData.sphere(rows=20, cols=20)
        colors = np.ones((md.faceCount(), 4), dtype=float)
        md.setFaceColors(colors)
        m3 = gl.GLMeshItem(meshdata=md, shader='balloon')
        m3.translate(i[0], i[1], i[2])
        save_item.append(m3)
    return save_item


list_item = visualise()
for i in list_item:
    w.addItem(i)
w.show()
plt.show()

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()