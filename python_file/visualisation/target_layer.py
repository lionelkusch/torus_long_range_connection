# -*- coding: utf-8 -*-
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
import numpy as np
import matplotlib.pyplot as plt
import trimesh

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts['distance'] = 10
w.show()
w.setWindowTitle('Target_layer')
def visualise(layer):
    save_item=[]
    if layer :
        pos_source = np.concatenate(np.load('/home/kusch/Documents/project/TVB/mouse/mouse_nest/770position_source_layer.npy',encoding='latin1'))
        pos_target = np.concatenate(np.load('/home/kusch/Documents/project/TVB/mouse/mouse_nest/770target_layer.npy',encoding='latin1'))
    else:
        pos_source = np.concatenate(np.load('/home/kusch/Documents/project/TVB/mouse/mouse_nest/770position_source.npy',encoding='latin1'))
        pos_target = np.concatenate(np.load('/home/kusch/Documents/project/TVB/mouse/mouse_nest/770target.npy',encoding='latin1'))
    save_item.append(gl.GLScatterPlotItem(pos=pos_source, color=(1,0,0,1), size=20))
    save_item.append(gl.GLScatterPlotItem(pos=pos_target, color=(0,1,0,1), size=2))
    your_mesh = trimesh.load_mesh('/home/kusch/Documents/project/TVB/mouse/mouse_nest/mesh_test/simple.off')
    m1 = gl.GLMeshItem(vertexes=your_mesh.vertices, faces=your_mesh.faces, color=(0,0,1,0.2), smooth=False)
    m1.setGLOptions('additive')
    save_item.append(m1)
    return save_item


list_item = visualise(False)
for i in list_item:
    w.addItem(i)
w.show()
plt.show()

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()