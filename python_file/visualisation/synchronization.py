# -*- coding: utf-8 -*-
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts['distance'] = 20
w.show()
w.setWindowTitle('pyqtgraph example: GLScatterPlotItem')

def visualise(path,begin,end,resolution,resolution_angle,name,delay,long_range=True):
    g = gl.GLGridItem()
    w.addItem(g)
    shift = 50.0
    list_ki_rev = np.load(path+'/ki.npy')[0]
    time = np.load(path+'/time.npy')[0]
    position = np.concatenate(np.load('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position.npy',encoding='latin1'))
    x= position[:,0]
    y = position[:,1]
    save_item = []
    mean = list_ki_rev[-100]
    # for frequence_max in np.arange(6.45,6.46,0.01): #1
    for frequence_max in np.arange(3.685,3.686,0.001):
        plt.figure(figsize=(20,8))
        save = []
        for index_t,angle_time_t in enumerate(list_ki_rev):
            t= time[index_t]
            if t>=begin and t<= end and shift<np.remainder(t,(1/frequence_max*1e3))<shift+resolution:
                plt.plot(range(angle_time_t.shape[0]),angle_time_t[np.argsort(mean)],'bo',markersize=0.05)
                plt.ylim(ymin = -0.5, ymax=6.8)
                print(t)
                save.append(angle_time_t)
        z1 = np.array(save)
        x1 = np.repeat([x],z1.shape[0],axis=0)/x.max()*2.*np.pi
        y1=np.repeat([y],z1.shape[0],axis=0)
        r1, R1 = 10.0, 20.0
        ## rotation around y
        # x = (R1 + r1 * np.cos(x1)) * np.cos(z1)
        # z = (R1 + r1 * np.cos(x1)) * np.sin(z1)
        # y = y1
        ## rotation around x
        # y = (R1 + r1 * np.cos(y1)) * np.cos(z1)
        # z = (R1 + r1 * np.cos(y1)) * np.sin(z1)
        # x = x1
        ## no rotation
        x = x1
        y = y1
        z = z1
        pos = np.concatenate(np.swapaxes(np.array([x,y,z]),0,2),axis=0)
        save_item.append(gl.GLScatterPlotItem(pos=pos, color=(0.1,0.1,0.1,0.03), size=2))
    return save_item

def visualise_one(path,begin,end,resolution,resolution_angle,name,delay,long_range=True):
    g = gl.GLGridItem()
    w.addItem(g)
    position = np.concatenate(np.load('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position_2.npy',encoding='latin1'))
    x= position[:,0]
    y = position[:,1]
    save_item = []
    save =  np.load(path+'/mean_phase_velocity.npy')*1000
    z1 = np.array([save])
    x1 = np.array([x])
    y1= np.array([y])
    x = x1
    y = y1
    z = z1
    xi = np.linspace(-0.1, 20*3+0.1, 20*3*10)
    yi = np.linspace(-0.1, 10*3+0.1, 10*3*10)
    triang = tri.Triangulation(x[0], y[0])
    interpolator = tri.LinearTriInterpolator(triang, z[0])
    Xi, Yi = np.meshgrid(xi, yi)
    zi = interpolator(Xi, Yi)
    plt.contourf(xi, yi, zi,levels=100, vmin = 8.0,vmax=18.0)
    plt.colorbar()
    pos = np.concatenate(np.swapaxes(np.array([x,y,z]),0,2),axis=0)
    save_item.append(gl.GLScatterPlotItem(pos=pos, color=(0.1,0.1,0.1,0.3), size=10))
    pos = np.concatenate(np.swapaxes(np.array([x-60,y,z]),0,2),axis=0)
    save_item.append(gl.GLMeshItem(vertexes=pos,faces=triang.triangles,drawEdges=True, edgeColor=(0, 0, 0, 1)))
    return save_item

path = '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_25.0_tau_long_330.0/'
# path = '/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/python_file/run/simulation/one_network_noise_sigma_2500_mean_400_g_0_05/_weight_excitatory_6.0_weight_long_0.0/'
list_item   = visualise_one(path,2000.0,12000.0,1.0,100,' ',0,False)
for i in list_item:
    w.addItem(i)
w.show()
plt.show()

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
