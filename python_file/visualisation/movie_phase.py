import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.animation as manimation
import matplotlib.cm as cm
import os
from mpl_toolkits.mplot3d import Axes3D
from copy import copy


def animation(path,begin,end,resolution,name,file_position):

    list_ki_rev = np.load(path+'/ki.npy')[0]
    time = np.load(path+'/time.npy')[0]
    print(list_ki_rev.shape)
    nb_neurons = list_ki_rev.shape[1]

    position = np.concatenate(np.load(file_position,encoding='latin1'))
    x= position[:,0]
    y = position[:,1]
    if os.path.exists(path+'/remove.npy'):
          x=np.delete(x,np.load(path+'/remove.npy'),None)
          y=np.delete(y,np.load(path+'/remove.npy'),None)
    src_x = 5
    src_y = 5
    target_x = 15
    target_y = 5
    x_src = [src_x*3, (src_x)*3, (src_x + 1)*3, (src_x + 1)*3, (src_x)*3]  # abscisses des sommets
    y_src = [src_y*3, (src_y + 1)*3, (src_y + 1)*3, (src_y)*3, (src_y)*3]  # ordonnees des sommets
    x_target = [(target_x)*3, (target_x)*3, (target_x + 1)*3, (target_x + 1)*3, (target_x)*3]  # abscisses des sommets
    y_target = [(target_y)*3, (target_y + 1)*3, (target_y + 1)*3, (target_y)*3, (target_y)*3]  # ordonnees des sommets

    my_dpi=100
    # parameter fig
    text_fontsize = 20.0
    title_fontsize = 20.0
    begin = np.argmin(np.abs(time-begin))
    end = np.argmin(np.abs(time-end))
    fig_3 = plt.figure(figsize=(2000/my_dpi, 800/my_dpi), dpi=my_dpi)
    def update_fig(i):
        print(i)
        fig_3.clf()
        ax=plt.gca()
        ax.tick_params(labelleft=False,labelbottom=False)
        plt.scatter(x,y,s=1,c=list_ki_rev[i],vmin=-np.pi,vmax=np.pi,cmap=cm.get_cmap('twilight_shifted'),animated=True)
        plt.tick_params(labelsize=text_fontsize)
        plt.xlabel('x',{"fontsize":text_fontsize})
        plt.ylabel('y',{"fontsize":text_fontsize})
        cbar = plt.colorbar()
        cbar.ax.set_ylabel('phase',{"fontsize":title_fontsize})
        cbar.ax.tick_params(labelsize=text_fontsize)
        plt.subplots_adjust(hspace=0.4)
        plt.plot(x_src,y_src,'b', linewidth=2.0)
        plt.plot(x_target,y_target,'b', linewidth=2.0)
        plt.title('phase t='+str(time[i]),{"fontsize":title_fontsize})
        return [fig_3]
    anim = manimation.FuncAnimation(fig_3, update_fig,frames=np.arange(begin,end,1), blit=True)
    if not os.path.exists(path+"/"+str(resolution)+"/"):
        os.makedirs(path+"/"+str(resolution)+"/")
    anim.save(path+"/"+str(resolution)+"/phase.mp4")
    plt.close('all')
        
        
pathes = [
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_10.0_tau_long_380.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_40.0_tau_long_300.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_25.0_tau_long_280.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_35.0_tau_long_340.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_35.0_tau_long_430.0/',
        '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no_long/_weight_long_25.0_tau_long_330.0/',
    #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_10_no/_weight_long_25.0_tau_long_330.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_no/_weight_long_5.0_tau_long_350.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_0.0_tau_long_0.1/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_2.0_tau_long_90.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_140.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_350.0/',
    # '/home/kusch/Documents/project/Jirsa/data/mechanism/10_high/_weight_long_5.0_tau_long_60.0/',

]
for i in pathes :
        animation(i,
               12000,32000,1.0,'Test','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position_2.npy')
pathes=[
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_0.0_tau_long_0.1/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_5.0_tau_long_60.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/test/single_10_high_2/_weight_long_5.0_tau_long_130.0/',
        
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_120.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_no/_weight_long_25.0_tau_long_400.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_0.0_tau_long_0.1/',
        #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high/_weight_long_0.0_tau_long_0.1/',
        #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_0.0_tau_long_0.1/',
        #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_5.0_tau_long_60.0/',
        #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_medium/_weight_long_5.0_tau_long_210.0/',
        #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_5.0_tau_long_380.0/',
        #'/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_5.0_tau_long_370.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_25.0_tau_long_380.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_low/_weight_long_40.0_tau_long_210.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_15.0_tau_long_200.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_15.0_tau_long_290.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_10.0_tau_long_200.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_300/_weight_long_10.0_tau_long_260.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_35.0_tau_long_70.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_35.0_tau_long_50.0/',
        # '/home/kusch/Documents/project/Jirsa/data/mechanism/mechanism_single_10_high_50/_weight_long_2.0_tau_long_70.0/',
]
# for i in pathes :
#     animation(i,2000,12000,1.0,'Test','/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position.npy')
