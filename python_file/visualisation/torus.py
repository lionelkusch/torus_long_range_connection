import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl

## Create a GL View widget to display data
app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.setWindowTitle('Torus with neurons')
w.setCameraPosition(distance=50)
w.setBackgroundColor('w')

##Torus
dim_x = 1
dim_y = 1
nb_cube_x = 20
nb_cube_y = 10
resolution = 100

x = np.linspace(0, nb_cube_x+1, resolution)/nb_cube_x*2.*np.pi
y = np.linspace(0, nb_cube_y+1, resolution)/nb_cube_y*2.*np.pi
theta, phi = np.meshgrid(x, y)

r1, R1 = nb_cube_y*0.9, nb_cube_x
X1 = (R1 + r1 * np.cos(phi)) * np.cos(theta)
Y1 = (R1 + r1 * np.cos(phi)) * np.sin(theta)
Z1 = r1 * np.sin(phi)

X1= np.reshape(X1,(resolution**2,1))
Y1= np.reshape(Y1,(resolution**2,1))
Z1= np.reshape(Z1,(resolution**2,1))

verts = np.concatenate([X1,Y1,Z1],axis=1)
faces = []
for i in range(verts.shape[0]):
     if i >= (nb_cube_x-1)*(nb_cube_y):
         if i == verts.shape[0]-1:
             faces.append([i,0,i-nb_cube_x])
             faces.append([0,nb_cube_x,i - nb_cube_x])
         else:
            faces.append([i,i+1,i-(nb_cube_x-1)*(nb_cube_y)])
            faces.append([i+1,i-(nb_cube_x-1)*(nb_cube_y),i-(nb_cube_x-1)*(nb_cube_y)+1])
     elif i % (nb_cube_x) == 0 and i !=0:
         faces.append([i,i+1,(i-nb_cube_x)])
         faces.append([i,i+1,(i+nb_cube_x+1)])
     else:
        faces.append([i,i+1,(i+nb_cube_x)])
        faces.append([(i+nb_cube_x+1),i+1,(i+nb_cube_x)])

p2 = gl.GLMeshItem(vertexes=verts, faces=faces,  smooth=False,
                   color=(0.0,0.0,0.0,1.0),
                   # color=(0.0205,0.0205,0.0205,0.1),
                   )
p2.translate(-10,-10,0)
# p2.setGLOptions('translucent')
# p2.setGLOptions('additive')
p2.setGLOptions('opaque')
# w.addItem(p2)

# plot neurons
position = np.load('/home/kusch/Documents/project/Jirsa/Jirsa_V_Stefanescu_A_2010_with_Nest/example/PG_position/position.npy',encoding='latin1')
liminu = 1.0
colors = np.array([(1,1,0,liminu),(1,0,1,liminu),(0,1,1,liminu), (0,0,1,liminu),(0,1,0,liminu),(1,0,0,liminu),(0.5,0.5,0.5,liminu)])
for index,i in enumerate(position) :
    rp, Rp = nb_cube_y, nb_cube_x
    thetap = i[:,0]/dim_x/nb_cube_x*2.*np.pi
    phip = i[:,1]/dim_y/nb_cube_y*2.*np.pi
    xp,yp,zp = (Rp + rp * np.cos(phip)) * np.cos(thetap),(Rp + rp * np.cos(phip)) * np.sin(thetap),rp * np.sin(phip)
    points = np.swapaxes(np.concatenate([[xp],[yp],[zp]],axis=0),0,1)
    p1 = gl.GLScatterPlotItem(pos=points, color= np.repeat([colors[(index%201)%colors.shape[0]]],points.shape[0],axis=0), size= 4.0)
    p1.translate(-10,-10,0)
    w.addItem(p1)
    w.addItem(p2)
# w.addItem(p2)


w.show()

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()