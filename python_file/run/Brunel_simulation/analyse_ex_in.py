from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.analysis_type import compute_rate,compute_irregularity_synchronization
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.result_class import Result_analyse
import os
import numpy as np

def load_spike_ids(path, begin, end):
    """
    Get the id of the neurons which create the spike
    :param gids: the array of the id and name
    :param path: the path to the file
    :param begin: the first time
    :param end: the end of time
    :return: The spike of all neurons between end and begin
    """
    data = {}
    if not os.path.exists(path + "/ex/spike_detector.gdf"):
        print('no file')
        return -1
    data_concatenated = np.loadtxt(path + "/ex/spike_detector.gdf")
    if data_concatenated.size < 5:
        print('empty file')
        return -1
    data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
    idx_time = ((data_raw[:, 1] > begin) * (data_raw[:, 1] < end))
    data_tmp = data_raw[idx_time]
    data['excitatory'] = [data_tmp]

    if not os.path.exists(path + "/in/spike_detector.gdf"):
        print('no file')
        return -1
    data_concatenated = np.loadtxt(path + "/in/spike_detector.gdf")
    if data_concatenated.size < 5:
        print('empty file')
        return -1
    data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
    idx_time = ((data_raw[:, 1] > begin) * (data_raw[:, 1] < end))
    data_tmp = data_raw[idx_time]
    data['inhibitory'] = [data_tmp]
    return data


def analysis_type(path,begin,end,resolution,limit_burst):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    #take the data
    data_pop_all = load_spike_ids(path, begin, end)
    if data_pop_all == -1:
        return -1
    gids_all = {}

    for name,i in data_pop_all.items():
        ids = np.swapaxes(i[0],0,1)[0]
        gids_all[name]=[(int(np.min(ids)),int(np.max(ids)))]

    results_save = Result_analyse()
    results_save.set_name_population(gids_all.keys())

    compute_rate(results_save,gids_all, data_pop_all, begin, end)
    compute_irregularity_synchronization(results_save,gids_all, data_pop_all, begin, end,resolution,limit_burst)
    # results_save.print_result()
    return results_save.result()

# analysis_type('/home/test/Documents/Doctorat/Jirsa/data/brunel/g_1.0_I_ext_700.0_0.1__t_ref_0.4_simtime_4000',
#          1000,4000,0.1,10.0)