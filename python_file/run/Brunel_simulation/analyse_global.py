from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.analysis_global import compute_rate,compute_irregularity_synchronization
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.result_class import Result_analyse
import os
import numpy as np

def load_spike(path, begin, end):
    """
    Get the id of the neurons which create the spike and time
    :param gids: the array of the id
    :param path: the path to the file
    :param begin: the first time
    :param end: the end of time
    :return: The spike of all neurons between end and begin
    """
    data = []
    if not os.path.exists(path + "/ex/spike_detector.gdf"):
        print('no file')
        return -1
    for i in ['ex','in']:
        data_concatenated = np.loadtxt(path + "/"+i+"/spike_detector.gdf")

        if data_concatenated.size < 5:
            print('empty file')
            return -1
        data_raw = data_concatenated[np.argsort(data_concatenated[:, 1])]
        idx_time = ((data_raw[:, 1] >= begin) * (data_raw[:, 1] <= end))
        data.append(data_raw[idx_time])
    return [np.concatenate(data)]


def analysis_global(path,begin,end,resolution,limit_burst):
    """
    The function for global analysis of spikes
    :param path: the path to the file for "population_GIDs.dat" and "spike_detector.gdf"
    :param begin: the first time
    :param end: the last time
    :param resolution: the resolution of the simulation
    :return: The different measure on the spike
    """
    #take the data
    data_all = load_spike(path, begin, end)
    result_global = Result_analyse()
    if data_all == -1:
        result_global.empty()
        result_global.set_name_population(['global'])
        return result_global.result()
    result_global.save_name_population('global')
    gids = []

    for i in data_all:
        ids = np.swapaxes(i,0,1)[0]
        gids.append((int(np.min(ids)),int(np.max(ids))))
        compute_rate(result_global,gids,data_all,begin,end)
    compute_irregularity_synchronization(result_global,gids, data_all, begin, end,resolution,limit_burst)

    # result_global.print_result()

    return result_global.result()

# analysis_global('/home/test/Documents/Doctorat/Jirsa/data/brunel/g_1.0_I_ext_700.0_0.1__t_ref_0.4_simtime_4000',
#          1000,4000,0.1,10.0)