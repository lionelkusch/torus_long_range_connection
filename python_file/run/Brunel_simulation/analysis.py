import sys
import datetime
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.run_exploration import init_database,insert_database
from analyse_global import analysis_global
from analyse_ex_in import analysis_type
import numpy as np
import pathos.multiprocessing as mp
import dill

def analysis(results_path, data_base, table_name, dict_variable, begin, end):
    print('time: ' + str(datetime.datetime.now()) + ' BEGIN ANALYSIS \n')
    # Save analysis in database
    init_database(data_base, table_name, dict_variable)
    # Global analysis
    result_global = analysis_global(path=results_path, begin=begin, end=end,
                                    resolution=0.1,
                                    limit_burst=10.0)
    insert_database(data_base, table_name, results_path, dict_variable, result_global)
    # type analysis
    result_type = analysis_type(path=results_path, begin=begin, end=end,
                                   resolution=0.1,
                                   limit_burst=10.0)
    if result_type != -1:
        insert_database(data_base, table_name, results_path, dict_variable, result_type)
    print('time: ' + str(datetime.datetime.now()) + ' END ANALYSIS \n')

def analysis_brunel (path):
    data_base = path + '/database.db'
    table_name = 'exploration'
    list_path=[]
    for I_mean in [700.0,950.0]:
        for t_ref in [0.4,1.5,2.0]:
            for g in np.arange(1.0,8.0,0.5):
                for I_std in np.concatenate((np.logspace(-3,1,10),[0.01,0.1,1.0])):
                    name = path + "g_" + str(g) + "_I_ext_" + str(I_mean) + '_' + str(
                        I_std) + '_' + "_t_ref_" + str(t_ref) + "_simtime_" + str(4000)
                    dict_variable = {
                        'mean_I_ext': I_mean,
                        't_ref': t_ref,
                        'g': g,
                        'sigma_I_ext': I_std,
                    }

                    list_path.append([name,dict_variable])
                    # multithreading analyse
    p = mp.ProcessingPool(processes=8)
    def analyse_function(arg):
        name,dict_variable = arg
        print('ANALYSIS : '+name)
        analysis(name, data_base, table_name, dict_variable, 1000.0, 4000.0)
    p.map(dill.copy(analyse_function), list_path)

# analysis_brunel(
#     '/home/test/Documents/Doctorat/Jirsa/data/brunel/')
if __name__ == "__main__":
    if len(sys.argv) == 2:
        analysis_brunel(sys.argv[1])