import sys
import brunel_network
import numpy as np

def run_brunel (path):
    for I_mean in [700.0,950.0]:
        for t_ref in [0.4,1.5,2.0]:
            for g in np.arange(1.0,8.0,0.5):
                for I_std in np.concatenate((np.logspace(-3,1,10),[0.01,0.1,1.0])):
                    brunel_network.network(path,g,I_mean,I_std, t_ref,4000)

if __name__ == "__main__":
    if len(sys.argv)==2:
        run_brunel(sys.argv[1])