from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation.run_exploration import run_exploration_2D
import numpy as np
from Jirsa_V_Stefanescu_A_2010_with_Nest.python_file.simulation import parameter_layer

def run_exploration(path,data_base,table_name,begin,end,print_volt=False):
    parameter_layer.param_topology['param_neuron']['t_ref']=1.5
    parameter_layer.param_connexion['g']=2.0
    run_exploration_2D(path, parameter_layer, data_base, table_name, {'mean_I_ext': np.arange(500.0, 1000.0, 25.0),'sigma_I_ext': np.concatenate((np.logspace(-3,2,20),[0.1,1.0]))}, begin, end, print_volt,simulation=False)

if __name__ == "__main__":
    import sys
    if len(sys.argv)==6:
        run_exploration(sys.argv[1],sys.argv[2],sys.argv[3],float(sys.argv[4]),float(sys.argv[5]))
    elif len(sys.argv)==7:
        run_exploration(sys.argv[1],sys.argv[2],sys.argv[3],float(sys.argv[4]),float(sys.argv[5]),bool(sys.argv[6]))
    elif len(sys.argv)==0:
        run_exploration( './I_ext_std_I_ext_t_ref_1.5/', 'database.db', 'exploration', 2000.0, 12000.0)
    else:
        print('missing argument')